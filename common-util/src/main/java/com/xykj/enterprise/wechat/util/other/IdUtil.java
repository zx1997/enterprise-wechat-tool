package com.xykj.enterprise.wechat.util.other;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description:
 * @author: zzf
 * @time: 2020/12/25 17:03
 */
public class IdUtil {

    /**
     * 判断是否成年
     *
     * @return boolean
     * @Param [num]
     **/
    public static boolean ifGrownUp(String num) throws ParseException {
        int year = Integer.parseInt(num.substring(6, 10));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date update = sdf.parse(String.valueOf(year + 18) + num.substring(10, 14));
        Date today = new Date();
        return today.after(update);
    }

    public static void main(String[] args) throws ParseException {
        System.out.println(IdUtil.ifGrownUp("430422199902219177")+"搜索");
    }
}
