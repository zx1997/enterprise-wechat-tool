package com.xykj.enterprise.wechat.util.wecom.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.UserInfo3rd;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-03-31 16:07
 */
public class UserInfo3rdUtil {
    public static UserInfo3rd get(String suite_access_token, String code) {
        Map<String, Object> param = new HashMap<>();
        param.put("suite_access_token", suite_access_token);
        param.put("code", code);

        return QYWXHttpCommUtil.get("service/getuserinfo3rd", param, UserInfo3rd.class);
    }
}
