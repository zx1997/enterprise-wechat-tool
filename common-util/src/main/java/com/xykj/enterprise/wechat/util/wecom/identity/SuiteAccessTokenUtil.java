package com.xykj.enterprise.wechat.util.wecom.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.SuiteAccessToken;
import com.xykj.enterprise.wechat.util.biz.BizException;
import com.xykj.enterprise.wechat.util.biz.BizUtil;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;
import com.ydn.simplecache.SimpleCache;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:21
 */
public class SuiteAccessTokenUtil {

    public static final Integer EXPIRE_SECONDS = 7200;

    public static SuiteAccessToken get(SimpleCache simpleCache, String suite_id, String suite_secret, String suite_ticket) {
        String key = suite_id + "_" + suite_ticket;
        SuiteAccessToken token = simpleCache.get(key);
        if (token != null) {
            return token;
        }
        Map<String, Object> param = new HashMap<>();
        param.put("suite_id", suite_id);
        param.put("suite_secret", suite_secret);
        param.put("suite_ticket", suite_ticket);

        token = QYWXHttpCommUtil.post("service/get_suite_token", param, SuiteAccessToken.class);

        // 更新缓存
        simpleCache.put(key, token, EXPIRE_SECONDS);

        return token;
    }
}
