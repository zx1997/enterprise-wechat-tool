package com.xykj.enterprise.wechat.util.wecom.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.PreAuthCode;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:29
 */
public class PreAuthCodeUtil {

    public static PreAuthCode get(String suite_access_token) {
        Map<String, Object> param = new HashMap<>();
        param.put("suite_access_token", suite_access_token);

        return QYWXHttpCommUtil.get("service/get_pre_auth_code", param, PreAuthCode.class);
    }

}
