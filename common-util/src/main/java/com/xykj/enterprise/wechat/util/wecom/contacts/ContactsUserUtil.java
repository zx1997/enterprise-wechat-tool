package com.xykj.enterprise.wechat.util.wecom.contacts;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.ConvertToOpenIdVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.ConvertToUserIdVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.CreateUserResp;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.GetActiveStatVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.GetJoinQrcodeVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.GetUserResp;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.InviteVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.SimpleUserListVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.UserListVo;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ContactsUserUtil {


    public static CreateUserResp createUser(String accessToken, String userid, String name, String mobile, String email, String department) {
        String[] departmentSplit = department.split(",");

        Map<String, Object> param = new HashMap<>();
        param.put("userid", userid);
        param.put("name", name);
        param.put("mobile", mobile);
        param.put("email", email);
        param.put("department", departmentSplit);
        param.put("access_token", accessToken);

        return QYWXHttpCommUtil.postWithAccessToken("user/create", param, CreateUserResp.class);
    }


    public static GetUserResp getUser(String accessToken, String userid) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("userid", userid);

        return QYWXHttpCommUtil.get("user/get", param, GetUserResp.class);
    }


    public static BaseResp updateUser(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);

        return QYWXHttpCommUtil.postWithAccessToken("user/update", params, BaseResp.class);
    }


    public static BaseResp deleteUser(String accessToken, String userid) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("userid", userid);

        return QYWXHttpCommUtil.get("user/delete", param, BaseResp.class);
    }


    public static BaseResp batchDeleteUser(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);

        return QYWXHttpCommUtil.postWithAccessToken("user/batchdelete", params, BaseResp.class);
    }


    public static SimpleUserListVo simpleList(String accessToken, Integer department_id, Integer fetch_child) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("department_id", department_id);
        param.put("fetch_child", fetch_child);

        return QYWXHttpCommUtil.get("user/simplelist", param, SimpleUserListVo.class);
    }


    public static UserListVo userList(String accessToken, Integer department_id, Integer fetch_child) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("department_id", department_id);
        param.put("fetch_child", fetch_child);

        return QYWXHttpCommUtil.get("user/list", param, UserListVo.class);
    }


    public static BaseResp authSucc(String accessToken, String userid) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("userid", userid);

        return QYWXHttpCommUtil.get("user/authsucc", param, BaseResp.class);
    }


    public static ConvertToOpenIdVo convertToOpenid(String accessToken, String userid) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("userid", userid);

        return QYWXHttpCommUtil.get("user/convert_to_openid", param, ConvertToOpenIdVo.class);
    }


    public static ConvertToUserIdVo convertToUserid(String accessToken, String openid) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("openid", openid);

        return QYWXHttpCommUtil.get("user/convert_to_userid", param, ConvertToUserIdVo.class);
    }


    public static GetActiveStatVo getActiveStat(String accessToken, String date) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("date", date);

        return QYWXHttpCommUtil.get("user/get_active_stat", param, GetActiveStatVo.class);
    }


    public static GetJoinQrcodeVo getJoinQrcode(String accessToken, String size_type) {
        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("size_type", size_type);

        return QYWXHttpCommUtil.get("user/get_join_qrcode", param, GetJoinQrcodeVo.class);
    }


    public static InviteVo invite(String accessToken, Map<String, Object> params) {
        params.put("access_token", accessToken);

        return QYWXHttpCommUtil.postWithAccessToken("batch/invite", params, InviteVo.class);
    }

}
