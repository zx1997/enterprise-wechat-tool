package com.xykj.enterprise.wechat.util.biz;

/**
 * @author Feng Chen
 */
public class Bizs {


    public static Biz ACCESS_TOKEN_FAIL = Biz.of(20001, "获取 Access Token 失败:");
    public static Biz ACCESS_TOKEN_TIMEOUT = Biz.of(20002, "Access Token 超时:");

    public static Biz UNKNOWN_USER = Biz.of(10001, "账号或密码不正确");
    public static Biz ERROR_VERIFY = Biz.of(10002, "短信验证码有误");
    public static Biz ERROR_INVITCODE = Biz.of(10003, "邀请码有误");
    public static Biz UNMATCH_IDCARD = Biz.of(10004, "身份证/手机号/姓名不匹配");
    public static Biz UNMATCH_FACE = Biz.of(10005, "身份证/姓名/人脸不匹配");
    public static Biz ERROR_DEVICE = Biz.of(10006, "与绑定设备不匹配");
    public static Biz UNKNOWN_TRADE = Biz.of(10007, "未知业务类型");
    public static Biz ERROR_REGISTERED = Biz.of(10008, "号码已被注册");
    public static Biz USER_NOT_EXISTS = Biz.of(10009, "用户不存在");
    public static Biz USER_IMSI_ERRO = Biz.of(10010, "用户账号与设备不匹配，请联系管理员解绑操作");
    public static Biz FROZEN_USER = Biz.of(10011, "用户已经被冻结，请联系客服!");
    public static Biz USER_IMSI_USED_ERROR = Biz.of(10012,"该设备已被##mobile##绑定，如有疑问，请联系客服");


    public static Biz ERROR_GRATI = Biz.of(11001, "感恩条件不满足");
    public static Biz ERROR_DEGRADE_GRATI = Biz.of(11002, "感恩降级条件不满足");

    public static Biz ERROR_BALANCE_LESS = Biz.of(21001, "账户余额不足");
    public static Biz ERROR_TRADE_PWD = Biz.of(21002, "交易密码不正确");
    public static Biz ERROR_TRADE_IMSI = Biz.of(21003, "交易IMSI不能为空");
    public static Biz ERROR_MEDIA_BUY_TYPE = Biz.of(21004, "只支持GB购买");
    public static Biz ERROR_ACCT_DATA = Biz.of(21005, "账本数据异常");
    public static Biz ERROR_ACCT_DATA_CHANGE = Biz.of(21006, "操作金额不能小于0");
    public static Biz ERROR_NOT_REAL = Biz.of(21007, "您还未实名");


    public static Biz ERROR_FORMAT_LOGIN_PWD = Biz.of(30001, "登录密码格式不满足6-20位");
    public static Biz ERROR_FORMAT_TRADE_PWD = Biz.of(30002, "交易密码格式不满足6位数字");


    public static Biz ERROR_NULL_PWD = Biz.of(40001,"请先通过手机验证码登陆，再重置密码");
    public static Biz ERROR_NULL_TRADE_PWD = Biz.of(40002,"请先通过手机验证码登陆，再重置交易APP密码");

    public static Biz ERROR_REGISTER_INVITER_MODE = Biz.of(50001,"邀请人不能为空");
    public static Biz ERROR_REGISTER_INVITER_LIMIT_MODE = Biz.of(50002,"无法注册，邀请人直推用户超过上限");
    public static Biz ERROR_REGISTER_NUM_LIMIT = Biz.of(50003,"今日用户注册量已达上限");


    public static Biz TRADE_TYPE_NOT_SUPPORT = Biz.of(60001, "交易类型不支持");
    public static class Biz {

        public int code;
        public String message;

        public static Biz of(int code, String message) {
            return new Biz(code, message);
        }

        public Biz() {
        }

        public Biz(int code, String message) {
            this.code = code;
            this.message = message;
        }
    }

}
