package com.xykj.enterprise.wechat.util.biz;

/**
 * 业务异常
 *
 */
public class BizException extends RuntimeException {

    private int errCode;

    private String errMsg;

    public BizException() {
        super("");
    }

    public BizException(int errCode, String errMsg) {
        super("");
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

}
