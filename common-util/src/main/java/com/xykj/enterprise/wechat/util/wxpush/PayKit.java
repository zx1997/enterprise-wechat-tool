package com.xykj.enterprise.wechat.util.wxpush;


import com.xykj.enterprise.wechat.util.string.StringUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Author: chenchao
 * @Date: 2020/8/31 10:38
 */
public class PayKit {

    public PayKit() {
    }


    public static String toXml(Map<String, String> params) {
        StringBuffer xml = forEachMap(params, "<xml>", "</xml>");
        return xml.toString();
    }

    public static Map<String, String> xmlToMap(String xmlStr) {
        XmlHelper xmlHelper = XmlHelper.of(xmlStr);
        return xmlHelper.toMap();
    }


    public static StringBuffer forEachMap(Map<String, String> params, String prefix, String suffix) {
        StringBuffer xml = new StringBuffer();
        if (StringUtil.isNotEmpty(prefix)) {
            xml.append(prefix);
        }

        Iterator var4 = params.entrySet().iterator();

        while (var4.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry) var4.next();
            String key = (String) entry.getKey();
            String value = (String) entry.getValue();
            if (!StringUtil.isEmpty(value)) {
                xml.append("<").append(key).append(">");
                xml.append((String) entry.getValue());
                xml.append("</").append(key).append(">");
            }
        }

        if (StringUtil.isNotEmpty(suffix)) {
            xml.append(suffix);
        }

        return xml;
    }

    public static String createLinkString(Map<String, String> params) {
        return createLinkString(params, false);
    }

    public static String createLinkString(Map<String, String> params, boolean encode) {
        return createLinkString(params, "&", encode);
    }

    public static String createLinkString(Map<String, String> params, String connStr, boolean encode) {
        return createLinkString(params, connStr, encode, false);
    }


    public static String createLinkString(Map<String, String> params, String connStr, boolean encode, boolean quotes) {
        List<String> keys = new ArrayList(params.keySet());
        Collections.sort(keys);
        StringBuilder content = new StringBuilder();

        for (int i = 0; i < keys.size(); ++i) {
            String key = (String) keys.get(i);
            String value = (String) params.get(key);
            if (i == keys.size() - 1) {
                if (quotes) {
                    content.append(key).append("=").append('"').append(encode ? urlEncode(value) : value).append('"');
                } else {
                    content.append(key).append("=").append(encode ? urlEncode(value) : value);
                }
            } else if (quotes) {
                content.append(key).append("=").append('"').append(encode ? urlEncode(value) : value).append('"').append(connStr);
            } else {
                content.append(key).append("=").append(encode ? urlEncode(value) : value).append(connStr);
            }
        }

        return content.toString();
    }

    public static String urlEncode(String src) {
        try {
            return URLEncoder.encode(src, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException var2) {
            var2.printStackTrace();
            return null;
        }
    }


    public static Map<String, String> aliToMap(HttpServletRequest request) {
        Map<String, String> params = new HashMap();
        Map<String, String[]> requestParams = request.getParameterMap();
        Iterator iter = requestParams.keySet().iterator();

        while (iter.hasNext()) {
            String name = (String) iter.next();
            String[] values = (String[]) requestParams.get(name);
            String valueStr = "";

            for (int i = 0; i < values.length; ++i) {
                valueStr = i == values.length - 1 ? valueStr + values[i] : valueStr + values[i] + ",";
            }

            params.put(name, valueStr);
        }

        return params;
    }

}
