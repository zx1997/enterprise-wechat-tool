package com.xykj.enterprise.wechat.util.wecom.contacts;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import com.xykj.enterprise.wechat.bean.ext.contacts.tag.AddTagUserVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.tag.AddTagVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.tag.DeleteTagUserVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.tag.GetTagUserVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.tag.TagListVo;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;


@Slf4j
public class ContactsTagUtil {


    public static AddTagVo createTag(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("tag/create", params, AddTagVo.class);
    }


    public static BaseResp updateTag(String accessToken, Integer tagid, String tagname) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("tagid", tagid);
        params.put("tagname", tagname);
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("tag/update", params, BaseResp.class);
    }


    public static BaseResp deleteTag(String accessToken, Integer tagid) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.get("tag/delete", params, BaseResp.class);
    }


    public static GetTagUserVo getTagUser(String accessToken, Integer tagid) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("tagid", tagid);
        return QYWXHttpCommUtil.get("tag/get", params, GetTagUserVo.class);
    }


    public static AddTagUserVo addTagUser(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("tag/addtagusers", params, AddTagUserVo.class);
    }


    public static DeleteTagUserVo deleteTagUser(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("tag/deltagusers", params, DeleteTagUserVo.class);
    }


    public static TagListVo tagList(String accessToken) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("tag/list", params, TagListVo.class);
    }
}
