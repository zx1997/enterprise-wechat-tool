package com.xykj.enterprise.wechat.util.wxpush;

import com.xykj.enterprise.wechat.util.other.SHA1Util;

/**
 * @Author george
 * @create 2021-04-20 14:12
 */
public class JSAPIUtil {

    public static String getSignature(String jsapi_ticket, String noncestr, long timestamp, String url) {
        //1. 字段值采用原始值，不要进行URL转义；2. 必须严格按照如下格式拼接，不可变动字段顺序。
        //jsapi_ticket=JSAPITICKET&noncestr=NONCESTR&timestamp=TIMESTAMP&url=URL
        StringBuilder string1 = new StringBuilder();
        string1.append("jsapi_ticket=")
                .append(jsapi_ticket)
                .append("&noncestr=")
                .append(noncestr)
                .append("&timestamp=")
                .append(timestamp)
                .append("&url=")
                .append(url);
        // 签名
        String signature = SHA1Util.encode(string1.toString());
        System.out.println("string1:" + string1.toString());
        return signature;
    }

    public static void main(String[] args) {
        //    jsapi_ticket=df30aacce48aeb092e964b546f7738c9&noncestr=66643d3c15714dbe9eb17b30e1fb228e&timestamp=1618899067684&url=http://baidu.com
        System.out.println(getSignature(
                "df30aacce48aeb092e964b546f7738c9",
                "66643d3c15714dbe9eb17b30e1fb228e",
                1618899067684l,
                "http://baidu.com"
        ));
    }



}
