package com.xykj.enterprise.wechat.util.other;

import java.util.UUID;

/**
 * @author Feng Chen
 */
public abstract class UuidUtil {

    private UuidUtil() {

    }

    public static String get() {
        return UUID.randomUUID().toString();
    }

    public static String getNoSpecialChar() {
        return UUID.randomUUID().toString().replace("-","");
    }

    public static void main(String[] args){
        System.out.println(System.currentTimeMillis());
        System.out.println(getNoSpecialChar());
    }


}
