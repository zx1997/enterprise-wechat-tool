package com.xykj.enterprise.wechat.util.string;

/**
 * @author Feng Chen
 */
public class StringUtil {

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static String arrayToString(String[] array) {
        StringBuilder ret = new StringBuilder("[");
        for (int i = 0; i < array.length; i++) {
            ret.append(array[i]);
            if (i < array.length - 1) {
                ret.append(",");
            }
        }
        ret.append("]");
        return ret.toString();
    }

    public static void main(String args[]){
        String[] args1 = {"123","dff","ghjkl"};
        System.out.println(arrayToString(args1));

    }

}
