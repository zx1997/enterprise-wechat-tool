package com.xykj.enterprise.wechat.util.wecom.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.AdminListVo;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-29 09:57
 */
public class AgentAdminUtil {

    public static AdminListVo getAdminList(String auth_corpid, String agentid, String suite_access_token) {
        Map<String, Object> params = new HashMap<>();
        params.put("auth_corpid", auth_corpid);
        params.put("agentid", agentid);

        Map<String, Object> urlParams = new HashMap<>();
        urlParams.put("suite_access_token", suite_access_token);

        return QYWXHttpCommUtil.postWithUrlParam("service/get_admin_list", urlParams, params, AdminListVo.class);
    }

}
