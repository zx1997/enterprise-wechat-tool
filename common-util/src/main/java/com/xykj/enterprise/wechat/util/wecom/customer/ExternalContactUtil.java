package com.xykj.enterprise.wechat.util.wecom.customer;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.AddContactWayVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.AddCorpTagVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.ExternalContactListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.ExternalContactVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetContactWayVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetCorpTagListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetExternalContactBatchVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetFollowUserListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetUnassignedListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GroupChatListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GroupChatVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.TransferCustomerVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.TransferGroupChatVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.TransferResultVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentCommentsVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentCustomerListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentSendResultVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentTaskVo;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Slf4j
public class ExternalContactUtil {

    public static GetFollowUserListVo getFollowUserList(String accessToken) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_follow_user_list", params, GetFollowUserListVo.class);

    }


    public static AddContactWayVo addContactWayMiniprogram(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/add_contact_way", params, AddContactWayVo.class);

    }


    public static AddContactWayVo addContactWayQrcode(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/add_contact_way", params, AddContactWayVo.class);

    }


    public static BaseResp deleteContactWay(String accessToken, String config_id) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("config_id", config_id);
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/del_contact_way", params, GetContactWayVo.class);

    }


    public static GetContactWayVo getContactWay(String accessToken, String config_id) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_contact_way", params, GetContactWayVo.class);
    }


    public static BaseResp updateContactWay(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/update_contact_way", params, BaseResp.class);
    }


    public static ExternalContactListVo externalContactList(String accessToken, String userid) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("userid", userid);
        return QYWXHttpCommUtil.get("externalcontact/list", params, ExternalContactListVo.class);

    }


    public static BaseResp externalContactRemark(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/remark", params, BaseResp.class);

    }


    public static ExternalContactVo getExternalContact(String accessToken, String external_userid) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("external_userid", external_userid);
        return QYWXHttpCommUtil.get("externalcontact/get", params, ExternalContactVo.class);

    }


    public static GetExternalContactBatchVo getExternalContactBatch(String accessToken, String userid, String cursor, Integer limit) {
        Map param = new HashMap();
        param.put("access_token", accessToken);
        param.put("userid", userid);
        param.put("cursor", cursor);
        param.put("limit", limit);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/batch/get_by_user", param, GetExternalContactBatchVo.class);

    }


    public static GetCorpTagListVo getCorpTagList(String accessToken, String[] tag_id, String[] group_id) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("tag_id", tag_id);
        params.put("group_id", group_id);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_corp_tag_list", params, GetCorpTagListVo.class);

    }


    public static AddCorpTagVo addCorpTag(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/add_corp_tag", params, AddCorpTagVo.class);

    }


    public static BaseResp editCorpTag(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/edit_corp_tag", params, BaseResp.class);

    }


    public static BaseResp deleteCorpTag(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/del_corp_tag", params, BaseResp.class);

    }


    public static BaseResp markCorpTag(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/mark_tag", params, BaseResp.class);

    }


    public static TransferCustomerVo transferCustomer(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/transfer_customer", params, TransferCustomerVo.class);

    }


    public static TransferResultVo transferResult(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/transfer_result", params, TransferResultVo.class);

    }


    public static GetUnassignedListVo getUnassignedList(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_unassigned_list", params, GetUnassignedListVo.class);

    }


    public static TransferCustomerVo transferResignedCustomer(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/resigned/transfer_customer", params, TransferCustomerVo.class);

    }


    public static TransferResultVo transferResignedResult(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/resigned/transfer_result", params, TransferResultVo.class);

    }


    public static TransferGroupChatVo transferGroupChat(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/groupchat/transfer", params, TransferGroupChatVo.class);

    }


    public static GroupChatListVo getGroupChatList(String accessToken, String status_filter, String owner_filter_user_list, String cursor, Integer limit) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("status_filter", status_filter);
        params.put("owner_filter_user_list", owner_filter_user_list);
        params.put("cursor", cursor);
        params.put("limit", limit);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/groupchat/list", params, GroupChatListVo.class);

    }

    public static GroupChatVo getGroupChat(String accessToken, String chatId) {
        Map<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("chat_id", chatId);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/groupchat/get", params, GroupChatVo.class);

    }


    //************************************ 客户朋友圈相关 API ************************************//
    // 官方文档：https://work.weixin.qq.com/api/doc/90001/90143/93443#%E8%8E%B7%E5%8F%96%E5%AE%A2%E6%88%B7%E6%9C%8B%E5%8F%8B%E5%9C%88%E4%BC%81%E4%B8%9A%E5%8F%91%E8%A1%A8%E7%9A%84%E5%88%97%E8%A1%A8

    /**
     * 获取企业全部的发布列表
     */
    public static GetMomentListVo getMomentList(String accessToken, String startTime, String endTime, String creator, String filterType, String cursor) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("start_time", startTime);
        params.put("end_time", endTime);
        params.put("creator", creator);
        params.put("filter_type", filterType);
        params.put("cursor", cursor);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_moment_list", params, GetMomentListVo.class);

    }

    /**
     * 获取客户朋友圈企业发表的列表
     */
    public static GetMomentTaskVo getMomentTask(String accessToken, String momentId, String cursor, Integer limit) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("moment_id", momentId);
        params.put("cursor", cursor);
        params.put("limit", limit);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_moment_task", params, GetMomentTaskVo.class);

    }

    /**
     * 获取客户朋友圈发表时，选择的可见范围
     */
    public static GetMomentCustomerListVo getMomentCustomerList(String accessToken, String momentId, String userid, String cursor, Integer limit) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("moment_id", momentId);
        params.put("userid", userid);
        params.put("cursor", cursor);
        params.put("limit", limit);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_moment_customer_list", params, GetMomentCustomerListVo.class);

    }

    /**
     * 获取客户朋友圈发表后的可见客户列表
     */
    public static GetMomentSendResultVo getMomentSendResult(String accessToken, String momentId, String userid, String cursor, Integer limit) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("moment_id", momentId);
        params.put("userid", userid);
        params.put("cursor", cursor);
        params.put("limit", limit);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_moment_send_result", params, GetMomentSendResultVo.class);
    }

    /**
     * 获取客户朋友圈的互动数据
     */
    public static GetMomentCommentsVo getMomentComments(String accessToken, String momentId, String userid) {
        Map params = new HashMap();
        params.put("access_token", accessToken);
        params.put("momentId", momentId);
        params.put("userid", userid);
        return QYWXHttpCommUtil.postWithAccessToken("externalcontact/get_moment_comments", params, GetMomentCommentsVo.class);
    }

    public static void main(String[] args) {
        String accessToken = "46F4O1grIlHsQSnIWUk98hCrm-Wf69KqnzUg_6UlnREFMYwPCp1aUukPE52e_lVfd_-wz_e8oswu3m6EPlYYqrR1Fa1L-Vntsugv0p0JmtsTXHZtnwWWkgxAEJ_Hx1Z5FB0vivEkXmXYvVWiXl7-kXZ95Vd95MV4YPu2RORxIh0_U-yd-BN2BIRYRShlLq9YpFPfbeE64PWWAJlej0ViUA";
        Date now = new Date();

        GetMomentListVo vo = getMomentList(
                accessToken,
                // 企业微信最多支持查询一个月
                TimeUtil.getTimeSecond(TimeUtil.getPastDate(30)),
                TimeUtil.getTimeSecond(now),
                null,
                null,
                null
        );
        log.debug("vo.tostring:{}" + vo.toString());

    }

}
