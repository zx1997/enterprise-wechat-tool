package com.xykj.enterprise.wechat.util.wecom.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.GetPermanentCodeVo;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:33
 */
public class PermanentCodeUtil {

    public static GetPermanentCodeVo get(String auth_code, String suite_access_token) {
        Map<String, Object> params = new HashMap<>();
        params.put("auth_code", auth_code);

        Map<String, Object> urlParams = new HashMap<>();
        urlParams.put("suite_access_token", suite_access_token);

        return QYWXHttpCommUtil.postWithUrlParam("service/get_permanent_code", urlParams, params, GetPermanentCodeVo.class);
    }

}
