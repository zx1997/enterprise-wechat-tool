package com.xykj.enterprise.wechat.util.time;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Feng Chen
 */
public abstract class DateUtil {

    private DateUtil() {

    }

    private static final DateFormat FULL_PATTERN = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private static final DateFormat DATE_PATTERN = new SimpleDateFormat("yyyyMMdd");

    private static final DateFormat DATE_MBL_PATTERN = new SimpleDateFormat("yyyy-MM-dd");

    private static final DateFormat MONTH_PATTERN = new SimpleDateFormat("yyyyMM");

    public static String getMonth() {
        return getMonth(new Date());
    }


    public static String getMonth(Date date) {
        return MONTH_PATTERN.format(date);
    }

    public static String getDate() {
        return DATE_PATTERN.format(new Date());
    }

    public static String getDate(Date date) {
        return DATE_PATTERN.format(date);
    }

    public static String format(Date date) {
        return FULL_PATTERN.format(date);
    }

    public static String formatDate(Date date) {
        return DATE_PATTERN.format(date);
    }

    public static Date parseDate(String date){
        try {
            return DATE_PATTERN.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date parse(String source) throws ParseException {
        return FULL_PATTERN.parse(source);
    }

    public static Date parseMbl(String source) {
        try {
            return DATE_MBL_PATTERN.parse(source);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String parseMbl(Date source) {
        return DATE_MBL_PATTERN.format(source);

    }

    public static List<String> getMonth(int num) {
        return getMonth(new Date(), num);
    }

    public static List<String> getMonth(Date date, int num) {
        List<String> months = new ArrayList<>();
        months.add(getMonth(date));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        for (int i = 0; i < num - 1; i++) {
            calendar.add(Calendar.MONDAY, -1);
            date = calendar.getTime();
            months.add(getMonth(date));
        }
        return months;
    }

    public static void main(String[] args) {
//        List<String> months = getMonth(new Date(), 3);
//        for (String month : months) {
//            System.out.println(month);
//        }

        int pageSize = 10;
        int total = 198;

        System.out.println((int) Math.ceil((total * 1.0) / pageSize));

    }
}
