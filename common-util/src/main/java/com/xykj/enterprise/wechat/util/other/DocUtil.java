package com.xykj.enterprise.wechat.util.other;

import com.alibaba.fastjson.JSON;
import org.bson.Document;

/**
 * @author Feng Chen
 */
public class DocUtil {

    /**
     * 将 document 对象转 bean
     *
     * @param doc
     * @param type
     * @param <T>
     * @return
     */
    public static <T> T decoce(Document doc, Class<T> type) {
        if (doc == null) {
            return null;
        }
        return JSON.parseObject(JSON.toJSONString(doc), type);
    }


    /**
     * 将 bean 转 document 对象
     *
     * @param object
     * @return
     */
    public static Document encode(Object object) {
        return new Document(JSON.parseObject(JSON.toJSONString(object)));
    }

}
