package com.xykj.enterprise.wechat.util.wecom.schedule;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import com.xykj.enterprise.wechat.bean.ext.schedule.AddScheduleVo;
import com.xykj.enterprise.wechat.bean.ext.schedule.GetScheduleVo;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;


@Slf4j
public class ScheduleUtil {


    public static AddScheduleVo addSchedule(String accessToken, String json) {
        return QYWXHttpCommUtil.postWithAccessToken("oa/schedule/add", json, accessToken, AddScheduleVo.class);

    }


    public static BaseResp updateSchedule(String accessToken, String json) {
        return QYWXHttpCommUtil.postWithAccessToken("oa/schedule/update", json, accessToken, BaseResp.class);

    }


    public static GetScheduleVo getSchedule(String accessToken, Map<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("oa/schedule/get", params, GetScheduleVo.class);

    }


    public static BaseResp deleteSchedule(String accessToken, Map<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("oa/schedule/del", params, BaseResp.class);

    }


    public static GetScheduleVo getScheduleByCalendar(String accessToken, Map<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("oa/schedule/get_by_calendar", params, GetScheduleVo.class);

    }
}
