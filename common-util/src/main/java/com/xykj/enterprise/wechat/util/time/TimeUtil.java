package com.xykj.enterprise.wechat.util.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Author: chenchao
 * @Date: 2020/7/11 16:16
 */
public class TimeUtil {
    public static String FORMAT_YYYYMMDDHHMMSS = "yyyy-MM-dd HH:mm:ss";

    public static String FORMAT_DEFAULT = "yyyyMMddHHmmss";

    public static String FORMAT_YYYYMMDD = "yyyyMMdd";

    public static String FORMAT_YYYYMM = "yyyyMM";


    /**
     * 获取当前时间
     */
    public static int getTimeYYMM(Date date) {
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_YYYYMM);
        return Integer.parseInt(df.format(date));
    }


    /**
     * 获取日期 天
     */
    public static int getTimeYYMMDD(Date date) {
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_YYYYMMDD);
        return Integer.parseInt(df.format(date));
    }


    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return
     */
    public static int getPastYYMMDD(int past) {
        Date pastDate = getPastDate(past);
        return getTimeYYMMDD(pastDate);
    }

    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return
     */
    public static Date getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        return calendar.getTime();
    }


    /**
     * 功能描述：返回月
     *
     * @param date Date 日期
     * @return 返回月份
     */
    public static int getMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 功能描述：返回日期
     *
     * @param date Date 日期
     * @return 返回日份
     */
    public static int getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 功能描述：返回小时
     *
     * @param date 日期
     * @return 返回小时
     */
    public static int getHour(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }


    /**
     * 功能描述：返回毫
     *
     * @param date 日期
     * @return 返回毫
     */
    public static long getMillis(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.getTimeInMillis();
    }


    public static boolean inTime(Date begin, Date end, Date now) {

        if (begin == null && end == null) {
            return true;
        }

        long nowTime = now.getTime();

        if (end == null) {
            long beginTime = begin.getTime();
            return nowTime > beginTime;
        }

        if (begin == null) {
            long endTime = end.getTime();
            return nowTime < endTime;
        }

        try {
            return nowTime > begin.getTime() && nowTime < end.getTime();
        } catch (Exception e) {
            return false;
        }
    }

    public static Date parse(String date, String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        try {
            return df.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String format(Date date, String pattern) {
        SimpleDateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    public static Date addTime(Date data, Integer second) {
        Long curTime = data.getTime();
        long expireTime = curTime + second * 1000;
        return new Date(expireTime);
    }

    /**
     * 时间戳，精确到秒
     *
     * @param date
     * @return
     */
    public static String getTimeSecond(Date date) {
        return Long.toString(date.getTime() / 1000L);
    }


    public static void main(String[] args) {
        long curTime = new Date().getTime();
        long expireTime = curTime - 1 * 60 * 60 * 1000;

        System.out.println(new Date(expireTime));


        System.out.println(curTime > expireTime);


    }

}
