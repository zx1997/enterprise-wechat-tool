package com.xykj.enterprise.wechat.util.other;

import com.alibaba.fastjson.JSON;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 红包拆分工具类
 *
 * @author Feng Chen
 */
public abstract class PkgUtil {

    private PkgUtil() {

    }

    /**
     * 将一个浮点型数随机分为n份
     *
     * @param total
     * @param num
     * @return
     */
    public static List<Double> divide(Double total, Integer num) {
        Integer place = getDecimalPlace(total);
        int mult = (int) Math.pow(10, place);
        int data = (int) (total * mult);
        List<Integer> tmp = divide(data, num);
        List<Double> list = new ArrayList<>();
        for (Integer value : tmp) {
            list.add(new BigDecimal(value).divide(new BigDecimal(mult)).doubleValue());
        }
        return list;
    }

    /**
     * 将一个浮点型数随机分为n份
     *
     * @param total
     * @param num
     * @return
     */
    public static List<BigDecimal> divide(BigDecimal total, Integer num) {
        Integer place = getDecimalPlace(total);
        int mult = (int) Math.pow(10, place);
        int data = (int) (total.doubleValue() * mult);
        List<Integer> tmp = divide(data, num);
        List<BigDecimal> list = new ArrayList<>();
        for (Integer value : tmp) {
            list.add(new BigDecimal(value).divide(new BigDecimal(mult)));
        }
        return list;
    }

    /**
     * 将一个整数随机分成几份
     *
     * @param total 总数
     * @param num   分成份数
     * @return
     */
    public static List<Integer> divide(Integer total, Integer num) {
        List<Integer> list = new ArrayList<>();

        // 剩余数
        Integer rest = total;

        // 剩余份
        Integer remain = num;

        Random random = new Random();
        for (int i = 0; i < num - 1; i++) {
            //随机范围:[1,剩余人均金额的2倍-1]分
            int amount = random.nextInt(rest / remain * 2 - 1) + 1;

            rest -= amount;
            remain--;
            list.add(amount);
        }

        // 最后一个直接放进去
        list.add(rest);
        return list;
    }

    // SECTION: INNER HELPER

    private static Integer getDecimalPlace(double value) {
        BigDecimal data = new BigDecimal("" + value);
        String str = data.toPlainString();
        int index = str.indexOf(".");
        return index < 0 ? 0 : (str.length() - 1 - index);
    }

    private static Integer getDecimalPlace(BigDecimal value) {
        return getDecimalPlace(value.doubleValue());
    }

    public static void main(String[] args) {

        for (int term = 1; term <= 100; term++) {
            System.out.println("================");
            System.out.println("第" + term + "轮开始");
//            List<BigDecimal> amountList = divide(new BigDecimal(8.6), 3);
//            List<Double> amountList = divide(8.6d, 3);
            List<Integer> amountList = divide(8, 3);
            System.out.println(JSON.toJSONString(amountList));
            System.out.println("第" + term + "轮结束");
        }


    }


}
