package com.xykj.enterprise.wechat.util.biz;

/**
 * @author YangYiQun
 */
public class BizUtil {
    public static void exception(Bizs.Biz biz) {
        exception(biz.code, biz.message);
    }

    public static void exception(String message) {
        exception(500, message);
    }

    public static void exception(int code, String message) {
        throw new BizException(code, message);
    }
}
