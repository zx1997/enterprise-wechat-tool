package com.xykj.enterprise.wechat.util.other;

/**
 * @Author george
 * @create 2021-04-20 14:48
 */
public class TokenKeyUtil {

    public static final String TOKEN_KEY = "TOKEN_****@####";

    // TOKEN key 生成
    public static String get(String userid, String corpid) {
        return TOKEN_KEY.replace("****", userid).replace("####", corpid);
    }

}
