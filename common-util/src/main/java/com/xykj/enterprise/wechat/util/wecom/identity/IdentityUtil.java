package com.xykj.enterprise.wechat.util.wecom.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.AccessToken;
import com.xykj.enterprise.wechat.bean.ext.identity.JsapiTicketRsp;
import com.xykj.enterprise.wechat.bean.ext.identity.SuiteAccessToken;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;

import java.util.HashMap;
import java.util.Map;


public class IdentityUtil {

    public static SuiteAccessToken getSuiteAccessToken(String suiteId, String suiteSecret, String suiteTicket) {
        Map<String, Object> param = new HashMap<>();
        param.put("suite_id", suiteId);
        param.put("suite_secret", suiteSecret);
        param.put("suite_ticket", suiteTicket);

        return QYWXHttpCommUtil.post("service/get_suite_token", param, SuiteAccessToken.class);
    }


    public static AccessToken getCorpToken(String authCorpId, String permanentCode, String suitAccessToken) {
        Map<String, Object> params = new HashMap<>();
        params.put("auth_corpid", authCorpId);
        params.put("permanent_code", permanentCode);

        Map<String, Object> urlParams = new HashMap<>();
        urlParams.put("suite_access_token", suitAccessToken);
        return QYWXHttpCommUtil.postWithUrlParam("service/get_corp_token", urlParams, params, AccessToken.class);
    }


    public static JsapiTicketRsp getAgentJsapiTicket(String accessToken) {
        Map<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        params.put("type", "agent_config");
        return QYWXHttpCommUtil.get("ticket/get", params, JsapiTicketRsp.class);
    }


    public static JsapiTicketRsp getCorpJsapiTicket(String accessToken) {
        Map<String, Object> params = new HashMap<>();
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.get("get_jsapi_ticket", params, JsapiTicketRsp.class);
    }
}
