package com.xykj.enterprise.wechat.util.base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

/**
 * @author Feng Chen
 */
public abstract class Base64Util {

    private Base64Util() {

    }

    public static final Logger logger = LoggerFactory.getLogger(Base64Util.class);

    public static String encode(String str) {
        try {
            byte[] bytes = Base64.getEncoder().encode(str.getBytes("utf-8"));
            return new String(bytes);
        } catch (UnsupportedEncodingException e) {
            logger.error("", e);
        }
        return null;
    }

    public static String decode(String str) {
        try {
            byte[] bytes = Base64.getDecoder().decode(str.getBytes("utf-8"));
            return new String(bytes);
        } catch (UnsupportedEncodingException e) {
            logger.error("", e);
        }
        return null;
    }

}
