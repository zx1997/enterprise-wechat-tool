package com.xykj.enterprise.wechat.util.wecom.contacts;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import com.xykj.enterprise.wechat.bean.ext.contacts.department.CreateDepartmentVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.department.DepartmentListVo;
import com.xykj.enterprise.wechat.util.wecom.QYWXHttpCommUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ContactsDepartmentUtil {

    public static CreateDepartmentVo createDepartment(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.postWithAccessToken("department/create", params, CreateDepartmentVo.class);
    }


    public static BaseResp deleteDepartment(String accessToken, Integer id) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("access_token", "");
        return QYWXHttpCommUtil.get("department/delete", params, BaseResp.class);
    }


    public static DepartmentListVo departmentList(String accessToken, Integer id) {
        Map<String, Object> params = new HashMap<>();
        params.put("id", id);
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.get("department/list", params, DepartmentListVo.class);
    }


    public static BaseResp updateDepartment(String accessToken, HashMap<String, Object> params) {
        params.put("access_token", accessToken);
        return QYWXHttpCommUtil.get("department/list", params, BaseResp.class);
    }
}
