package com.xykj.enterprise.wechat.wap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 */
@SpringBootApplication
@ComponentScan("com.xykj.enterprise.wechat")
public class WapPMBooter {

    public static void main(String[] args) {
        SpringApplication.run(WapPMBooter .class);
    }

}
