package com.xykj.enterprise.wechat.wap.controller;

import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.base.web.domain.AjaxPage;
import com.xykj.enterprise.wechat.bean.wap.pc.ProjectVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcProject;
import com.xykj.enterprise.wechat.core.service.pc.PCProjectService;
import com.ydn.dbframe.plugin.activerecord.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 项目管理相关接口
 *
 * @Author george
 * @create 2021-05-12 16:57
 */
@Api(description = "项目管理接口")
@RestController
@RequestMapping("project")
public class ProjectController {
    @Autowired
    private PCProjectService pcProjectService;

    @ApiOperation(value = "查询项目列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "limit", value = "页大小", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "projectProcess", value = "项目进度", dataType = "string", paramType = "query", required = false),

    })
    @GetMapping("list")
    public AjaxPage<PcProject> list(Integer page, Integer limit, String projectProcess) {
        Page<PcProject> pcProjectPage = pcProjectService.list(page, limit, UserContext.getCorpId(), UserContext.getUserId(), projectProcess);
        return AjaxPage.success(pcProjectPage.getList(), pcProjectPage.getTotalRow(), pcProjectPage.getTotalPage());
    }

    @ApiOperation(value = "查询项目列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "项目ID", dataType = "string", paramType = "query", required = true),

    })
    @GetMapping("detail")
    public AjaxData detail(Integer id) {
        PcProject pcProject = pcProjectService.get(id, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success(pcProject);
    }

    @ApiOperation(value = "新增项目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectVo", dataType = "ProjectVo", required = true),
    })
    @PostMapping("save")
    public AjaxData save(@RequestBody ProjectVo vo) {
        pcProjectService.save(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "修改项目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectVo", dataType = "ProjectVo", required = true),
    })
    @PostMapping("update")
    public AjaxData update(@RequestBody ProjectVo vo) {
        pcProjectService.update(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "修改项目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectVo", dataType = "ProjectVo", required = true),
    })
    @PostMapping("delete")
    public AjaxData delete(@RequestBody ProjectVo vo) {
        pcProjectService.delete(vo.getId(), UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }


}
