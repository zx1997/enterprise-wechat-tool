package com.xykj.enterprise.wechat.wap.controller;

import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.base.web.domain.AjaxPage;
import com.xykj.enterprise.wechat.bean.wap.pc.ProjectBugVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcBug;
import com.xykj.enterprise.wechat.core.service.pc.PCBugService;
import com.ydn.dbframe.plugin.activerecord.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-27 15:36
 */
@Api(description = "项目BUG管理接口")
@RestController
@RequestMapping("bug")
public class BugController {
    @Autowired
    private PCBugService pcBugService;

    @ApiOperation(value = "查询项目列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "limit", value = "页大小", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "projectProcess", value = "项目进度", dataType = "string", paramType = "query", required = false),

    })
    @GetMapping("list")
    public AjaxPage<PcBug> list(Integer page, Integer limit, Long projectId) {
        Map<String, Object> param = new HashMap<>();
        param.put("corpid", UserContext.getCorpId());
        param.put("userId", UserContext.getUserId());
        param.put("projectId", projectId);

        Page<PcBug> pcBugPage = pcBugService.list(page, limit, param);
        return AjaxPage.success(pcBugPage.getList(), pcBugPage.getTotalRow(), pcBugPage.getTotalPage());
    }

    @ApiOperation(value = "新增项目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectVo", dataType = "ProjectVo", required = true),
    })
    @PostMapping("save")
    public AjaxData save(@RequestBody ProjectBugVo vo) {
        pcBugService.save(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "修改项目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectVo", dataType = "ProjectVo", required = true),
    })
    @PostMapping("update")
    public AjaxData update(@RequestBody ProjectBugVo vo) {
        pcBugService.update(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "删除项目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectVo", dataType = "ProjectVo", required = true),
    })
    @PostMapping("delete")
    public AjaxData delete(@RequestBody ProjectBugVo vo) {
        pcBugService.delete(vo.getId(), UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }


}
