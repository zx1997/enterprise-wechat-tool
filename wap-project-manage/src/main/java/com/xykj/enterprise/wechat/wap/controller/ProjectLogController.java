package com.xykj.enterprise.wechat.wap.controller;

import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.base.web.domain.AjaxPage;
import com.xykj.enterprise.wechat.bean.wap.pc.ProjectLogVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcProjectLog;
import com.xykj.enterprise.wechat.core.service.pc.PCProjectLogService;
import com.ydn.dbframe.plugin.activerecord.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 成员项目日志管理相关接口
 *
 * @Author george
 * @create 2021-05-12 16:57
 */
@Api(description = "成员项目日志管理接口")
@RestController
@RequestMapping("projectLog")
public class ProjectLogController {
    @Autowired
    private PCProjectLogService pcProjectLogService;

    @ApiOperation(value = "查询成员项目日志列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "limit", value = "页大小", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "projectId", value = "项目ID", dataType = "string", paramType = "query", required = false),
    })
    @GetMapping("list")
    public AjaxPage<PcProjectLog> list(Integer page, Integer limit, Integer projectId) {
        Map<String, Object> param = new HashMap<>();
        param.put("corpid", UserContext.getCorpId());
        param.put("userId", UserContext.getUserId());
        param.put("projectId", projectId);
        Page<PcProjectLog> pcProjectPage = pcProjectLogService.list(page, limit, param);
        return AjaxPage.success(pcProjectPage.getList(), pcProjectPage.getTotalRow(), pcProjectPage.getTotalPage());
    }

    @ApiOperation(value = "新增成员项目日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectLogVo", dataType = "ProjectLogVo", required = true),
    })
    @PostMapping("save")
    public AjaxData save(@RequestBody ProjectLogVo vo) {
        pcProjectLogService.save(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "修改成员项目日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectLogVo", dataType = "ProjectLogVo", required = true),
    })
    @PostMapping("update")
    public AjaxData update(@RequestBody ProjectLogVo vo) {
        pcProjectLogService.update(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "修改成员项目日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectLogVo", dataType = "ProjectLogVo", required = true),
    })
    @PostMapping("delete")
    public AjaxData delete(@RequestBody ProjectLogVo vo) {
        pcProjectLogService.delete(vo.getId(), UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }


}
