package com.xykj.enterprise.wechat.wap.controller;

import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxPage;
import com.xykj.enterprise.wechat.core.model.dodb.User;
import com.xykj.enterprise.wechat.core.service.corp.UserService;
import com.ydn.dbframe.plugin.activerecord.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-29 11:36
 */
@Api(description = "企业成员管理接口")
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "查询项目列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "limit", value = "页大小", dataType = "string", paramType = "query", required = true),

    })
    @GetMapping("list")
    public AjaxPage list(Integer page, Integer limit) {
        Map<String, Object> param = new HashMap<>();
        param.put("corpid", UserContext.getCorpId());
        Page<User> userPage = userService.list(page, limit, param);
        return AjaxPage.success(userPage.getList(), userPage.getTotalRow(), userPage.getTotalPage());
    }

}
