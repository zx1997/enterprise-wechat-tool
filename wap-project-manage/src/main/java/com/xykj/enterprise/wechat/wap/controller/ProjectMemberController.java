package com.xykj.enterprise.wechat.wap.controller;

import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.base.web.domain.AjaxPage;
import com.xykj.enterprise.wechat.bean.wap.pc.ProjectMemberVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcProjectMember;
import com.xykj.enterprise.wechat.core.service.pc.PCProjectMemberService;
import com.ydn.dbframe.plugin.activerecord.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 项目成员管理相关接口
 *
 * @Author george
 * @create 2021-05-12 16:57
 */
@Api(description = "项目成员管理接口")
@RestController
@RequestMapping("projectMember")
public class ProjectMemberController {
    @Autowired
    private PCProjectMemberService pcProjectMemberService;

    @ApiOperation(value = "查询项目成员列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "limit", value = "页大小", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "projectId", value = "页大小", dataType = "string", paramType = "query", required = false),

    })
    @GetMapping("list")
    public AjaxPage<PcProjectMember> list(Integer page, Integer limit, Integer projectId) {
        Map<String, Object> param = new HashMap<>();
        param.put("corpid", UserContext.getCorpId());
        param.put("userId", UserContext.getUserId());
        param.put("projectId", projectId);
        Page<PcProjectMember> pcProjectPage = pcProjectMemberService.list(page, limit, param);
        return AjaxPage.success(pcProjectPage.getList(), pcProjectPage.getTotalRow(), pcProjectPage.getTotalPage());
    }

    @ApiOperation(value = "查询项目成员明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "项目成员ID", dataType = "string", paramType = "query", required = true),

    })
    @GetMapping("get")
    public AjaxData<PcProjectMember> get(Integer id) {
        PcProjectMember member = pcProjectMemberService.get(id, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success(member);
    }


    @ApiOperation(value = "新增项目成员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectMemberVo", dataType = "ProjectMemberVo", required = true),
    })
    @PostMapping("save")
    public AjaxData save(@RequestBody ProjectMemberVo vo) {
        pcProjectMemberService.save(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "修改项目成员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectMemberVo", dataType = "ProjectMemberVo", required = true),
    })
    @PostMapping("update")
    public AjaxData update(@RequestBody ProjectMemberVo vo) {
        pcProjectMemberService.update(vo, UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }

    @ApiOperation(value = "删除项目成员")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "vo", value = "ProjectMemberVo", dataType = "ProjectMemberVo", required = true),
    })
    @PostMapping("delete")
    public AjaxData delete(@RequestBody ProjectMemberVo vo) {
        pcProjectMemberService.delete(vo.getId(), UserContext.getCorpId(), UserContext.getUserId());
        return AjaxData.success();
    }


}
