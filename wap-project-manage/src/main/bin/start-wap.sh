#!/bin/sh

SERVER_NAME="wap-pm"
MEM_OPTS="-Xms512m -Xmx1024m"
CP="../conf/:../lib/*"

java $MEM_OPTS -Dname=$SERVER_NAME  -cp $CP com.xykj.enterprise.wechat.wap.WapPMBooter >> ../logs/$SERVER_NAME.log  &