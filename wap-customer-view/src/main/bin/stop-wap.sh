#!/bin/sh

SERVER_NAME="wap-server"
ps -ef | grep `whoami` | grep java | grep $SERVER_NAME | grep -v grep | awk '{print $2}' | while read PID
do
    kill -9 $PID 2>&1 >/dev/null
    [ "$?" == "0" ] && echo "stop  instance $SERVER_NAME successful! pid:${PID}"
done
