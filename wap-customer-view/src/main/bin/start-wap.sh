#!/bin/sh

SERVER_NAME="wap-server"
MEM_OPTS="-Xms512m -Xmx1024m"
CP="../conf/:../lib/*"

java $MEM_OPTS -Dname=$SERVER_NAME  -cp $CP com.xykj.enterprise.wechat.wap.WapBooter >> ../logs/$SERVER_NAME.log  &