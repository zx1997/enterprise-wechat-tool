package com.xykj.enterprise.wechat.wap.service;

import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import com.xykj.enterprise.wechat.bean.ext.schedule.AddScheduleVo;
import com.xykj.enterprise.wechat.bean.ext.schedule.GetScheduleVo;
import com.xykj.enterprise.wechat.client.CorpClient;
import com.ydn.appserver.Request;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @Author zhouxu
 * @create 2021-04-13 15:08
 */
@Slf4j
@Service
public class CorpService extends BaseService {

    @Value("${wx.saas.SuiteID}")
    private String SuiteID;
    @Value("${wx.saas.Secret}")
    private String Secret;

    @Autowired
    private CorpClient corpClient;

    public AjaxData<GetScheduleVo> getSchedule(String userId, String corpid, String schedule_id_list) {
        Request request = new Request("GetSchedule");
        request.setParameter("userId", userId);
        request.setParameter("corpid", corpid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("schedule_id_list", schedule_id_list);
        return convertData(corpClient.send(request), GetScheduleVo.class);
    }

    public AjaxData addSchedule(String userId, String corpid, String scheduleJson) {
        Request request = new Request("AddSchedule");
        request.setParameter("userId", userId);
        request.setParameter("corpid", corpid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("schedule", scheduleJson);
        return convertData(corpClient.send(request), AddScheduleVo.class);
    }

    public AjaxData updateSchedule(String userId, String corpid, String scheduleJson) {
        Request request = new Request("UpdateSchedule");
        request.setParameter("userId", userId);
        request.setParameter("corpid", corpid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("schedule", scheduleJson);
        return convertData(corpClient.send(request), BaseResp.class);
    }

    public AjaxData<GetScheduleVo> getScheduleByCalendar(String userId, String corpid, String cal_id, Integer offset, Integer limit) {
        Request request = new Request("GetScheduleByCalendar");
        request.setParameter("userId", userId);
        request.setParameter("corpid", corpid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("cal_id", cal_id);
        request.setParameter("offset", offset);
        request.setParameter("limit", limit);
        return convertData(corpClient.send(request), GetScheduleVo.class);
    }

    public AjaxData test() {
        log.debug("corpClient toString:{}", corpClient.toString());
        return null;
    }

}
