package com.xykj.enterprise.wechat.wap.config;

import com.xykj.enterprise.wechat.base.web.interceptor.CorsInterceptor;
import com.xykj.enterprise.wechat.base.web.interceptor.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * 拦截器配置
 *
 * @author Feng Chen
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Autowired
    private TokenInterceptor tokenInterceptor;

    @Autowired
    private CorsInterceptor corsInterceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(corsInterceptor).addPathPatterns("/**");

        registry.addInterceptor(tokenInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(
                        "/test/data",
                        "/test/page",
                        "/user/loginByPwd",
                        "/user/loginByCode",
                        "/user/getVerify",
                        "/user/verify",
                        "/user/registerAddLoginPwd",
                        "/user/updateLoginPwd",
                        "/user/updateTradePwd",
                        "/user/unionLogin",
                        "/user/unionRegister",
                        "/user/unionRegisterAddPwd",
                        "/user/loginByQQ",
                        "/user/loginByWechat",
                        "/user/getAppVersion",
                        "/user/loginOut",
                        "/user/register",
                        "/video/listHot",
                        "/user/getFederationToken",
                        "/text/aboutUs",
                        "/text/tutorial",
                        "/text/textCode",
                        "/text/getospel",
                        "/text/problem",
                        "/text/getTestKind",
                        "/user/checkMobile",
                        "/user/getAesCiphertext",
                        "/video/listFoundByPhp",
                        "/video/actionByPhp",
                        "/video/deleteByEs",
                        "/video/addVideoEs"
                )
                .excludePathPatterns(
                        "/swagger-resources/**",
                        "/webjars/**",
                        "/v2/**",
                        "/swagger-ui.html/**"
                )
                // 支付回调
                .excludePathPatterns(
                        "/pay/wxPayNotify",
                        "/pay/aliPayNotify",
                        "/pay/adapayNotify",
                        "/pay/adapayDrawcashNotify"
                )
                // 企业内部开发回调
                .excludePathPatterns(
                        "/customerWxpush/**",
                        "/contactsWxpush/**"
                )
                // 第三方服务回调
                .excludePathPatterns(
                        "/saasEvent/**",
                        "/saasData/**"
                )
                // 配置信息
                .excludePathPatterns(
                        "/common/loginWechat",
                        "/common/loginThird"
                )
                // 第三方授权
                .excludePathPatterns(
                        "/identity/**"
                )
                .excludePathPatterns(
                        "/**/test"
                )
        ;

        super.addInterceptors(registry);
    }


    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        super.addResourceHandlers(registry);
    }
}
