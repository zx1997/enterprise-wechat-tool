package com.xykj.enterprise.wechat.wap.controller;

import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.base.web.domain.AjaxPage;
import com.xykj.enterprise.wechat.bean.wap.customer.SaveFollowLogModel;
import com.xykj.enterprise.wechat.wap.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author george
 * @create 2021-04-14 11:28
 */
@Api(description = "客户接口")
@RestController
@RequestMapping("customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @ApiOperation(value = "客户信息查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "externalUserid", value = "客户ID", dataType = "string", paramType = "query", required = true),
    })
    @GetMapping("getCustomerInfo")
    public AjaxData getCustomerInfo(String externalUserid) {
        return customerService.getCustomerInfo(UserContext.getUserId(), UserContext.getCorpId(), externalUserid);
    }

    @ApiOperation(value = "企业标签列表")
    @ApiImplicitParams({
    })
    @GetMapping("findAllGroupTag")
    public AjaxPage findAllGroupTag() {
        return customerService.findAllGroupTag(UserContext.getCorpId());
    }

    @ApiOperation(value = "删除跟踪记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "logId", value = "logId", dataType = "string", paramType = "query", required = true),
    })
    @GetMapping("deleteFollowLog")
    public AjaxData deleteFollowLog(Long logId) {
        return customerService.deleteFollowLog(UserContext.getUserId(), UserContext.getCorpId(), logId);
    }

    @ApiOperation(value = "新增跟进记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "model", value = "SaveFollowLogModel", dataType = "SaveFollowLogModel", required = true),
    })
    @PostMapping("saveFollowLog")
    private AjaxData saveFollowLog(@RequestBody SaveFollowLogModel model) {
        return customerService.saveFollowLog(
                UserContext.getUserId(),
                UserContext.getCorpId(),
                model.getLogId(),
                model.getCustomerId(),
                model.getFollowContent(),
                model.getFollowStatus(),
                model.getScheduleId()
        );
    }

    @ApiOperation(value = "查询跟踪记录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "externalUserid", value = "客户ID", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "page", value = "当前页", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "limit", value = "页大小", dataType = "string", paramType = "query", required = true),

    })
    @GetMapping("listFollowLogs")
    public AjaxPage listFollowLogs(String externalUserid, Integer page, Integer limit) {
        return customerService.listFollowLogs(UserContext.getUserId(), UserContext.getCorpId(), externalUserid, page, limit);
    }


}
