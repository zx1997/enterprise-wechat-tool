package com.xykj.enterprise.wechat.wap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Feng Chen
 */
@SpringBootApplication
@ComponentScan("com.xykj.enterprise.wechat")
public class WapBooter {

    public static void main(String[] args) {
        SpringApplication.run(WapBooter.class);
    }

}
