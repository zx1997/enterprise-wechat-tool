CREATE TABLE `t_customer_follow_user` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID' ,
`corp_id`  bigint(11) NOT NULL DEFAULT 0 COMMENT '企业ID' ,
`customer_id`  bigint(11) NOT NULL COMMENT '客户ID' ,
`userid`  bigint(11) NOT NULL COMMENT '员工id' ,
`remark`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客户备注' ,
`description`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客户描述信息' ,
`createtime`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`remark_corp_name`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注企业名称' ,
`remark_mobiles`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注手机号' ,
`oper_userid`  bigint(11) NULL DEFAULT NULL COMMENT '发起添加的userid' ,
`add_way`  varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 0 COMMENT '添加客户的来源 0-未知来源 1-扫描二维码 2-搜索手机号 3-名片分享 4-群聊 5-手机通讯录 6-微信联系人 7-来自微信的添加好友申请 8-安装第三方应用时自动添加的客服人员 9-搜索邮箱 201-内部成员共享 202' ,
`state`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业自定义的state参数，用于区分客户具体是通过哪个「联系我」添加，由企业通过创建「联系我」方式指定' ,
`tags`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '该成员添加此外部联系人所打标签' ,
PRIMARY KEY (`id`)
)
;

