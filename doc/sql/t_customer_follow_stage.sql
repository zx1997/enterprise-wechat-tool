CREATE TABLE `t_customer_follow_stage` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'stage ID' ,
`code`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跟进阶段code' ,
`name`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跟进阶段' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`mark`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息' ,
PRIMARY KEY (`id`)
)
;