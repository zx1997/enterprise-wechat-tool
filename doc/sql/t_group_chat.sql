CREATE TABLE `t_group_chat` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID' ,
`corp_id`  bigint(11) NOT NULL DEFAULT 0 COMMENT '企业ID' ,
`chat_id`  bigint(11) NULL COMMENT '客户群ID' ,
`name`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '群名' ,
`owner`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '群主ID' ,
`notice`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '群公告' ,
`mark`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '群备注信息' ,
`member_list`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '群成员列表' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
PRIMARY KEY (`id`)
)
;

