CREATE TABLE `t_customer_behavior` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '轨迹主键ID' ,
`customer_id`  bigint(11) NOT NULL COMMENT '客户ID' ,
`object_type`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT  '目标类型' ,
`object_id`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '目标id' ,
`time`  bigint(11) NOT NULL DEFAULT 0 COMMENT '时长' ,
`share`  bigint(11) NOT NULL DEFAULT 0 COMMENT '分享次数' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`mark`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息' ,
PRIMARY KEY (`id`)
)
;