CREATE TABLE `t_corp` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID' ,
`corp_id`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业ID' ,
`corp_name`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '企业名称' ,
`corp_sercet`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '企业Secret' ,
`corp_type`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司类型' ,
`corp_square_logo_url`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司logo' ,
`access_token`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`expire_at`  datetime NULL DEFAULT NULL COMMENT '失效时间' ,
PRIMARY KEY (`id`)
)
;

