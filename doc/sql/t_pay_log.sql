CREATE TABLE `t_pay_log` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID' ,
`corp_id`  bigint(11) NOT NULL DEFAULT 0 COMMENT '企业ID' ,
`customer_id`  bigint(11) NOT NULL DEFAULT 0 COMMENT '客户ID' ,
`order_id`  bigint(11) DEFAULT NULL COMMENT '订单ID' ,
`content`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跟进详情、内容' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`mark`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息' ,
PRIMARY KEY (`id`)
)
;