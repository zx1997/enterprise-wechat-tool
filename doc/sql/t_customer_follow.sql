CREATE TABLE `t_customer_follow` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '跟进主键ID' ,
`customer_id`  bigint(11) NOT NULL COMMENT '客户ID' ,
`follow_user_id`  bigint(11) NULL DEFAULT NULL COMMENT '跟进人ID' ,
`stage`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT  '跟进阶段，对应t_customer_follow_stage中的code' ,
`content`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '跟进详情、内容' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`mark`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息' ,
PRIMARY KEY (`id`)
)
;