CREATE TABLE `t_customer_tag` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id' ,
`customer_id`  bigint(11) NOT NULL DEFAULT 0 COMMENT '客户id' ,
`tag_id`  bigint(11) NOT NULL DEFAULT 0 COMMENT '标签id' ,
`tag_name`  varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标签组名称' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`deleted`  char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 0 COMMENT '是否已经被删除 0-正常 1-被删除' ,
PRIMARY KEY (`id`)
)
;