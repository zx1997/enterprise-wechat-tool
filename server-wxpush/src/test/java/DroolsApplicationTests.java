import com.xykj.enterprise.wechat.bean.busi.vo.People;
import com.xykj.enterprise.wechat.bean.busi.vo.WxpushVo;
import org.drools.core.base.RuleNameEqualsAgendaFilter;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author zhouxu
 * @create 2021-02-21 14:47
 */
//@Slf4j
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({"classpath:spring/app-config.xml"})
public class DroolsApplicationTests {

    @Autowired
    private KieSession session;

//    @Autowired
//    private KieBase kieBase;


//    @Test
    public void wxpushVo() {
        WxpushVo vo = new WxpushVo();
        vo.setMsgType("event");
        vo.setEvent("change_external_contact");
        vo.setChangeType("add_external_contact");
        session.insert(vo);
        Integer count = session.fireAllRules(new RuleNameStartsWithAgendaFilter("客户联系"));
//        log.debug("触发规则条数：{},vo:{}", count, vo);
    }

//    @Test
    public void people() {

        People people = new People();
        people.setName("达");
        people.setSex(0);
        people.setDrlType("people");
//        log.info("people:{}", people.toString());
        session.insert(people);//插入
        session.fireAllRules(new RuleNameEqualsAgendaFilter("girl"));//执行规则

    }


//    @Test
    public void collect() {
        session.insert(new People(1, "达", 15, "collect"));
        session.insert(new People(0, "秋", 15, "collect"));
        session.insert(new People(0, "春", 15, "collect"));
        session.insert(new People(1, "夏", 15, "collect"));
        session.insert(new People(0, "冬", 15, "collect"));
        session.insert(new People(3, "金", 15, "collect"));

        session.fireAllRules();//执行规则
    }

//    @Test
    public void diyaccumulate() {
        session.insert(new People(1, "达", 26, "diyaccumulate"));
        session.insert(new People(0, "秋", 18, "diyaccumulate"));
        session.insert(new People(0, "春", 38, "diyaccumulate"));
        session.insert(new People(1, "夏", 90, "diyaccumulate"));
        session.insert(new People(0, "冬", 55, "diyaccumulate"));
        session.insert(new People(3, "金", 12, "diyaccumulate"));

        session.fireAllRules();//执行规则
    }

//    @Test
    public void update() {

        People people = new People();
        people.setName("达");
        people.setSex(0);
        people.setAge(17);
        people.setDrlType("update");
        session.insert(people);//插入
        session.fireAllRules();//执行规则
        System.out.println("test执行====" + people.toString());
    }

//    @Test
    public void impot() {
        People people = new People();
        people.setDrlType("impot");
        session.insert(people);//插入
        session.fireAllRules();//执行规则
    }

//    @AfterEach
//    public void runDispose() {
//        session.dispose();//释放资源
//    }
}

