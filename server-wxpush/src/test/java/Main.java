import lombok.Data;

/**
 * @Author george
 * @create 2021-05-13 10:28
 */
@Data
public class Main {

    private Hoop hoop;

    public void test(){
        hoop.test();
    }

//    public static void main(String[] args){
//        Main main = new Main();
//        main.setHoop(new HoopSon());
//        main.test();
//
//
//    }

}
