#!/bin/sh

SERVER_NAME="server-wxpush"
PORT=8195
MEM_OPTS="-Xms512m -Xmx1024m"
CP="../conf/:../lib/*"

java $MEM_OPTS -Dname=$SERVER_NAME  -cp $CP com.ydn.appserver.DefaultServerMain -port ${PORT} >> ../logs/$SERVER_NAME.log  &