package com.xykj.enterprise.wechat.wxpush.proc.customer;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetExternalContactBatchVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetFollowUserListVo;
import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.xykj.enterprise.wechat.util.wecom.customer.ExternalContactUtil;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author zhouxu
 * @create 2021-04-08 09:54
 */
@Slf4j
public class SyncCustomerProc implements ItsProc {
    @Autowired
    private CustomerService customerService;

    @Override
    public void execute(String message) throws Exception {
        log.debug(message);
        JSONObject jsonObject = JSONObject.parseObject(message);
        String accessToken = jsonObject.getString("access_token");
        String corpid = jsonObject.getString("corpid");

        // 1.查询员工列表
        GetFollowUserListVo listVo = ExternalContactUtil.getFollowUserList(accessToken);
        for (String userid : listVo.getFollow_user()) {
            // 2.根据员工，查询客户列表
            GetExternalContactBatchVo batchVo = ExternalContactUtil.getExternalContactBatch(accessToken, userid, null, 1000);
            // 3.同步客户信息
            customerService.syncCustomerList(batchVo, corpid, userid);
        }

    }
}
