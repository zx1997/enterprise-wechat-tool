package com.xykj.enterprise.wechat.wxpush.proc.customer;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.ExternalContactVo;
import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.service.corp.CorpService;
import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.xykj.enterprise.wechat.core.service.identity.IdentityService;
import com.xykj.enterprise.wechat.util.wecom.customer.ExternalContactUtil;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 外部联系人免验证添加成员事件
 * 外部联系人添加了配置了客户联系功能且开启了免验证的成员时（此时成员尚未确认添加对方为好友），回调该事件
 *
 * @Author zhouxu
 * @create 2021-02-25 14:16
 */
@Slf4j
public class AddHalfExternalContactProc implements ItsProc {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private IdentityService identityService;

    @Override
    public void execute(String message) throws Exception {
        log.debug("message:{}", message);
        JSONObject jsonObject = JSONObject.parseObject(message);

        String corpid = jsonObject.getString("AuthCorpId");
        String suiteId = jsonObject.getString("SuiteId");
        String secret = jsonObject.getString("secret");
        // access_token
        String accessToken = identityService.getAccessToken(corpid, suiteId, secret);

        log.debug("外部联系人免验证添加成员事件.....");
        String userid = jsonObject.getString("UserID");
        String externalUserid = jsonObject.getString("ExternalUserID");

        // 1.查询客户详情
        ExternalContactVo vo = ExternalContactUtil.getExternalContact(accessToken, externalUserid);
        // 2.新增客户
        customerService.save(corpid, vo);
    }
}
