package com.xykj.enterprise.wechat.wxpush.proc.group;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GroupChatInfo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GroupChatListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GroupChatVo;
import com.xykj.enterprise.wechat.core.service.group.GroupChatService;
import com.xykj.enterprise.wechat.util.wecom.customer.ExternalContactUtil;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 同步客户群
 *
 * @Author zhouxu
 * @create 2021-04-12 10:45
 */
@Slf4j
public class SyncGroupChatProc implements ItsProc {

    @Autowired
    private GroupChatService groupChatService;


    @Override
    public void execute(String message) throws Exception {
        log.debug(message);
        JSONObject jsonObject = JSONObject.parseObject(message);
        String accessToken = jsonObject.getString("access_token");
        String corpid = jsonObject.getString("corpid");

        // 1.查询客户群列表
        GroupChatListVo listVo = ExternalContactUtil.getGroupChatList(accessToken, null, null, null, 1000);
        for (GroupChatInfo info : listVo.getGroup_chat_list()) {
            GroupChatVo chatVo = ExternalContactUtil.getGroupChat(accessToken, info.getChat_id());
            // 2.客户组数据同步
            groupChatService.save(corpid, chatVo);
        }

        groupChatService.syncSucc(corpid);

    }
}
