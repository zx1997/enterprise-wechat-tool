package com.xykj.enterprise.wechat.wxpush.api;

import com.alibaba.fastjson.JSON;
import com.xykj.enterprise.wechat.bean.busi.vo.wxpush.SaasWxpushVo;
import com.xykj.enterprise.wechat.core.service.wxpush.WxpushService;
import com.xykj.enterprise.wechat.util.wxpush.PayKit;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import com.ydn.its.ItsCluster;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-02-26 11:15
 */
@Function(description = "第三方应用-事件回调", parameters = {
        @Parameter(name = "msgSignature", type = Type.String, description = "签名文本", required = true),
        @Parameter(name = "timestamp", type = Type.String, description = "时间戳", required = true),
        @Parameter(name = "nonce", type = Type.String, description = "随机数", required = true),
        @Parameter(name = "data", type = Type.String, description = "消息内容(加密)", required = true),
        @Parameter(name = "msg", type = Type.String, description = "消息内容(解密)", required = true),

        @Parameter(name = "suite_id", type = Type.String, description = "应用suite_id", required = true),
        @Parameter(name = "secret", type = Type.String, description = "secret", required = true),
})
@Component
@Slf4j
public class SaasEventWxpush implements Action {


    @Autowired
    private KieSession session;
    @Autowired
    private WxpushService wxpushService;
    @Autowired
    private ItsCluster its;

    @Override
    public Response execute(Request request) throws Exception {

        String msgSignature = request.getString("msgSignature");
        String timestamp = request.getString("timestamp");
        String nonce = request.getString("nonce");
        String data = request.getString("data");
        String msg = request.getString("msg");
        // xml消息文本解析
        Map<String, String> params = PayKit.xmlToMap(msg);

        String infoType = params.get("InfoType");
        String event = params.get("Event");
        String changeType = params.get("ChangeType");

        params.put("suite_id", request.getString("suite_id"));
        params.put("secret", request.getString("secret"));

        // 日志服务-记录日志
        String jsonMsg = JSON.toJSONString(params);
        log.debug("params:{}", jsonMsg);
        wxpushService.saveLog(msgSignature, timestamp, nonce, data, msg, jsonMsg, infoType, event, changeType);

        // 规则引擎-分析匹配
        SaasWxpushVo vo = new SaasWxpushVo();
        vo.setInfoType(infoType);
        vo.setChangeType(changeType);
        session.insert(vo);//插入
//        session.fireAllRules(new RuleNameStartsWithAgendaFilter("推送suite_ticket"));//执行规则（指定前缀）
        session.fireAllRules();

        // 触发异步事件处理
        try {
            its.pub(vo.getAction(), jsonMsg);
        } catch (Exception e) {
            log.error("", e);
            return Response.fail("操作异常");
        }

        return Response.success();
    }
}
