package com.xykj.enterprise.wechat.wxpush.proc.customer;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.service.corp.CorpService;
import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.xykj.enterprise.wechat.core.service.identity.IdentityService;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 删除企业客户事件
 * 配置了客户联系功能的成员删除外部联系人时，回调该事件
 *
 * @Author george
 * @create 2021-04-22 09:40
 */
@Slf4j
public class DelExternalContactProc implements ItsProc {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private IdentityService identityService;

    @Override
    public void execute(String message) throws Exception {
        log.debug("message:{}", message);
        JSONObject jsonObject = JSONObject.parseObject(message);

        String corpid = jsonObject.getString("AuthCorpId");
        String suiteId = jsonObject.getString("SuiteId");
        String secret = jsonObject.getString("secret");
        // access_token
        String accessToken = identityService.getAccessToken(corpid, suiteId, secret);

        log.debug("删除企业客户事件.....");
        String userid = jsonObject.getString("UserID");
        String externalUserid = jsonObject.getString("ExternalUserID");


        // 删除客户
        customerService.delete(corpid, externalUserid);
    }

}
