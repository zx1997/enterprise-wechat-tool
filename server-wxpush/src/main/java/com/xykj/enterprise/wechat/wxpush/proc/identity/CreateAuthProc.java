package com.xykj.enterprise.wechat.wxpush.proc.identity;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.busi.vo.wxpush.CreateAuthVo;
import com.xykj.enterprise.wechat.bean.ext.identity.AdminListVo;
import com.xykj.enterprise.wechat.bean.ext.identity.Agent;
import com.xykj.enterprise.wechat.bean.ext.identity.GetPermanentCodeVo;
import com.xykj.enterprise.wechat.bean.ext.identity.SuiteAccessToken;
import com.xykj.enterprise.wechat.core.model.dodb.SuiteInfo;
import com.xykj.enterprise.wechat.core.service.corp.AgentService;
import com.xykj.enterprise.wechat.core.service.corp.CorpService;
import com.xykj.enterprise.wechat.core.service.identity.SuiteService;
import com.xykj.enterprise.wechat.core.service.identity.SuiteTicketService;
import com.xykj.enterprise.wechat.util.wecom.identity.AgentAdminUtil;
import com.xykj.enterprise.wechat.util.wecom.identity.PermanentCodeUtil;
import com.xykj.enterprise.wechat.util.wecom.identity.SuiteAccessTokenUtil;
import com.ydn.dbframe.plugin.activerecord.Db;
import com.ydn.its.ItsProc;
import com.ydn.simplecache.SimpleCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 企业安装应用
 * 1.保存永久授权码
 * 2.保存应用信息 agentInfo
 * 3.保存应用管理员信息 agentAdminList
 *
 * @Author zhouxu
 * @create 2021-04-01 10:22
 */
@Slf4j
public class CreateAuthProc implements ItsProc {

    @Autowired
    private SuiteTicketService suiteTicketService;
    @Autowired
    private CorpService corpService;
    @Autowired
    private SimpleCache simpleCache;
    @Autowired
    private SuiteService suiteService;
    @Autowired
    private AgentService agentService;

    @Override
    public void execute(String message) throws Exception {
        log.debug(message);
        CreateAuthVo vo = JSONObject.parseObject(message, CreateAuthVo.class);

        SuiteInfo suiteInfo = suiteService.getSecret(vo.getSuiteId());

        // 获取suiteAccessToken
        SuiteAccessToken suiteAccessToken = SuiteAccessTokenUtil.get(
                simpleCache,
                vo.getSuiteId(),
                suiteInfo.getSecret(),
                suiteTicketService.getLatest(vo.getSuiteId()).getSuitTicket()
        );

        // 获取permanentCode
        GetPermanentCodeVo getPermanentCodeVo = PermanentCodeUtil.get(
                vo.getAuthCode(),
                suiteAccessToken.getSuite_access_token()
        );

        String corpid = getPermanentCodeVo.getAuth_corp_info().getCorpid();
        String suiteId = vo.getSuiteId();
        Agent agent = getPermanentCodeVo.getAuth_info().getAgent().get(0);

        // 获取应用管理员
        AdminListVo adminListVo = AgentAdminUtil.getAdminList(
                corpid,
                suiteId,
                String.valueOf(agent.getAgentid())
        );


        Db.tx(() -> {

            // 存储企业信息、永久授权码
            corpService.save(
                    suiteId,
                    getPermanentCodeVo
            );

            // 初始化企业扩展信息
            corpService.initExtInfo(corpid);
            // 企业应用授权信息存储
            agentService.saveAgentInfo(
                    agent,
                    corpid,
                    suiteId
            );

            // 存储管理员信息
            agentService.saveAgentAdmin(
                    adminListVo.getAdmin(),
                    String.valueOf(agent.getAgentid()),
                    corpid,
                    suiteId
            );

            return true;
        });

    }
}