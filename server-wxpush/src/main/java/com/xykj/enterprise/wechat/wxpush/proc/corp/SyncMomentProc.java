package com.xykj.enterprise.wechat.wxpush.proc.corp;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentListVo;
import com.xykj.enterprise.wechat.core.service.corp.MomentService;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import com.xykj.enterprise.wechat.util.wecom.customer.ExternalContactUtil;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * 同步朋友圈数据
 *
 * @Author george
 * @create 2021-05-10 14:16
 */
@Slf4j
public class SyncMomentProc implements ItsProc {

    @Autowired
    private MomentService momentService;

    @Override
    public void execute(String message) throws Exception {
        log.debug(message);
        JSONObject jsonObject = JSONObject.parseObject(message);
        String accessToken = jsonObject.getString("access_token");
        String corpid = jsonObject.getString("corpid");

        GetMomentListVo listVo = ExternalContactUtil.getMomentList(
                accessToken,
                TimeUtil.getTimeSecond(TimeUtil.getPastDate(30)),
                TimeUtil.getTimeSecond(new Date()),
                null,
                null,
                null
        );

        momentService.save(corpid,listVo);

    }
}
