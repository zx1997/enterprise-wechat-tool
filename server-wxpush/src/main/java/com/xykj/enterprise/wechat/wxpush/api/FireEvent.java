package com.xykj.enterprise.wechat.wxpush.api;

import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import com.ydn.its.ItsCluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Feng Chen
 */
@Function(description = "触发事件", parameters = {
        @Parameter(name = "topic", type = Type.String, description = "事件主题，需提前注册", required = true),
        @Parameter(name = "message", type = Type.String, description = "事件内容, json 串", required = true)
})
@Component
public class FireEvent implements Action {

    @Autowired
    private ItsCluster its;

    private static final Logger logger = LoggerFactory.getLogger(FireEvent.class);

    @Override
    public Response execute(Request request) throws Exception {

        String topic = request.getString("topic");
        String message = request.getString("message");

        try {
            its.pub(topic, message);
            return Response.success();
        } catch (Exception e) {
            logger.error("", e);
            return Response.fail("操作异常");
        }

    }

}
