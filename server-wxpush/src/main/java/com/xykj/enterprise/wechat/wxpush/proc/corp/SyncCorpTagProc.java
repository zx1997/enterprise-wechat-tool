package com.xykj.enterprise.wechat.wxpush.proc.corp;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetCorpTagListVo;
import com.xykj.enterprise.wechat.core.service.corp.CorpTagService;
import com.xykj.enterprise.wechat.util.wecom.customer.ExternalContactUtil;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 同步企业标签
 *
 * @Author zhouxu
 * @create 2021-04-12 10:45
 */
@Slf4j
public class SyncCorpTagProc implements ItsProc {

    @Autowired
    private CorpTagService corpTagService;

    @Override
    public void execute(String message) throws Exception {

        JSONObject jsonObject = JSONObject.parseObject(message);
        String accessToken = jsonObject.getString("access_token");
        String corpid = jsonObject.getString("corpid");

        // 1.查询企业标签
        GetCorpTagListVo listVo = ExternalContactUtil.getCorpTagList(accessToken, null, null);
        // 2.同步数据
        corpTagService.syncCorpTag(listVo, corpid);
    }
}
