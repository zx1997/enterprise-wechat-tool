package com.xykj.enterprise.wechat.wxpush.proc.corp;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.UserListVo;
import com.xykj.enterprise.wechat.core.service.corp.UserService;
import com.xykj.enterprise.wechat.util.wecom.contacts.ContactsUserUtil;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 批量同步企业员工信息
 *
 * @Author zhouxu
 * @create 2021-04-06 17:05
 */
@Slf4j
public class SyncUserProc implements ItsProc {

    @Autowired
    private UserService userService;

    @Override
    public void execute(String message) throws Exception {
        log.debug(message);
        JSONObject jsonObject = JSONObject.parseObject(message);
        String accessToken = jsonObject.getString("access_token");
        String corpid = jsonObject.getString("corpid");
        Integer department_id = jsonObject.getInteger("department_id");
        Integer fetch_child = jsonObject.getInteger("fetch_child ");

        // 1.查询用户列表
        UserListVo listVo = ContactsUserUtil.userList(accessToken, department_id, fetch_child);
        // 2.同步用户信息
        userService.syncUserList(listVo, corpid);

    }
}
