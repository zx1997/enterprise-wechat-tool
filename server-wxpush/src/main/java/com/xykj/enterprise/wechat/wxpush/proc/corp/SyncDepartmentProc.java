package com.xykj.enterprise.wechat.wxpush.proc.corp;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.contacts.department.DepartmentListVo;
import com.xykj.enterprise.wechat.core.service.corp.DepartmentService;
import com.xykj.enterprise.wechat.util.wecom.contacts.ContactsDepartmentUtil;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 批量同步部门信息
 *
 * @Author zhouxu
 * @create 2021-04-06 17:05
 */
@Slf4j
public class SyncDepartmentProc implements ItsProc {

    @Autowired
    private DepartmentService departmentService;

    @Override
    public void execute(String message) throws Exception {
        log.debug(message);
        JSONObject jsonObject = JSONObject.parseObject(message);
        String accessToken = jsonObject.getString("access_token");
        String corpid = jsonObject.getString("corpid");
        Integer id = jsonObject.getInteger("id");

        // 1.查询部门列表
        DepartmentListVo listVo = ContactsDepartmentUtil.departmentList(accessToken, id);
        // 2.同步部门信息
        departmentService.syncDepartList(listVo, corpid);

    }
}
