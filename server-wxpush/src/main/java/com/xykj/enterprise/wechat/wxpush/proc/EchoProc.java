package com.xykj.enterprise.wechat.wxpush.proc;

import com.ydn.its.ItsProc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 测试
 */
public class EchoProc implements ItsProc {

    private static final Logger logger = LoggerFactory.getLogger(EchoProc.class);

    @Override
    public void execute(String message) throws Exception {
        logger.info(message);
    }

}
