package com.xykj.enterprise.wechat.wxpush.proc.corp.user;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.busi.vo.wxpush.user.CreateUserVo;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author zhouxu
 * @create 2021-03-29 15:10
 */
@Slf4j
public class CreateUserProc implements ItsProc {


    @Override
    public void execute(String message) throws Exception {
        log.info(message);
        CreateUserVo vo = JSONObject.parseObject(message, CreateUserVo.class);
        log.info("vo toString:{}", vo.toString());
        // TODO 会员新增

    }
}
