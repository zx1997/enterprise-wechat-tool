package com.xykj.enterprise.wechat.wxpush.proc.identity;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.busi.vo.wxpush.SuiteTicketVo;
import com.xykj.enterprise.wechat.core.service.identity.SuiteTicketService;
import com.ydn.its.ItsProc;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 获取最新suite_ticket
 *
 * @Author zhouxu
 * @create 2021-03-29 16:03
 */
@Slf4j
public class SuiteTicketProc implements ItsProc {

    @Autowired
    private SuiteTicketService suiteTicketService;

    @Override
    public void execute(String message) throws Exception {
        log.debug(message);
        SuiteTicketVo vo = JSONObject.parseObject(message, SuiteTicketVo.class);
        // 保存 suite_ticket
        suiteTicketService.create(vo);

    }
}
