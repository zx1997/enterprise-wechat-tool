#!/bin/sh

SERVER_NAME="wap-brochures"
MEM_OPTS="-Xms512m -Xmx1024m"
CP="../conf/:../lib/*"

java $MEM_OPTS -Dname=$SERVER_NAME  -cp $CP com.xykj.enterprise.wechat.wap.brochures.WapBrochuresBooter >> ../logs/$SERVER_NAME.log  &