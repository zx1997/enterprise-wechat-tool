package com.xykj.enterprise.wechat.wap.brochures.service;

import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.base.web.domain.AjaxPage;
import com.xykj.enterprise.wechat.bean.wap.vo.CorpTagVo;
import com.xykj.enterprise.wechat.bean.wap.vo.CustomerFollowLogVo;
import com.xykj.enterprise.wechat.bean.wap.vo.GetCustomerInfoVo;
import com.xykj.enterprise.wechat.client.CustomerClient;
import com.ydn.appserver.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @Author zhouxu
 * @create 2021-02-23 17:47
 */
@Service
public class CustomerService extends BaseService {

    @Value("${wx.saas.SuiteID}")
    private String SuiteID;
    @Value("${wx.saas.Secret}")
    private String Secret;

    @Autowired
    private CustomerClient customerClient;

    public AjaxData<GetCustomerInfoVo> getCustomerInfo(String userId, String corpid, String externalUserid) {
        Request request = new Request("GetCustomerInfo");
        request.setParameter("userid", userId);
        request.setParameter("corpid", corpid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("externalUserid", externalUserid);
        return convertData(customerClient.send(request), GetCustomerInfoVo.class);
    }

    public AjaxPage findAllGroupTag(String corpId) {
        Request request = new Request("FindAllGroupTag");
        request.setParameter("corpid", corpId);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        return convertPage(customerClient.send(request), CorpTagVo.class);
    }

    public AjaxData deleteFollowLog(String userId, String corpid, Long logId) {
        Request request = new Request("DeleteFollowLog");
        request.setParameter("userid", userId);
        request.setParameter("corpid", corpid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("logId", logId);
        return convertData(customerClient.send(request));
    }

    public AjaxData saveFollowLog(String userId, String corpid, Long logId, String customerId, String followContent, String followStatus, String scheduleId) {
        Request request = new Request("SaveFollowLog");
        request.setParameter("userid", userId);
        request.setParameter("corpid", corpid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("logId", logId);
        request.setParameter("customerId", customerId);
        request.setParameter("followContent", followContent);
        request.setParameter("followStatus", followStatus);
        request.setParameter("scheduleId", scheduleId);

        return convertData(customerClient.send(request));
    }

    public AjaxPage listFollowLogs(String userid, String corpid, String externalUserid, Integer page, Integer limit) {
        Request request = new Request("ListFollowLogs");
        request.setParameter("corpid", corpid);
        request.setParameter("userid", userid);
        request.setParameter("suite_id", SuiteID);
        request.setParameter("secret", Secret);
        request.setParameter("externalUserid", externalUserid);
        request.setParameter("page", page);
        request.setParameter("limit", limit);
        return convertPage(customerClient.send(request), CustomerFollowLogVo.class);
    }

}
