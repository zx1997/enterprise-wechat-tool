package com.xykj.enterprise.wechat.wap.brochures.controller;

import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.bean.wap.JsonPostModel;
import com.xykj.enterprise.wechat.wap.brochures.service.CorpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * @Author zhouxu
 * @create 2021-03-29 22:32
 */
@Api(description = "日程接口")
@RestController
@RequestMapping("schedule")
@Slf4j
public class ScheduleController {

    @Autowired
    private CorpService corpService;

    @ApiOperation(value = "获取日程详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "schedule_id_list", value = "数组形式，例如：schedule_id_list=abcd,efgd,dss", paramType = "query", dataType = "String", required = true),
    })
    @GetMapping("getSchedule")
    public AjaxData getSchedule(String schedule_id_list) {
        return corpService.getSchedule(UserContext.getUserId(), UserContext.getCorpId(), schedule_id_list);
    }

    @ApiOperation(value = "创建日程")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "jsonData", value = "日程对象参数-json格式(两次encode)", dataType = "JsonPostModel", required = true),
    })
    @PostMapping("addSchedule")
    public AjaxData getSchedule(@RequestBody JsonPostModel jsonData) throws UnsupportedEncodingException {
        log.debug("解码前：{},解码串：{}", jsonData.getJsonData(), URLDecoder.decode(URLDecoder.decode(jsonData.getJsonData(), "UTF-8")));
        return corpService.addSchedule(UserContext.getUserId(), UserContext.getCorpId(), jsonData.getJsonData());
    }

    @ApiOperation(value = "修改日程")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "jsonData", value = "日程对象参数-json格式(两次encode)", dataType = "JsonPostModel", required = true),
    })
    @PostMapping("updateSchedule")
    public AjaxData updateSchedule(@RequestBody JsonPostModel jsonData) throws UnsupportedEncodingException {
        log.debug("解码前：{},解码串：{}", jsonData.getJsonData(), URLDecoder.decode(URLDecoder.decode(jsonData.getJsonData(), "UTF-8")));
        return corpService.updateSchedule(UserContext.getUserId(), UserContext.getCorpId(), jsonData.getJsonData());
    }

    @ApiOperation(value = "修改日程")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cal_id", value = "日历ID", dataType = "string", paramType = "query", required = true),
            @ApiImplicitParam(name = "offset", value = "分页，偏移量, 默认为0", dataType = "int", paramType = "query", required = false),
            @ApiImplicitParam(name = "limit", value = "分页，预期请求的数据量，默认为500，取值范围 1 ~ 1000", dataType = "int", paramType = "query", required = false),
    })
    @PostMapping("getScheduleByCalendar")
    public AjaxData getScheduleByCalendar(String cal_id, Integer offset, Integer limit) {
        return corpService.getScheduleByCalendar(UserContext.getUserId(), UserContext.getCorpId(), cal_id, offset, limit);
    }

    @GetMapping("test")
    public AjaxData test() {
        return corpService.test();
    }

}
