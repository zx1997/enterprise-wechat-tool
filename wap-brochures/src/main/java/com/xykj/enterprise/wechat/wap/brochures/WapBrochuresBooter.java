package com.xykj.enterprise.wechat.wap.brochures;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Feng Chen
 */
@SpringBootApplication
@ComponentScan("com.xykj.enterprise.wechat")
public class WapBrochuresBooter {

    public static void main(String[] args) {
        SpringApplication.run(WapBrochuresBooter.class);
    }

}
