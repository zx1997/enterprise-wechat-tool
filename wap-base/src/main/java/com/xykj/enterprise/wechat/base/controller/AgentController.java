package com.xykj.enterprise.wechat.base.controller;

import com.xykj.enterprise.wechat.base.service.AgentService;
import com.xykj.enterprise.wechat.base.web.context.UserContext;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author george
 * @create 2021-05-29 10:27
 */
@Api(description = "应用相关接口")
@RestController
@RequestMapping("agent")
public class AgentController {
    @Autowired
    private AgentService agentService;

    @ApiOperation(value = "判断是否管理员")
    @ApiImplicitParams({
    })
    @GetMapping("isAgentAdmin")
    public AjaxData isAgentAdmin() {
        return agentService.isAgentAdmin(UserContext.getCorpId(), UserContext.getUserId());
    }

}
