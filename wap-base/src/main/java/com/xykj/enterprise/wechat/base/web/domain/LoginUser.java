package com.xykj.enterprise.wechat.base.web.domain;

/**
 * @author Feng Chen
 */
public class LoginUser {

    private String userId;

    private String corpId;

    public static LoginUser newUser(String userId, String corpId) {
        return new LoginUser(userId, corpId);
    }

    public LoginUser(String userId, String corpId) {
        this.userId = userId;
        this.corpId = corpId;
    }

    public String getUserId() {
        return userId;
    }

    public String getCorpId() {
        return corpId;
    }

}
