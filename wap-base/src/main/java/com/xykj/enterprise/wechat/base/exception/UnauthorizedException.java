package com.xykj.enterprise.wechat.base.exception;


import static com.xykj.enterprise.wechat.base.web.HttpStatus.UNAUTHORIZED;

/**
 * @author Feng Chen
 */
public class UnauthorizedException extends AppException {

    public UnauthorizedException() {
        super(UNAUTHORIZED, "未授权");
    }

}
