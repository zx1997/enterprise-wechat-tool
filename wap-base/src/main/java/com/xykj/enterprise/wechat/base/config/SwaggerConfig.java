package com.xykj.enterprise.wechat.base.config;

import io.swagger.annotations.ApiOperation;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;

/**
 * @author Feng Chen
 */
@ConfigurationProperties(prefix = "swagger")
@Component
@Setter
@EnableSwagger2
@Profile({"dev", "test"})
public class SwaggerConfig {

    private String apiName;

    private String apiVersion;

    private boolean enabled;

    private String pathMapping;

    @Bean
    public Docket docket() {
        return new Docket(DocumentationType.SWAGGER_2)
                // 是否开启 swagger
                .enable(enabled)
                .apiInfo(
                        new ApiInfoBuilder()
                                .title(String.format("%s_接口文档", apiName))
                                .description(String.format("%s相关接口", apiName))
                                .version(String.format("版本号: %s", apiVersion))
                                .build()
                )
                // 设置哪些接口暴露给swagger
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(
                        Arrays.asList(
                                new ParameterBuilder()
                                        .name("token")
                                        .description("令牌")
                                        .parameterType("header")
                                        .modelRef(new ModelRef("string"))
                                        .required(false)
                                        .build()
                        )
                )
                .pathMapping(pathMapping);
    }


}
