package com.xykj.enterprise.wechat.base.web.domain;


import com.xykj.enterprise.wechat.base.web.HttpStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 操作信息提醒
 *
 * @author Feng Chen
 */
public class AjaxResult extends HashMap<String, Object> {

    /**
     * 状态码
     */
    public static final String TAG_CODE = "errCode";

    /**
     * 返回内容
     */
    public static final String TAG_MSG = "errMsg";

    /**
     * 数据对象
     */
    public static final String TAG_DATA = "data";

    public AjaxResult() {
    }

    public AjaxResult(int code, String msg) {
        put(TAG_CODE, code);
        put(TAG_MSG, msg);
    }

    public AjaxResult(int code, String msg, Object data) {
        put(TAG_CODE, code);
        put(TAG_MSG, msg);
        if (data != null) {
            put(TAG_DATA, data);
        }
    }

    /**
     * 返回成功消息
     *
     * @return
     */
    public static AjaxResult success() {
        return AjaxResult.success("操作成功");
    }

    /**
     * 返回成功数据
     *
     * @param data
     * @return
     */
    public static AjaxResult success(Object data) {
        return AjaxResult.success("操作成功", data);
    }

    /**
     * 返回成功数据
     *
     * @param list
     * @return
     */
    public static AjaxResult success(List list, long total) {
        Map data = new HashMap();
        data.put("list", list);
        data.put("total", total);
        return AjaxResult.success("操作成功", data);
    }

    /**
     * 返回成功数据
     *
     * @param list
     * @return
     */
    public static AjaxResult success(List list) {
        Map data = new HashMap();
        data.put("list", list);
        return AjaxResult.success("操作成功", data);
    }


    /**
     * 返回成功消息
     *
     * @param msg
     * @return
     */
    public static AjaxResult success(String msg) {
        return AjaxResult.success(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg
     * @param data
     * @return
     */
    public static AjaxResult success(String msg, Object data) {
        return new AjaxResult(HttpStatus.SUCCESS, msg, data);
    }

    /**
     * 返回失败信息
     *
     * @return
     */
    public static AjaxResult fail() {
        return AjaxResult.fail("操作失败");
    }

    /**
     * 返回失败信息
     *
     * @param msg
     * @return
     */
    public static AjaxResult fail(String msg) {
        return AjaxResult.fail(msg, null);
    }

    /**
     * 返回错误消息
     *
     * @param msg
     * @param data
     * @return
     */
    public static AjaxResult fail(String msg, Object data) {
        return new AjaxResult(HttpStatus.ERROR, msg, data);
    }

    /**
     * 返回失败消息
     *
     * @param code
     * @param msg
     * @return
     */
    public static AjaxResult fail(int code, String msg) {
        return new AjaxResult(code, msg, null);
    }

}
