package com.xykj.enterprise.wechat.base.web.context;


import com.xykj.enterprise.wechat.base.web.domain.LoginUser;

/**
 * @author Feng Chen
 */
public abstract class UserContext {

    private static final ThreadLocal<LoginUser> context = new ThreadLocal<>();

    private UserContext() {

    }

    public static void set(LoginUser user) {
        context.set(user);
    }

    public static String getUserId() {
        return context.get().getUserId();
    }

    public static String getCorpId() {
        return context.get().getCorpId();
    }

    public static LoginUser get() {
        return context.get();
    }

    public static void remove() {
        context.remove();
    }

}
