package com.xykj.enterprise.wechat.base.config;

import com.ydn.simplecache.RedisConfiguration;
import com.ydn.simplecache.SimpleCache;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author Feng Chen
 */
@ConfigurationProperties(prefix = "cache")
@Component
@Setter
public class CacheConfig {

    private String host;

    private Integer port;

    private String passwd;

    @Bean
    public SimpleCache simpleCache() {
        RedisConfiguration configuration = new RedisConfiguration();
        configuration.setHost(host);
        configuration.setPort(port);
        configuration.setPassword(passwd);
        return new SimpleCache(configuration);
    }

}
