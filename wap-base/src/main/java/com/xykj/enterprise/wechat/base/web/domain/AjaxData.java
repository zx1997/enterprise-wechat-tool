package com.xykj.enterprise.wechat.base.web.domain;

import com.xykj.enterprise.wechat.base.web.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Feng Chen
 */
@ApiModel(value = "data")
@Setter
@Getter
public class AjaxData<T> {

    @ApiModelProperty(value = "错误编码")
    private int errCode = HttpStatus.SUCCESS;

    @ApiModelProperty(value = "错误信息")
    private String errMsg = "";

    @ApiModelProperty(value = "数据")
    T data;

    /**
     * 成功返回
     *
     * @param <T>
     * @return
     */
    public static <T> AjaxData<T> success() {
        return success(null);
    }

    /**
     * 成功返回
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> AjaxData<T> success(T data) {
        return success(HttpStatus.SUCCESS, "", data);
    }

    /**
     * 成功返回
     *
     * @param errCode
     * @param errMsg
     * @param data
     * @param <T>
     * @return
     */
    public static <T> AjaxData<T> success(int errCode, String errMsg, T data) {
        AjaxData<T> result = new AjaxData<>();
        result.setErrCode(errCode);
        result.setErrMsg(errMsg);
        result.setData(data);
        return result;
    }

    /**
     * 错误返回
     *
     * @param <T>
     * @return
     */
    public static <T> AjaxData<T> failure() {
        return failure(HttpStatus.ERROR, "操作失败");
    }

    /**
     * 失败返回
     *
     * @param errMsg
     * @param <T>
     * @return
     */
    public static <T> AjaxData<T> failure(String errMsg) {
        return failure(HttpStatus.ERROR, errMsg);
    }

    /**
     * 失败返回
     *
     * @param errCode
     * @param errMsg
     * @param <T>
     * @return
     */
    public static <T> AjaxData<T> failure(int errCode, String errMsg) {
        AjaxData<T> result = new AjaxData<>();
        result.setErrCode(errCode);
        result.setErrMsg(errMsg);
        return result;
    }


}
