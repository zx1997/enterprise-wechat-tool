package com.xykj.enterprise.wechat.base.exception;

import static com.xykj.enterprise.wechat.base.web.HttpStatus.FORBIDDEN;

/**
 * @author Feng Chen
 */
public class TokenExpiredException extends AppException {

    public TokenExpiredException() {
        super(FORBIDDEN, "访问受限，授权过期");
    }
}
