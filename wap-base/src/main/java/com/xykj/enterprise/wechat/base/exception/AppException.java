package com.xykj.enterprise.wechat.base.exception;

/**
 * @author Feng Chen
 */
public class AppException extends RuntimeException  {

    private int errCode;

    private String errMsg;

    public AppException() {
        super();
    }

    public AppException(int errCode, String errMsg) {
        this.errCode = errCode;
        this.errMsg = errMsg;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
