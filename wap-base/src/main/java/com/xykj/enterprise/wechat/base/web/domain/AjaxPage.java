package com.xykj.enterprise.wechat.base.web.domain;

import com.xykj.enterprise.wechat.base.web.HttpStatus;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Feng Chen
 */
@ApiModel(value = "page", description = "分页数据")
@Setter
@Getter
public class AjaxPage<T> {

    @ApiModelProperty(value = "错误编码")
    private int errCode = HttpStatus.SUCCESS;

    @ApiModelProperty(value = "错误信息")
    private String errMsg = "";

    @ApiModelProperty(value = "数据")
    List<T> list;

    @ApiModelProperty(value = "总页数")
    int total;

    @ApiModelProperty(value = "总行数")
    int count;

    /**
     * 成功返回
     *
     * @param list  数据
     * @param total 总页数
     * @param count 总行数
     * @param <T>
     * @return
     */
    public static <T> AjaxPage<T> success(List<T> list, int total, int count) {
        AjaxPage<T> result = new AjaxPage<>();
        result.setList(list);
        result.setTotal(total);
        result.setCount(count);
        return result;
    }

    /**
     * 错误返回
     *
     * @param <T>
     * @return
     */
    public static <T> AjaxPage<T> failure() {
        return failure(HttpStatus.ERROR, "操作失败");
    }

    /**
     * 错误返回
     *
     * @param errMsg
     * @param <T>
     * @return
     */
    public static <T> AjaxPage<T> failure(String errMsg) {
        return failure(HttpStatus.ERROR, errMsg);
    }

    /**
     * 错误返回
     *
     * @param errCode
     * @param errMsg
     * @param <T>
     * @return
     */
    public static <T> AjaxPage<T> failure(int errCode, String errMsg) {
        AjaxPage<T> result = new AjaxPage<>();
        result.setErrCode(errCode);
        result.setErrMsg(errMsg);
        return result;
    }


}
