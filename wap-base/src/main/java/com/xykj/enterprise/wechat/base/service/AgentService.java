package com.xykj.enterprise.wechat.base.service;

import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.client.AuthClient;
import com.ydn.appserver.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @Author george
 * @create 2021-05-29 10:25
 */
@Service
public class AgentService extends BaseService {

    @Autowired
    private AuthClient authClient;

    @Value("${wx.saas.SuiteID}")
    private String SuiteID;

    public AjaxData isAgentAdmin(String corpid, String userId) {
        Request request = new Request("IsAgentAdmin");
        request.setParameter("suiteId", SuiteID);
        request.setParameter("corpid", corpid);
        request.setParameter("userId", userId);
        return convertData(authClient.send(request));
    }
}
