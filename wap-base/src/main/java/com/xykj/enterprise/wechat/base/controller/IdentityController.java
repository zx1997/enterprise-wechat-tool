package com.xykj.enterprise.wechat.base.controller;

import com.xykj.enterprise.wechat.base.service.IdentityService;
import com.xykj.enterprise.wechat.base.web.domain.AjaxData;
import com.xykj.enterprise.wechat.bean.wap.TokenVo;
import com.xykj.enterprise.wechat.bean.wap.domain.LoginByCodeModel;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhouxu
 * @create 2021-03-29 22:32
 */
@Api(description = "第三方应用-企业授权、身份认证、用户登录")
@RestController
@RequestMapping("identity")
public class IdentityController {

    @Autowired
    private IdentityService identityService;

    @ApiOperation(value = "生成企业微信-企业授权地址")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "redirectUri", value = "授权通过后回调url", paramType = "query", dataType = "String", required = true),
            @ApiImplicitParam(name = "state", value = "state", paramType = "query", dataType = "String", required = false),
    })
    @GetMapping("generateWeComEnterpriseAuthUrl")
    public AjaxData generateWeComEnterpriseAuthUrl(String redirectUri, String state) {
        return identityService.generateWeComEnterpriseAuthUrl(redirectUri, state);
    }

    @ApiOperation(value = "生成企业微信-企业永久授权码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "authCode", value = "临时授权码", paramType = "query", dataType = "String", required = true),
    })
    @PostMapping("generatePermanentCode")
    public AjaxData generatePermanentCode(String authCode) {
        return identityService.generatePermanentCode(authCode);
    }

    @ApiOperation(value = "生成企业微信-员工授权地址")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "redirectUri", value = "授权通过后回调url", paramType = "query", dataType = "String", required = true),
            @ApiImplicitParam(name = "state", value = "state", paramType = "query", dataType = "String", required = false),
    })
    @GetMapping("generateWeComUserAuthUrl")
    public AjaxData getPreAuthCode(String redirectUri, String state) {
        return identityService.generateWeComUserAuthUrl(redirectUri, state);
    }


    @ApiOperation(value = "登录接口")
    @ApiImplicitParam(name = "model", value = "LoginByCodeModel", dataType = "LoginByCodeModel")
    @PostMapping("loginByCode")
    public AjaxData<TokenVo> loginByCode(@RequestBody LoginByCodeModel model) {
        return identityService.loginByCode(model.getCode());
    }

    @ApiOperation(value = "wx.getAgentConfig 接口参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "url", value = "回调url", paramType = "query", dataType = "String", required = true),
    })
    @GetMapping("getAgentConfig")
    public AjaxData getAgentConfig(String url) {
        return identityService.getAgentConfig(url);
    }

    @ApiOperation(value = "wx.getConfig 接口参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "url", value = "回调url", paramType = "query", dataType = "String", required = true),
    })
    @GetMapping("getConfig")
    public AjaxData getConfig(String url) {
        return identityService.getConfig(url);
    }


}
