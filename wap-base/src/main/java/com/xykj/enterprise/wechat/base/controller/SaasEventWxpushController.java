package com.xykj.enterprise.wechat.base.controller;

import com.xykj.enterprise.wechat.base.service.SaasEventWxpushService;
import com.xykj.enterprise.wechat.util.wxpush.xml.AesException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhouxu
 * @create 2021-02-23 17:10
 */
@Api(description = "第三方应用-事件回调接口")
@RestController
@RequestMapping("saasEvent")
@Slf4j
public class SaasEventWxpushController {

    @Autowired
    private SaasEventWxpushService saasEventWxpushService;

    @ApiOperation(value = "微信回调接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "msg_signature", value = "签名文本", paramType = "query", dataType = "string", required = true),
            @ApiImplicitParam(name = "timestamp", value = "时间戳", paramType = "query", dataType = "string", required = true),
            @ApiImplicitParam(name = "nonce", value = "随机数", paramType = "query", dataType = "string", required = true),
            @ApiImplicitParam(name = "echostr", value = "消息文本（加密）", paramType = "query", dataType = "string", required = true),
    })
    @RequestMapping(value = "wxpush", method = {RequestMethod.GET})
    public String verify(String msg_signature, String timestamp, String nonce, String echostr) {
        try {
            log.debug("msg_signature:{},timestamp:{},nonce:{},echostr:{}", msg_signature, timestamp, nonce, echostr);
            return saasEventWxpushService.verify(msg_signature, timestamp, nonce, echostr);
        } catch (AesException e) {
            log.error(e.getMessage(), e.getCode());
            return e.getMessage();
        } catch (Exception e) {
            log.error("", e);
            return e.getMessage();
        }
    }

    @ApiOperation(value = "微信回调接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "msg_signature", value = "签名文本", paramType = "query", dataType = "string", required = true),
            @ApiImplicitParam(name = "timestamp", value = "时间戳", paramType = "query", dataType = "string", required = true),
            @ApiImplicitParam(name = "nonce", value = "随机数", paramType = "query", dataType = "string", required = true),
            @ApiImplicitParam(name = "data", value = "消息文本（xml格式，加密）", dataType = "string", required = false),
    })
    @RequestMapping(value = "wxpush", method = {RequestMethod.POST})
    public String post(String msg_signature, String timestamp, String nonce, @RequestBody String data) {
        try {
            log.debug("msg_signature:{},timestamp:{},nonce:{},data:{}", msg_signature, timestamp, nonce, data);
            return saasEventWxpushService.post(msg_signature, timestamp, nonce, data);
        } catch (AesException e) {
            log.error(e.getMessage(), e.getCode());
            return e.getMessage();
        } catch (Exception e) {
            log.error("", e);
            return e.getMessage();
        }
    }

}
