#!/bin/sh

SERVER_NAME="server-auth"
PORT=8191
MEM_OPTS="-Xms512m -Xmx1024m"
CP="../conf/:../lib/*"

java $MEM_OPTS -Dname=$SERVER_NAME  -cp $CP com.ydn.appserver.DefaultServerMain -port ${PORT} >> ../logs/$SERVER_NAME.log  &