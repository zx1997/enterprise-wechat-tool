package com.xykj.enterprise.wechat.auth.api;

import com.xykj.enterprise.wechat.bean.ext.identity.GetPermanentCodeVo;
import com.xykj.enterprise.wechat.bean.ext.identity.SuiteAccessToken;
import com.xykj.enterprise.wechat.core.service.corp.CorpService;
import com.xykj.enterprise.wechat.core.service.identity.SuiteTicketService;
import com.xykj.enterprise.wechat.util.wecom.identity.PermanentCodeUtil;
import com.xykj.enterprise.wechat.util.wecom.identity.SuiteAccessTokenUtil;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import com.ydn.simplecache.SimpleCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author zhouxu
 * @create 2021-03-29 22:15
 */
@Function(description = "生成企业微信-企业永久授权码", parameters = {
        @Parameter(name = "suite_id", type = Type.String, description = "以ww或wx开头应用id（对应于旧的以tj开头的套件id", required = true),
        @Parameter(name = "auth_code", type = Type.String, description = "临时授权码", required = true),
        @Parameter(name = "suite_id", type = Type.String, description = "suite_id", required = true),
        @Parameter(name = "secret", type = Type.String, description = "secret", required = true),

})
@Component
@Slf4j
public class GeneratePermanentCode implements Action {

    @Autowired
    private SuiteTicketService suiteTicketService;
    @Autowired
    private SimpleCache simpleCache;
    @Autowired
    private CorpService corpService;

    @Override
    public Response execute(Request request) throws Exception {
        String suiteId = request.getString("suite_id");
        String authCode = request.getString("auth_code");
        String suiteSecret = request.getString("secret");
        String suiteTicket = suiteTicketService.getLatest(suiteId).getSuitTicket();
        try {

            // 获取suiteAccessToken
            SuiteAccessToken suiteAccessToken = SuiteAccessTokenUtil.get(simpleCache, suiteId, suiteSecret, suiteTicket);
            // 获取permanentCode
            GetPermanentCodeVo getPermanentCodeVo = PermanentCodeUtil.get(authCode, suiteAccessToken.getSuite_access_token());
            // 存储永久授权码
            corpService.save(suiteId, getPermanentCodeVo);
            // 初始化企业扩展信息
            corpService.initExtInfo(getPermanentCodeVo.getAuth_corp_info().getCorpid());
            return Response.success();
        } catch (Exception e) {
            log.error("生成企业微信-企业永久授权码失败", e);
            return Response.fail(e);
        }

    }
}
