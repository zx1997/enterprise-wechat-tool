package com.xykj.enterprise.wechat.auth.api;

import com.xykj.enterprise.wechat.core.model.dodb.AgentAdmin;
import com.xykj.enterprise.wechat.core.model.dodb.AgentInfo;
import com.xykj.enterprise.wechat.core.service.corp.AgentService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 判断是否为应用管理员
 *
 * @Author george
 * @create 2021-05-28 18:09
 */
@Function(description = "判断是否为应用管理员", parameters = {
        @Parameter(name = "corpid", type = Type.String, description = "授权方corpid", required = true),
        @Parameter(name = "suite_id", type = Type.String, description = "应用suite_id", required = true),
        @Parameter(name = "userId", type = Type.String, description = "userId", required = true),

})
@Component
@Slf4j
public class IsAgentAdmin implements Action {
    @Autowired
    private AgentService agentService;

    public static final boolean isAdmin = true;

    @Override
    public Response execute(Request request) throws Exception {
        // 判断是否管理员
        String corpid = request.getString("corpid");
        String suiteId = request.getString("suiteId");
        String userId = request.getString("userId");
        AgentInfo info = agentService.getAgent(corpid, suiteId);

        List<AgentAdmin> admins = agentService.listAdmin(corpid, suiteId, info.getAgentid());
        if (admins != null && admins.contains(userId)) {
            return Response.success().put("isAdmin", isAdmin);
        }
        return Response.success().put("isAdmin", !isAdmin);
    }
}
