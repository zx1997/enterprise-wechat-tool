package com.xykj.enterprise.wechat.auth.api;

import com.xykj.enterprise.wechat.bean.busi.vo.GetAgentConfigVo;
import com.xykj.enterprise.wechat.core.model.dodb.AgentInfo;
import com.xykj.enterprise.wechat.core.service.corp.AgentService;
import com.xykj.enterprise.wechat.core.service.identity.JsapiTicketService;
import com.xykj.enterprise.wechat.util.other.UuidUtil;
import com.xykj.enterprise.wechat.util.wxpush.JSAPIUtil;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Calendar;

/**
 * @Author zhouxu
 * @create 2021-04-02 15:56
 */
@Function(description = "客户端 wx.config 接口", parameters = {
        @Parameter(name = "corpid", type = Type.String, description = "授权方corpid", required = true),
        @Parameter(name = "suite_id", type = Type.String, description = "suite_id", required = true),
        @Parameter(name = "secret", type = Type.String, description = "secret", required = true),

        @Parameter(name = "url", type = Type.String, description = "回调地址", required = true),

})
@Component
@Slf4j
public class GetConfig implements Action {

    @Autowired
    private JsapiTicketService jsapiTicketService;
    @Autowired
    private AgentService agentService;

    @Override
    public Response execute(Request request) throws Exception {
        String corpid = request.getString("corpid");
        String suite_id = request.getString("suite_id");
        String secret = request.getString("secret");

        AgentInfo agentInfo = agentService.getAgent(corpid, suite_id);
        if (agentInfo == null) {
            return Response.fail("agentInfo 为空" + "corpid = " + corpid + ",suiteId = " + suite_id);
        }

        try {
            // 获取jsapi_ticket
            String jsapi_ticket = jsapiTicketService.getCorpTicket(
                    corpid,
                    agentInfo.getAgentid(),
                    suite_id,
                    secret
            );


            String noncestr = UuidUtil.getNoSpecialChar();
            long timestamp = Calendar.getInstance().getTimeInMillis();
            String url = request.getString("url");

            GetAgentConfigVo vo = new GetAgentConfigVo();
            vo.setCorpid(corpid);
            vo.setAgentid(agentInfo.getAgentid());
            vo.setTimestamp(String.valueOf(timestamp));
            vo.setNonceStr(noncestr);
            vo.setSignature(JSAPIUtil.getSignature(jsapi_ticket, noncestr, timestamp, url));
            return Response.success().put("data", vo);

        } catch (Exception e) {
            log.error("", e);
            return Response.fail(e);
        }

    }
}
