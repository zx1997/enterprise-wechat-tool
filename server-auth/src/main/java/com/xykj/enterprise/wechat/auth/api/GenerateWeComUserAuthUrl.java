package com.xykj.enterprise.wechat.auth.api;

import com.alibaba.fastjson.JSONArray;
import com.xykj.enterprise.wechat.bean.busi.WeComUserAuthUrl;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Author zhouxu
 * @create 2021-03-31 15:17
 */
@Function(description = "生成企业微信-员工授权地址", parameters = {
        @Parameter(name = "appId", type = Type.String, description = "以ww或wx开头应用id（对应于旧的以tj开头的套件id", required = true),
        @Parameter(name = "agentId", type = Type.String, description = "企业应用的id", required = true),
        @Parameter(name = "redirectUri", type = Type.String, description = "授权通过后回调url", required = true),
        @Parameter(name = "state", type = Type.String, description = "state", required = true),
})
@Component
@Slf4j
public class GenerateWeComUserAuthUrl implements Action {

    @Override
    public Response execute(Request request) throws Exception {
        String appId = request.getString("appId");
        String redirectUri = request.getString("redirectUri");
        String agentId = request.getString("agentId");
        String state = request.getString("state");

        WeComUserAuthUrl url = new WeComUserAuthUrl(appId, redirectUri, agentId, state);
        log.debug("url json string :{}", JSONArray.toJSON(url).toString());
        return Response.success().put("data", url);
    }
}
