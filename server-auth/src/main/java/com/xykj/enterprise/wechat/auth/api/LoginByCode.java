package com.xykj.enterprise.wechat.auth.api;

import com.alibaba.fastjson.JSON;
import com.xykj.enterprise.wechat.bean.ext.identity.UserInfo3rd;
import com.xykj.enterprise.wechat.client.WxpushClient;
import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;
import com.xykj.enterprise.wechat.core.service.corp.CorpService;
import com.xykj.enterprise.wechat.core.service.corp.UserService;
import com.xykj.enterprise.wechat.core.service.identity.IdentityService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-03-31 15:54
 */
@Function(description = "员工登录", parameters = {
        @Parameter(name = "suite_id", type = Type.String, description = "以ww或wx开头应用id（对应于旧的以tj开头的套件id", required = true),
        @Parameter(name = "code", type = Type.String, description = "员工 code", required = true),
        @Parameter(name = "secret", type = Type.String, description = "secret", required = true),

})
@Component
@Slf4j
public class LoginByCode implements Action {

    @Autowired
    private UserService userService;
    @Autowired
    private IdentityService identityService;
    @Autowired
    private CorpService corpService;
    @Autowired
    private WxpushClient wxpushClient;

    @Override
    public Response execute(Request request) throws Exception {

        // 获取员工信息
        UserInfo3rd userInfo3rd = userService.get(
                request.getString("suite_id"),
                request.getString("code"),
                request.getString("secret")
        );
        log.debug("userInfo3rd:{}", userInfo3rd);

        String accessToken = identityService.getAccessToken(
                userInfo3rd.getCorpId(),
                request.getString("suite_id"),
                request.getString("secret")
        );

        CorpExt corpExt = corpService.getCorpExt(userInfo3rd.getCorpId());
        Map param = new HashMap();
        // 异步同步部门信息
        if (!corpExt.isDepartmentSync()) {
            param.clear();
            param.put("access_token", accessToken);
            param.put("corpid", userInfo3rd.getCorpId());
            param.put("id", null);
            wxpushClient.fireEvent("sync_department", JSON.toJSONString(param));
        }

        // 异步同步员工信息
        if (!corpExt.isUserSync()) {
            param.clear();
            param.put("access_token", accessToken);
            param.put("corpid", userInfo3rd.getCorpId());
            param.put("department_id", 1);// 根部门
            param.put("fetch_child", 1);// 递归
            wxpushClient.fireEvent("sync_user", JSON.toJSONString(param));
        }

        // 异步同步客户信息
        if (!corpExt.isCustomerSync()) {
            param.clear();
            param.put("access_token", accessToken);
            param.put("corpid", userInfo3rd.getCorpId());
            wxpushClient.fireEvent("sync_customer", JSON.toJSONString(param));
        }

        // 异步同步企业标签
        if (!corpExt.isCorpTagSync()) {
            param.clear();
            param.put("access_token", accessToken);
            param.put("corpid", userInfo3rd.getCorpId());
            wxpushClient.fireEvent("sync_corp_tag", JSON.toJSONString(param));
        }

        // 异步同步客户群
        if (!corpExt.isGroupChatSync()) {
            param.clear();
            param.put("access_token", accessToken);
            param.put("corpid", userInfo3rd.getCorpId());
            wxpushClient.fireEvent("sync_group_chat", JSON.toJSONString(param));
        }

        // 异步同步朋友圈
        if (!corpExt.isMomentSync()){
            param.clear();
            param.put("access_token", accessToken);
            param.put("corpid", userInfo3rd.getCorpId());
            wxpushClient.fireEvent("sync_moment", JSON.toJSONString(param));
        }

        return Response.success().put("data", userInfo3rd);
    }
}
