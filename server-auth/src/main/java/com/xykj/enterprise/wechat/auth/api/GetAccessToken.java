package com.xykj.enterprise.wechat.auth.api;

import com.xykj.enterprise.wechat.core.service.identity.IdentityService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author zhouxu
 * @create 2021-04-02 15:56
 */
@Function(description = "获取AccessToken（更新）", parameters = {
        @Parameter(name = "corpid", type = Type.String, description = "授权方corpid", required = true),
        @Parameter(name = "suite_id", type = Type.String, description = "应用suite_id", required = true),
        @Parameter(name = "secret", type = Type.String, description = "secret", required = true),

})
@Component
@Slf4j
public class GetAccessToken implements Action {

    @Autowired
    private IdentityService identityService;

    @Override
    public Response execute(Request request) throws Exception {
        try {
            String accessToken = identityService.getAccessToken(
                    request.getString("corpid"),
                    request.getString("suite_id"),
                    request.getString("secret")
            );
            return Response.success().put("data", accessToken);

        } catch (Exception e) {
            log.error("", e);
            return Response.fail(e);
        }

    }
}
