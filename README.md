##企业微信工具--后端服务相关
###工程简介
####公共组件层
        common-bean：公用bean
        common-client：服务通信client组件
        common-core：组件层（service）；包含原子层（dao）
        common-util：公共组件（常用工具包，企业微信API工具类封装等组件）
####服务API层        
        server-auth：认证授权服务API
        server-corp：企业服务API
        server-customer：客户服务API
        server-group：组群服务API
        server-wxpush：企业微信回调API
####展示层（前端接口）    
        wap-base：公共展示层（jwt认证，swagger配置，token 认证，cors跨域处理）
        wap-brochures：展示层-宣传册（第三方saas应用）
        wap-customer-view：展示层-客户视图（第三方saas应用）

