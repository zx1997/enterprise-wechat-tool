package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCorpExt<M extends BaseCorpExt<M>> extends Model<M> implements IBean {

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 部门数据主动同步标记（0未同步 1-已同步）
	 */
	public void setDepartmentSync(java.lang.String departmentSync) {
		set("department_sync", departmentSync);
	}
	
	/**
	 * 部门数据主动同步标记（0未同步 1-已同步）
	 */
	public java.lang.String getDepartmentSync() {
		return getStr("department_sync");
	}

	/**
	 * 成员数据主动同步标记（0未同步 1-已同步）
	 */
	public void setUserSync(java.lang.String userSync) {
		set("user_sync", userSync);
	}
	
	/**
	 * 成员数据主动同步标记（0未同步 1-已同步）
	 */
	public java.lang.String getUserSync() {
		return getStr("user_sync");
	}

	/**
	 * 客户数据主动同步标记（0未同步 1-已同步）
	 */
	public void setCustomerSync(java.lang.String customerSync) {
		set("customer_sync", customerSync);
	}
	
	/**
	 * 客户数据主动同步标记（0未同步 1-已同步）
	 */
	public java.lang.String getCustomerSync() {
		return getStr("customer_sync");
	}

	/**
	 * 企业标签数据主动同步标记（0未同步 1-已同步）
	 */
	public void setCorpTagSync(java.lang.String corpTagSync) {
		set("corp_tag_sync", corpTagSync);
	}
	
	/**
	 * 企业标签数据主动同步标记（0未同步 1-已同步）
	 */
	public java.lang.String getCorpTagSync() {
		return getStr("corp_tag_sync");
	}

	/**
	 * 客户组数据主动同步
	 */
	public void setGroupChatSync(java.lang.String groupChatSync) {
		set("group_chat_sync", groupChatSync);
	}
	
	/**
	 * 客户组数据主动同步
	 */
	public java.lang.String getGroupChatSync() {
		return getStr("group_chat_sync");
	}

	/**
	 * 朋友圈数据主动同步
	 */
	public void setMomentSync(java.lang.String momentSync) {
		set("moment_sync", momentSync);
	}
	
	/**
	 * 朋友圈数据主动同步
	 */
	public java.lang.String getMomentSync() {
		return getStr("moment_sync");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
