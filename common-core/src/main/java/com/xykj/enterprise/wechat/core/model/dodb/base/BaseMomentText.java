package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMomentText<M extends BaseMomentText<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 朋友圈ID
	 */
	public void setMomentId(java.lang.String momentId) {
		set("moment_id", momentId);
	}
	
	/**
	 * 朋友圈ID
	 */
	public java.lang.String getMomentId() {
		return getStr("moment_id");
	}

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 朋友圈文本
	 */
	public void setContent(java.lang.String content) {
		set("content", content);
	}
	
	/**
	 * 朋友圈文本
	 */
	public java.lang.String getContent() {
		return getStr("content");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
