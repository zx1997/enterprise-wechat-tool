package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.contacts.member.UserListVo;
import com.xykj.enterprise.wechat.bean.ext.identity.UserInfo3rd;
import com.xykj.enterprise.wechat.core.model.dodb.User;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.Map;

public interface UserService {

    User getById(String userId);

    Page<User> list(Integer page, Integer limit, Map<String,Object> param);

    UserInfo3rd get(String suiteId, String code, String secret);

    void syncUserList(UserListVo listVo, String corpid);
}
