package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcProjectMember;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.Date;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-12 14:46
 */
public interface PCProjectMemberDao {

    Page<PcProjectMember> list(Integer page, Integer limit, Map<String,Object> params);

    PcProjectMember get(Long id,String corpid);

    Long save(
            String corpid,
            Integer projectId,
            String memberId,
            String memberContent,
            Date planStartTime,
            Date realStartTime,
            Date planEndTime,
            Date realEndTime,
            String creator
    );

    void update(
            Long id,
            String corpid,
            Integer projectId,
            String memberId,
            String memberContent,
            Date planStartTime,
            Date realStartTime,
            Date planEndTime,
            Date realEndTime,
            String updator
    );

    void delete(Long id, String corpid);

}
