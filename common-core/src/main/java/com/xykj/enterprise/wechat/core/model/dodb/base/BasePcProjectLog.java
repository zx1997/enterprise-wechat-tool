package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BasePcProjectLog<M extends BasePcProjectLog<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 工作日期
	 */
	public void setWorkDay(java.lang.Integer workDay) {
		set("work_day", workDay);
	}
	
	/**
	 * 工作日期
	 */
	public java.lang.Integer getWorkDay() {
		return getInt("work_day");
	}

	/**
	 * 参与人标识
	 */
	public void setWorker(java.lang.String worker) {
		set("worker", worker);
	}
	
	/**
	 * 参与人标识
	 */
	public java.lang.String getWorker() {
		return getStr("worker");
	}

	/**
	 * 参与项目
	 */
	public void setProjectId(java.lang.Integer projectId) {
		set("project_id", projectId);
	}
	
	/**
	 * 参与项目
	 */
	public java.lang.Integer getProjectId() {
		return getInt("project_id");
	}

	/**
	 * 项目内容
	 */
	public void setWorkContent(java.lang.String workContent) {
		set("work_content", workContent);
	}
	
	/**
	 * 项目内容
	 */
	public java.lang.String getWorkContent() {
		return getStr("work_content");
	}

	/**
	 * 工时（小时为单位）
	 */
	public void setWorkHour(java.math.BigDecimal workHour) {
		set("work_hour", workHour);
	}
	
	/**
	 * 工时（小时为单位）
	 */
	public java.math.BigDecimal getWorkHour() {
		return get("work_hour");
	}

	/**
	 * 填报时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 填报时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 填报人
	 */
	public void setCreator(java.lang.String creator) {
		set("creator", creator);
	}
	
	/**
	 * 填报人
	 */
	public java.lang.String getCreator() {
		return getStr("creator");
	}

	/**
	 * 修改时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	/**
	 * 修改时间
	 */
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

	/**
	 * 修改人
	 */
	public void setUpdator(java.lang.String updator) {
		set("updator", updator);
	}
	
	/**
	 * 修改人
	 */
	public java.lang.String getUpdator() {
		return getStr("updator");
	}

}
