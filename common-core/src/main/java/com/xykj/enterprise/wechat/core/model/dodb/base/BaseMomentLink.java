package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMomentLink<M extends BaseMomentLink<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 网页链接标题
	 */
	public void setTitle(java.lang.String title) {
		set("title", title);
	}
	
	/**
	 * 网页链接标题
	 */
	public java.lang.String getTitle() {
		return getStr("title");
	}

	/**
	 * 网页链接url
	 */
	public void setUrl(java.lang.String url) {
		set("url", url);
	}
	
	/**
	 * 网页链接url
	 */
	public java.lang.String getUrl() {
		return getStr("url");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 朋友圈ID
	 */
	public void setMomentId(java.lang.String momentId) {
		set("moment_id", momentId);
	}
	
	/**
	 * 朋友圈ID
	 */
	public java.lang.String getMomentId() {
		return getStr("moment_id");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
