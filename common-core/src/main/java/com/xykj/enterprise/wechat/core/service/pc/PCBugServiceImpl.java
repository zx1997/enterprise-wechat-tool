package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectBugVo;
import com.xykj.enterprise.wechat.core.dao.pc.PCBugDao;
import com.xykj.enterprise.wechat.core.model.dodb.PcBug;
import com.ydn.dbframe.plugin.activerecord.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @Author george
 * @create 2021-05-27 15:32
 */
@Service
public class PCBugServiceImpl implements PCBugService {
    @Autowired
    private PCBugDao pcBugDao;

    @Override
    public Page<PcBug> list(Integer page, Integer limit, Map<String, Object> param) {
        return pcBugDao.list(page, limit, param);
    }

    @Override
    public void save(ProjectBugVo vo, String corpid, String userid) {
        pcBugDao.save(
                corpid,
                vo.getProjectId(),
                vo.getProposer(),
                vo.getBugName(),
                vo.getBugDesc(),
                vo.getAttachment(),
                vo.getHandler(),
                vo.getBugSource()
        );
    }

    @Override
    public void update(ProjectBugVo vo, String corpid, String userid) {
        pcBugDao.update(
                vo.getId(),
                corpid,
                vo.getProjectId(),
                vo.getProposer(),
                vo.getBugName(),
                vo.getBugDesc(),
                vo.getAttachment(),
                vo.getHandler(),
                vo.getBugSource()
        );
    }

    @Override
    public void delete(Long id, String corpid, String userId) {
        pcBugDao.delete(id, corpid);
    }
}
