package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.core.model.dodb.CorpSuite;

/**
 * @Author george
 * @create 2021-04-28 15:38
 */
public interface IdentityService {

    String getAccessToken(String corpid, String suiteId, String secret);

    CorpSuite refreshAccessToken(String corpid, String suiteId, String secret);
}
