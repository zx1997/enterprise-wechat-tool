package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.SysParam;
import org.springframework.stereotype.Repository;

/**
 * @Author zhouxu
 * @create 2021-04-01 16:17
 */
@Repository
public class SysParamDaoImpl implements SysParamDao {

    @Override
    public SysParam getByCode(String code) {
        return SysParam.dao.findById(code);
    }

}
