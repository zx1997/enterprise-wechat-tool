package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.config.DbConfig;
import com.xykj.enterprise.wechat.core.model.dodb.MomentVideo;
import com.ydn.dbframe.plugin.activerecord.Db;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author george
 * @create 2021-05-10 11:36
 */
@Repository
public class MomentVideoDaoImpl implements MomentVideoDao {
    @Override
    public MomentVideo get(String corpid, String momentId) {
        String sql = " select * from t_moment_video where corpid = ? and moment_id = ? ";
        return MomentVideo.dao.findFirst(sql, corpid, momentId);
    }

    @Override
    public void save(String media_id, String thumb_media_id, String corpid, String momentId) {
        MomentVideo video = new MomentVideo();
        video.setMediaId(media_id);
        video.setThumbMediaId(thumb_media_id);
        video.setCorpid(corpid);
        video.setMomentId(momentId);
        video.setCreateTime(new Date());
        video.save();
    }

    @Override
    public void delete(String momentId, String corpid) {
        String sql = " delete from t_moment_video where corpid = ? and moment_id = ? limit 1 ";
        Db.use(DbConfig.DATASOURCE).update(sql, momentId, corpid);
    }
}
