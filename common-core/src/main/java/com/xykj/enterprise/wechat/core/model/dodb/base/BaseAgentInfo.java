package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAgentInfo<M extends BaseAgentInfo<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * suiteId
	 */
	public void setSuiteId(java.lang.String suiteId) {
		set("suite_id", suiteId);
	}
	
	/**
	 * suiteId
	 */
	public java.lang.String getSuiteId() {
		return getStr("suite_id");
	}

	/**
	 * 授权方应用ID
	 */
	public void setAgentid(java.lang.String agentid) {
		set("agentid", agentid);
	}
	
	/**
	 * 授权方应用ID
	 */
	public java.lang.String getAgentid() {
		return getStr("agentid");
	}

	/**
	 * 授权方应用名字
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 授权方应用名字
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 授权应用的方形头像
	 */
	public void setRoundLogoUrl(java.lang.String roundLogoUrl) {
		set("round_logo_url", roundLogoUrl);
	}
	
	/**
	 * 授权应用的方形头像
	 */
	public java.lang.String getRoundLogoUrl() {
		return getStr("round_logo_url");
	}

	/**
	 * 授权应用的圆形头像
	 */
	public void setSquareLogoUrl(java.lang.String squareLogoUrl) {
		set("square_logo_url", squareLogoUrl);
	}
	
	/**
	 * 授权应用的圆形头像
	 */
	public java.lang.String getSquareLogoUrl() {
		return getStr("square_logo_url");
	}

	/**
	 * 旧的多应用套件中的对应应用ID，新开发者忽略
	 */
	public void setAppid(java.lang.String appid) {
		set("appid", appid);
	}
	
	/**
	 * 旧的多应用套件中的对应应用ID，新开发者忽略
	 */
	public java.lang.String getAppid() {
		return getStr("appid");
	}

	/**
	 * 应用对应的权限
	 */
	public void setPrivilege(java.lang.String privilege) {
		set("privilege", privilege);
	}
	
	/**
	 * 应用对应的权限
	 */
	public java.lang.String getPrivilege() {
		return getStr("privilege");
	}

	/**
	 * 共享了应用的互联企业信息
	 */
	public void setShareFrom(java.lang.String shareFrom) {
		set("share_from", shareFrom);
	}
	
	/**
	 * 共享了应用的互联企业信息
	 */
	public java.lang.String getShareFrom() {
		return getStr("share_from");
	}

}
