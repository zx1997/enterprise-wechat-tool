package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseSuiteInfo<M extends BaseSuiteInfo<M>> extends Model<M> implements IBean {

	/**
	 * suite_id
	 */
	public void setSuiteId(java.lang.String suiteId) {
		set("suite_id", suiteId);
	}
	
	/**
	 * suite_id
	 */
	public java.lang.String getSuiteId() {
		return getStr("suite_id");
	}

	/**
	 * secret
	 */
	public void setSecret(java.lang.String secret) {
		set("secret", secret);
	}
	
	/**
	 * secret
	 */
	public java.lang.String getSecret() {
		return getStr("secret");
	}

	/**
	 * corpid
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * corpid
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 备注
	 */
	public void setRemark(java.lang.String remark) {
		set("remark", remark);
	}
	
	/**
	 * 备注
	 */
	public java.lang.String getRemark() {
		return getStr("remark");
	}

}
