package com.xykj.enterprise.wechat.core.service.corp;

import com.alibaba.fastjson.JSONObject;
import com.xykj.enterprise.wechat.bean.ext.identity.Admin;
import com.xykj.enterprise.wechat.bean.ext.identity.Agent;
import com.xykj.enterprise.wechat.core.dao.AgentAdminDao;
import com.xykj.enterprise.wechat.core.dao.AgentInfoDao;
import com.xykj.enterprise.wechat.core.model.dodb.AgentAdmin;
import com.xykj.enterprise.wechat.core.model.dodb.AgentInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author george
 * @create 2021-05-28 17:51
 */
@Service
public class AgentServiceImpl implements AgentService {
    @Autowired
    private AgentInfoDao agentInfoDao;
    @Autowired
    private AgentAdminDao agentAdminDao;

    @Override
    public void saveAgentInfo(Agent agent, String corpid, String suiteId) {
        agentInfoDao.save(
                corpid,
                suiteId,
                String.valueOf(agent.getAgentid()),
                agent.getName(),
                agent.getRound_logo_url(),
                agent.getSquare_logo_url(),
                agent.getAppid(),
                JSONObject.toJSONString(agent.getPrivilege()),
                ""
        );
    }


    @Override
    public List<AgentAdmin> listAdmin(String corpid, String suiteId, String agentId) {
        return agentAdminDao.list(corpid, suiteId, agentId);
    }

    @Override
    public AgentInfo getAgent(String corpid, String suiteId) {
        return agentInfoDao.get(corpid, suiteId);
    }

    @Override
    public void saveAgentAdmin(List<Admin> adminList, String agentId, String corpid, String suiteId) {
        if (adminList == null || adminList.isEmpty()) {
            return;
        }
        for (Admin admin : adminList) {
            agentAdminDao.save(
                    corpid,
                    suiteId,
                    agentId,
                    admin.getUserid(),
                    admin.getOpen_userid(),
                    String.valueOf(admin.getAuth_type())
            );
        }
    }

}
