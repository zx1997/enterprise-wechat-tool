package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseSysParam<M extends BaseSysParam<M>> extends Model<M> implements IBean {

	/**
	 * 操作员标识
	 */
	public void setParamCode(java.lang.String paramCode) {
		set("param_code", paramCode);
	}
	
	/**
	 * 操作员标识
	 */
	public java.lang.String getParamCode() {
		return getStr("param_code");
	}

	/**
	 * 操作员名称
	 */
	public void setParamName(java.lang.String paramName) {
		set("param_name", paramName);
	}
	
	/**
	 * 操作员名称
	 */
	public java.lang.String getParamName() {
		return getStr("param_name");
	}

	/**
	 * 操作员编码
	 */
	public void setParamValue(java.lang.String paramValue) {
		set("param_value", paramValue);
	}
	
	/**
	 * 操作员编码
	 */
	public java.lang.String getParamValue() {
		return getStr("param_value");
	}

	/**
	 * 操作员密码
	 */
	public void setRemark(java.lang.String remark) {
		set("remark", remark);
	}
	
	/**
	 * 操作员密码
	 */
	public java.lang.String getRemark() {
		return getStr("remark");
	}

}
