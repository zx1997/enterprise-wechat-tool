package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomerBehavior<M extends BaseCustomerBehavior<M>> extends Model<M> implements IBean {

	/**
	 * 轨迹主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 轨迹主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 客户ID
	 */
	public void setCustomerId(java.lang.Long customerId) {
		set("customer_id", customerId);
	}
	
	/**
	 * 客户ID
	 */
	public java.lang.Long getCustomerId() {
		return getLong("customer_id");
	}

	/**
	 * 目标类型
	 */
	public void setObjectType(java.lang.String objectType) {
		set("object_type", objectType);
	}
	
	/**
	 * 目标类型
	 */
	public java.lang.String getObjectType() {
		return getStr("object_type");
	}

	/**
	 * 目标id
	 */
	public void setObjectId(java.lang.String objectId) {
		set("object_id", objectId);
	}
	
	/**
	 * 目标id
	 */
	public java.lang.String getObjectId() {
		return getStr("object_id");
	}

	/**
	 * 时长
	 */
	public void setTime(java.lang.Long time) {
		set("time", time);
	}
	
	/**
	 * 时长
	 */
	public java.lang.Long getTime() {
		return getLong("time");
	}

	/**
	 * 分享次数
	 */
	public void setShare(java.lang.Long share) {
		set("share", share);
	}
	
	/**
	 * 分享次数
	 */
	public java.lang.Long getShare() {
		return getLong("share");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 备注信息
	 */
	public void setMark(java.lang.String mark) {
		set("mark", mark);
	}
	
	/**
	 * 备注信息
	 */
	public java.lang.String getMark() {
		return getStr("mark");
	}

}
