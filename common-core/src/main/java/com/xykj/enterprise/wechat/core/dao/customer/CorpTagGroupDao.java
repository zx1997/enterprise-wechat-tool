package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CorpTagGroup;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-12 10:24
 */
public interface CorpTagGroupDao {

    CorpTagGroup get(String corpid, String groupId);

    List<CorpTagGroup> findAll(String corpid);

    CorpTagGroup save(
            String corpid,
            String groupId,
            String name,
            Integer order,
            String deleted
    );

    void update(
            Long id,
            String corpid,
            String groupId,
            String name,
            Integer order,
            String deleted
    );
}
