package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.core.dao.SuiteInfoDao;
import com.xykj.enterprise.wechat.core.model.dodb.SuiteInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author george
 * @create 2021-05-28 15:27
 */
@Service
public class SuiteServiceImpl implements SuiteService {
    @Autowired
    private SuiteInfoDao suiteInfoDao;

    @Override
    public SuiteInfo getSecret(String suiteId) {
        return suiteInfoDao.get(suiteId);
    }
}
