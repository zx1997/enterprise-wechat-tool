package com.xykj.enterprise.wechat.core.service.wxpush;

/**
 * @Author zhouxu
 * @create 2021-02-25 11:27
 */
public interface WxpushService {

    void saveLog(String msgSignatuer, String timestamp, String nonce, String data, String msg, String jsonMsg, String msgType, String event, String changeType);

}
