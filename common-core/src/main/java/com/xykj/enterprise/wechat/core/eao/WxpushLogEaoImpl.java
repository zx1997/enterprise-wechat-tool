package com.xykj.enterprise.wechat.core.eao;

import com.alibaba.fastjson.JSON;
import com.xykj.enterprise.wechat.core.es.EsClient;
import com.xykj.enterprise.wechat.core.model.dodb.WxpushLog;
import com.xykj.enterprise.wechat.core.model.eos.WxpushLogEo;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import com.ydn.dbframe.plugin.activerecord.Page;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author zhouxu
 * @create 2021-02-26 10:01
 */
@Component
public class WxpushLogEaoImpl extends BaseEao<WxpushLogEo> implements WxpushLogEao {

    @Autowired
    private EsClient esClient;

    private static final String INDEX_NAME = "wxpush_log";
    private static final String TYPE_NAME = "wxpush_log";

    @Override
    public void save(String msgSignatuer, String timestamp, String nonce, String data, String msg, String jsonMsg,
                     String msgType, String event, String changeType) {
        WxpushLogEo eo = new WxpushLogEo();
        eo.setMsgSignature(msgSignatuer);
        eo.setTimestamp(timestamp);
        eo.setNonce(nonce);
        eo.setData(data);
        eo.setMsg(msg);
        eo.setJsonMsg(jsonMsg);
        eo.setStatus(WxpushLog.STATUS_INIT);
        Date now = new Date();
        eo.setCreateTime(now.getTime());
        eo.setDayTime(TimeUtil.getTimeYYMMDD(now));
        eo.setMsgType(msgType);
        eo.setEvent(event);
        eo.setChangeType(changeType);

        esClient.getClient()
                .prepareIndex(INDEX_NAME, TYPE_NAME)
                .setSource(JSON.parseObject(JSON.toJSONString(eo)))
                .get()
        ;

    }

    @Override
    public Page<WxpushLogEo> listByDayTime(Integer dayTime, Integer pageNumber, Integer pageSize, Class<WxpushLogEo> clazz) {
        pageNumber = pageNumber - 1;

        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.filter().add(QueryBuilders.termQuery("dayTime", dayTime));

        SearchResponse response = esClient.getClient()
                .prepareSearch(INDEX_NAME)
                .setTypes(TYPE_NAME)
                .setQuery(queryBuilder)
                .setFrom(pageNumber * (pageSize - 1))
                .setSize(pageSize)
                .addSort(
                        SortBuilders.fieldSort("createTime").order(SortOrder.DESC)
                ).get();

        return toPage(response, pageNumber, pageSize, clazz);
    }

    @Override
    public WxpushLogEo queryById(String logId) {
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.filter().add(QueryBuilders.termQuery("logId", logId));

        SearchResponse response = esClient.getClient()
                .prepareSearch(INDEX_NAME)
                .setTypes(TYPE_NAME)
                .setQuery(queryBuilder)
                .setSize(1)
                .get();

        if (response.getHits().totalHits > 0) {
            SearchHit hit = response.getHits().getAt(0);
            return JSON.parseObject(JSON.toJSONString(hit.getSourceAsMap()), WxpushLogEo.class);
        }

        return null;
    }


}
