package com.xykj.enterprise.wechat.core.service.sys;

import com.xykj.enterprise.wechat.core.model.dodb.SysParam;

/**
 * 系统运行参数
 *
 * @Author zhouxu
 * @create 2021-04-01 16:19
 */
public interface SysParamService {

    SysParam getByCode(String code);

}
