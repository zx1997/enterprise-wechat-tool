package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.contacts.department.DepartmentListVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.department.DepartmentVo;
import com.xykj.enterprise.wechat.core.dao.CorpExtDao;
import com.xykj.enterprise.wechat.core.dao.DepartmentDao;
import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;
import com.xykj.enterprise.wechat.core.model.dodb.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-09 09:11
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {
    @Autowired
    private DepartmentDao departmentDao;
    @Autowired
    private CorpExtDao corpExtDao;

    @Override
    public List<Department> list(String corpid,Integer parentid) {
        return departmentDao.list(corpid,parentid);
    }

    @Override
    public void syncDepartList(DepartmentListVo listVo, String corpid) {

        if (listVo != null && !listVo.getDepartment().isEmpty()) {
            for (DepartmentVo vo : listVo.getDepartment()) {
                Department tmp = departmentDao.findByDepartId(vo.getId(), corpid);
                if (tmp == null) {

                    departmentDao.save(
                            vo.getId(),
                            vo.getName(),
                            vo.getNameEn(),
                            vo.getParentId(),
                            vo.getOrder(),
                            corpid
                    );

                } else {

                    departmentDao.update(
                            tmp.getId(),
                            vo.getId(),
                            vo.getName(),
                            vo.getNameEn(),
                            vo.getParentId(),
                            vo.getOrder(),
                            corpid
                    );
                }
            }
        }

        // 同步完成标记
        corpExtDao.syncDepartmentData(corpid, CorpExt.SUCC);

    }

}
