package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.core.model.dodb.SuiteInfo;

/**
 * @Author george
 * @create 2021-05-28 15:27
 */
public interface SuiteService {

    SuiteInfo getSecret(String suiteId);

}
