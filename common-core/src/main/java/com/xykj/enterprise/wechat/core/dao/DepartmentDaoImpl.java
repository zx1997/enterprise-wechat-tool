package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.Department;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-06 17:14
 */
@Repository
public class DepartmentDaoImpl implements DepartmentDao {

    @Override
    public void save(Integer departId, String name, String nameEn, Integer parentid, Integer order, String corpid) {
        Department depart = new Department();
        depart.setDepartId(departId);
        depart.setName(name);
        depart.setNameEn(nameEn);
        depart.setParentid(parentid);
        depart.setOrder(order);
        depart.setCorpid(corpid);
        depart.setCreateTime(new Date());
        depart.save();
    }

    @Override
    public Department findByDepartId(Integer id, String corpid) {
        String sql = "select * from t_department where depart_id = ? and corpid = ?";
        return Department.dao.findFirst(sql, id, corpid);
    }

    @Override
    public void update(Long id, Integer departId, String name, String nameEn, Integer parentid, Integer order, String corpid) {
        Department depart = Department.dao.findById(id);
        depart.setDepartId(departId);
        depart.setName(name);
        depart.setNameEn(nameEn);
        depart.setParentid(parentid);
        depart.setOrder(order);
        depart.setCorpid(corpid);
        depart.setUpdateTime(new Date());
        depart.update();

    }

    @Override
    public List<Department> list(String corpid, Integer parentid) {
        String sql = "select * from t_department where corpid = ? and parentid = ? ";

        return Department.dao.find(sql, corpid, parentid);
    }
}
