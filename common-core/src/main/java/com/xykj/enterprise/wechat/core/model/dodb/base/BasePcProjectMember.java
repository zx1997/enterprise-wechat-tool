package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BasePcProjectMember<M extends BasePcProjectMember<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业标识
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业标识
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 项目标识
	 */
	public void setProjectId(java.lang.Integer projectId) {
		set("project_id", projectId);
	}
	
	/**
	 * 项目标识
	 */
	public java.lang.Integer getProjectId() {
		return getInt("project_id");
	}

	/**
	 * 参与人标识
	 */
	public void setMemberId(java.lang.String memberId) {
		set("member_id", memberId);
	}
	
	/**
	 * 参与人标识
	 */
	public java.lang.String getMemberId() {
		return getStr("member_id");
	}

	/**
	 * 参与人内容
	 */
	public void setMemberContent(java.lang.String memberContent) {
		set("member_content", memberContent);
	}
	
	/**
	 * 参与人内容
	 */
	public java.lang.String getMemberContent() {
		return getStr("member_content");
	}

	/**
	 * 计划开始时间
	 */
	public void setPlanStartTime(java.util.Date planStartTime) {
		set("plan_start_time", planStartTime);
	}
	
	/**
	 * 计划开始时间
	 */
	public java.util.Date getPlanStartTime() {
		return get("plan_start_time");
	}

	/**
	 * 实际开始时间
	 */
	public void setRealStartTime(java.util.Date realStartTime) {
		set("real_start_time", realStartTime);
	}
	
	/**
	 * 实际开始时间
	 */
	public java.util.Date getRealStartTime() {
		return get("real_start_time");
	}

	/**
	 * 计划结束时间
	 */
	public void setPlanEndTime(java.util.Date planEndTime) {
		set("plan_end_time", planEndTime);
	}
	
	/**
	 * 计划结束时间
	 */
	public java.util.Date getPlanEndTime() {
		return get("plan_end_time");
	}

	/**
	 * 实际结束时间
	 */
	public void setRealEndTime(java.util.Date realEndTime) {
		set("real_end_time", realEndTime);
	}
	
	/**
	 * 实际结束时间
	 */
	public java.util.Date getRealEndTime() {
		return get("real_end_time");
	}

	/**
	 * 创建人
	 */
	public void setCreator(java.lang.String creator) {
		set("creator", creator);
	}
	
	/**
	 * 创建人
	 */
	public java.lang.String getCreator() {
		return getStr("creator");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 修改时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	/**
	 * 修改时间
	 */
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

	/**
	 * 修改人
	 */
	public void setUpdateor(java.lang.String updateor) {
		set("updateor", updateor);
	}
	
	/**
	 * 修改人
	 */
	public java.lang.String getUpdateor() {
		return getStr("updateor");
	}

}
