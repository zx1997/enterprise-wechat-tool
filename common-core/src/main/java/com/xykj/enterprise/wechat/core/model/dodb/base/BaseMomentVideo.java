package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMomentVideo<M extends BaseMomentVideo<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 视频media_id
	 */
	public void setMediaId(java.lang.String mediaId) {
		set("media_id", mediaId);
	}
	
	/**
	 * 视频media_id
	 */
	public java.lang.String getMediaId() {
		return getStr("media_id");
	}

	/**
	 * 视频封面media_id
	 */
	public void setThumbMediaId(java.lang.String thumbMediaId) {
		set("thumb_media_id", thumbMediaId);
	}
	
	/**
	 * 视频封面media_id
	 */
	public java.lang.String getThumbMediaId() {
		return getStr("thumb_media_id");
	}

	/**
	 * 朋友圈ID
	 */
	public void setMomentId(java.lang.String momentId) {
		set("moment_id", momentId);
	}
	
	/**
	 * 朋友圈ID
	 */
	public java.lang.String getMomentId() {
		return getStr("moment_id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
