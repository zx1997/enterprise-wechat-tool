package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseCorpExt;

/**
 *
 */
@SuppressWarnings("serial")
public class CorpExt extends BaseCorpExt<CorpExt> {
    public static final CorpExt dao = new CorpExt().dao();

    public static final String SUCC = "1";

    public boolean isDepartmentSync() {
        return SUCC.equals(getDepartmentSync());
    }

    public boolean isUserSync() {
        return SUCC.equals(getUserSync());
    }

    public boolean isCustomerSync() {
        return SUCC.equals(getCustomerSync());
    }

    public boolean isCorpTagSync() {
        return SUCC.equals(getCorpTagSync());
    }

    public boolean isGroupChatSync() {
        return SUCC.equals(getGroupChatSync());
    }

    public boolean isMomentSync() {
        return SUCC.equals(getMomentSync());
    }
}
