package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.GroupChat;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-04-22 13:49
 */
@Repository
public class GroupChatDaoImpl implements GroupChatDao {

    @Override
    public GroupChat get(String corpid, String chatId) {
        String sql = " select * from t_group_chat where corpid = ? and chat_id = ? ";
        return GroupChat.dao.findFirst(sql,corpid,chatId);
    }

    @Override
    public Long save(String corpid, String chatId, String name, String owner, String notice, String adminList) {
        GroupChat chat = new GroupChat();
        chat.setCorpid(corpid);
        chat.setChatId(chatId);
        chat.setName(name);
        chat.setOwner(owner);
        chat.setNotice(notice);
        chat.setAdminList(adminList);
        chat.setCreateTime(new Date());
        chat.save();

        return chat.getId();
    }

    @Override
    public void update(Long id, String corpid, String chatId, String name, String owner, String notice, String adminList) {
        GroupChat chat = GroupChat.dao.findById(id);
        if (chat != null) {
            chat.setCorpid(corpid);
            chat.setChatId(chatId);
            chat.setName(name);
            chat.setOwner(owner);
            chat.setNotice(notice);
            chat.setAdminList(adminList);
            chat.setUpdateTime(new Date());
            chat.update();
        }
    }

    @Override
    public Page<GroupChat> list(Integer page, Integer limit, String corpid) {
        Map<String, Object> param = new HashMap<>();
        param.put("corpid", corpid);
        SqlParse sqlParse = new SqlParse(param);
        sqlParse.addSQL(" select * from t_group_chat ");
        sqlParse.addSQL(" where 1=1 ");
        sqlParse.addSQL(" corpid = :corpid ");

        return GroupChat.dao.paginate(page, limit, sqlParse);
    }
}
