package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcProject;
import com.ydn.dbframe.plugin.activerecord.Page;

/**
 * @Author george
 * @create 2021-05-12 15:14
 */
public interface PCProjectService {

    Page<PcProject> list(Integer page, Integer limit, String corpid, String userid,String projectProcess);

    PcProject get(Integer id,String corpid,String userid);

    void save(ProjectVo vo, String corpid, String userId);

    void update(ProjectVo vo, String corpid, String userId);

    void delete(Integer id, String corpid, String userId);

}
