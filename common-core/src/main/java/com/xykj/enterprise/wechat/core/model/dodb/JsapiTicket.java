package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseJsapiTicket;

import java.util.Calendar;

/**
 *
 */
@SuppressWarnings("serial")
public class JsapiTicket extends BaseJsapiTicket<JsapiTicket> {
    public static final JsapiTicket dao = new JsapiTicket().dao();

    // 是否失效
    public boolean isAgentTicketExpire() {
        if (getAgentTicketExp() == null) {
            return true;
        }
        Long exp = getAgentTicketExp().getTime();
        // 当前时间大于等于过期时间，失效
        return Calendar.getInstance().getTimeInMillis() >= exp;
    }

    // 是否失效
    public boolean isCorpTicketExpire() {
        if (getCorpTicketExp() == null) {
            return true;
        }
        Long exp = getCorpTicketExp().getTime();
        // 当前时间大于等于过期时间，失效
        return Calendar.getInstance().getTimeInMillis() >= exp;
    }
}
