package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.config.DbConfig;
import com.xykj.enterprise.wechat.core.model.dodb.MomentImage;
import com.ydn.dbframe.plugin.activerecord.Db;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author george
 * @create 2021-05-10 11:43
 */
@Repository
public class MomentImageDaoImpl implements MomentImageDao {
    @Override
    public MomentImage get(String momentId, String corpid) {
        String sql = " select * from t_moment_image where corpid = ? and moment_id = ? ";
        return MomentImage.dao.findFirst(sql, corpid, momentId);
    }

    @Override
    public void save(String mediaId, String corpid, String momentId) {
        MomentImage image = new MomentImage();
        image.setMediaId(mediaId);
        image.setCorpid(corpid);
        image.setMomentId(momentId);
        image.setCreateTime(new Date());
        image.save();
    }

    @Override
    public void delete(String momentId, String corpid) {
        String sql = " delete from t_moment_image where corpid = ? and moment_id = ? limit 1 ";
        Db.use(DbConfig.DATASOURCE).update(sql, momentId, corpid);
    }
}
