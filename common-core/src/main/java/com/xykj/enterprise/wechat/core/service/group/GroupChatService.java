package com.xykj.enterprise.wechat.core.service.group;

import com.xykj.enterprise.wechat.bean.ext.externalcontact.GroupChatVo;

/**
 * @Author george
 * @create 2021-04-22 14:00
 */
public interface GroupChatService {

    void save(String corpid, GroupChatVo info);

    void syncSucc(String corpid);

}
