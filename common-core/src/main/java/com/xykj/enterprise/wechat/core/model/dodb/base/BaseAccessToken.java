package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAccessToken<M extends BaseAccessToken<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 应用场景编码
	 */
	public void setSenceCode(java.lang.String senceCode) {
		set("sence_code", senceCode);
	}
	
	/**
	 * 应用场景编码
	 */
	public java.lang.String getSenceCode() {
		return getStr("sence_code");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 应用密钥
	 */
	public void setCorpsecret(java.lang.String corpsecret) {
		set("corpsecret", corpsecret);
	}
	
	/**
	 * 应用密钥
	 */
	public java.lang.String getCorpsecret() {
		return getStr("corpsecret");
	}

	/**
	 * 请求地址
	 */
	public void setUrl(java.lang.String url) {
		set("url", url);
	}
	
	/**
	 * 请求地址
	 */
	public java.lang.String getUrl() {
		return getStr("url");
	}

	/**
	 * token有效时间（7200s，两小时）
	 */
	public void setExpires(java.lang.Integer expires) {
		set("expires", expires);
	}
	
	/**
	 * token有效时间（7200s，两小时）
	 */
	public java.lang.Integer getExpires() {
		return getInt("expires");
	}

	public void setEffTime(java.util.Date effTime) {
		set("eff_time", effTime);
	}
	
	public java.util.Date getEffTime() {
		return get("eff_time");
	}

	public void setExpTime(java.util.Date expTime) {
		set("exp_time", expTime);
	}
	
	public java.util.Date getExpTime() {
		return get("exp_time");
	}

}
