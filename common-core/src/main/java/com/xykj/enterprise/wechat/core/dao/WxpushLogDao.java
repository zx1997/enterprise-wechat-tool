package com.xykj.enterprise.wechat.core.dao;

/**
 * @Author zhouxu
 * @create 2021-02-25 11:19
 */
public interface WxpushLogDao {

    void save(String msgSignatuer, String timestamp, String nonce, String data, String msg, String jsonMsg,
              String msgType, String event, String changeType);

}
