package com.xykj.enterprise.wechat.core.model.eos;

import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-02-26 09:54
 */
@Data
public class WxpushLogEo {
    // uuid
    private String logId;
    // 签名文本
    private String msgSignature;
    // 时间戳
    private String timestamp;
    // 随机数
    private String nonce;
    // xml数据（加密）
    private String data;
    // 消息类型
    private String msgType;
    // 事件类型
    private String event;
    // 动作类型
    private String changeType;
    // xml数据（解密）
    private String msg;
    // json格式数据
    private String jsonMsg;
    private String status;
    private Integer dayTime;
    private long createTime;
    private long updateTime;


}
