package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomerTag<M extends BaseCustomerTag<M>> extends Model<M> implements IBean {

	/**
	 * id
	 */
	public void setTagId(java.lang.Long tagId) {
		set("tag_id", tagId);
	}
	
	/**
	 * id
	 */
	public java.lang.Long getTagId() {
		return getLong("tag_id");
	}

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 添加外部联系人的userid
	 */
	public void setUserId(java.lang.String userId) {
		set("user_id", userId);
	}
	
	/**
	 * 添加外部联系人的userid
	 */
	public java.lang.String getUserId() {
		return getStr("user_id");
	}

	/**
	 * 外部联系人userid
	 */
	public void setExternalUserid(java.lang.String externalUserid) {
		set("external_userid", externalUserid);
	}
	
	/**
	 * 外部联系人userid
	 */
	public java.lang.String getExternalUserid() {
		return getStr("external_userid");
	}

	/**
	 * 标签组名称
	 */
	public void setTagName(java.lang.String tagName) {
		set("tag_name", tagName);
	}
	
	/**
	 * 标签组名称
	 */
	public java.lang.String getTagName() {
		return getStr("tag_name");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 是否已经被删除 0-正常 1-被删除
	 */
	public void setDeleted(java.lang.String deleted) {
		set("deleted", deleted);
	}
	
	/**
	 * 是否已经被删除 0-正常 1-被删除
	 */
	public java.lang.String getDeleted() {
		return getStr("deleted");
	}

	/**
	 * 选中状态（0未选中 1选中）
	 */
	public void setIsSelect(java.lang.String isSelect) {
		set("is_select", isSelect);
	}
	
	/**
	 * 选中状态（0未选中 1选中）
	 */
	public java.lang.String getIsSelect() {
		return getStr("is_select");
	}

}
