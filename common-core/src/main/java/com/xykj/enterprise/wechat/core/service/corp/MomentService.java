package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentListVo;
import com.xykj.enterprise.wechat.core.model.dodb.Moment;
import com.ydn.dbframe.plugin.activerecord.Page;

/**
 * 朋友圈（企业展示给客户）
 *
 * @Author george
 * @create 2021-05-10 11:45
 */
public interface MomentService {

    Page<Moment> page(Integer page, Integer limit);

    /**
     * 同步保存朋友圈数据
     * @param corpid
     * @param listVo
     */
    void save(String corpid, GetMomentListVo listVo);

}
