package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.GroupChat;
import com.ydn.dbframe.plugin.activerecord.Page;

/**
 * 聊天群
 *
 * @Author george
 * @create 2021-04-22 13:44
 */
public interface GroupChatDao {

    GroupChat get(String corpid, String chatId);

    Long save(
            String corpid,
            String chatId,
            String name,
            String owner,
            String notice,
            String adminList

    );

    void update(
            Long id,
            String corpid,
            String chatId,
            String name,
            String owner,
            String notice,
            String adminList
    );

    Page<GroupChat> list(Integer page, Integer limit, String corpid);

}
