package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.AgentInfo;
import org.springframework.stereotype.Repository;

/**
 * @Author george
 * @create 2021-05-28 17:41
 */
@Repository
public class AgentInfoDaoImpl implements AgentInfoDao {


    @Override
    public AgentInfo get(String corpid, String suiteId) {
        String sql = " select * from t_agent_info where corpid = ? and suite_id = ? ";
        return AgentInfo.dao.findFirst(sql, corpid, suiteId);
    }

    @Override
    public void save(
            String corpid,
            String suiteId,
            String agentId,
            String name,
            String roundLogoUrl,
            String squareLogoUrl,
            String appid,
            String privilege,
            String shareFrom
    ) {
        AgentInfo info = new AgentInfo();
        info.setCorpid(corpid);
        info.setSuiteId(suiteId);
        info.setAgentid(agentId);
        info.setName(name);
        info.setRoundLogoUrl(roundLogoUrl);
        info.setSquareLogoUrl(squareLogoUrl);
        info.setAppid(appid);
        info.setPrivilege(privilege);
        info.setShareFrom(shareFrom);
        info.save();

    }
}
