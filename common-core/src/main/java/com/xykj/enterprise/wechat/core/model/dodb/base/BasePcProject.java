package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BasePcProject<M extends BasePcProject<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Integer getId() {
		return getInt("id");
	}

	/**
	 * 企业标识
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业标识
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 项目名称
	 */
	public void setProjectName(java.lang.String projectName) {
		set("project_name", projectName);
	}
	
	/**
	 * 项目名称
	 */
	public java.lang.String getProjectName() {
		return getStr("project_name");
	}

	/**
	 * 项目阶段
	 */
	public void setProjectProcess(java.lang.String projectProcess) {
		set("project_process", projectProcess);
	}
	
	/**
	 * 项目阶段
	 */
	public java.lang.String getProjectProcess() {
		return getStr("project_process");
	}

	/**
	 * 项目负责人
	 */
	public void setProjectManager(java.lang.String projectManager) {
		set("project_manager", projectManager);
	}
	
	/**
	 * 项目负责人
	 */
	public java.lang.String getProjectManager() {
		return getStr("project_manager");
	}

	/**
	 * 项目签订日期
	 */
	public void setSignDay(java.lang.Integer signDay) {
		set("sign_day", signDay);
	}
	
	/**
	 * 项目签订日期
	 */
	public java.lang.Integer getSignDay() {
		return getInt("sign_day");
	}

	/**
	 * 项目金额
	 */
	public void setProjectAmount(java.math.BigDecimal projectAmount) {
		set("project_amount", projectAmount);
	}
	
	/**
	 * 项目金额
	 */
	public java.math.BigDecimal getProjectAmount() {
		return get("project_amount");
	}

	/**
	 * 销售费用
	 */
	public void setSalesExpense(java.math.BigDecimal salesExpense) {
		set("sales_expense", salesExpense);
	}
	
	/**
	 * 销售费用
	 */
	public java.math.BigDecimal getSalesExpense() {
		return get("sales_expense");
	}

	/**
	 * 开发预计开始时间
	 */
	public void setPlanDevStartTime(java.util.Date planDevStartTime) {
		set("plan_dev_start_time", planDevStartTime);
	}
	
	/**
	 * 开发预计开始时间
	 */
	public java.util.Date getPlanDevStartTime() {
		return get("plan_dev_start_time");
	}

	/**
	 * 客户要求结束时间
	 */
	public void setCustomerReqEndTime(java.util.Date customerReqEndTime) {
		set("customer_req_end_time", customerReqEndTime);
	}
	
	/**
	 * 客户要求结束时间
	 */
	public java.util.Date getCustomerReqEndTime() {
		return get("customer_req_end_time");
	}

	/**
	 * 实际开始时间
	 */
	public void setRealStartTime(java.util.Date realStartTime) {
		set("real_start_time", realStartTime);
	}
	
	/**
	 * 实际开始时间
	 */
	public java.util.Date getRealStartTime() {
		return get("real_start_time");
	}

	/**
	 * 实际结束时间
	 */
	public void setRealEndTime(java.util.Date realEndTime) {
		set("real_end_time", realEndTime);
	}
	
	/**
	 * 实际结束时间
	 */
	public java.util.Date getRealEndTime() {
		return get("real_end_time");
	}

	/**
	 * 维护开始时间
	 */
	public void setMaintainStartTime(java.util.Date maintainStartTime) {
		set("maintain_start_time", maintainStartTime);
	}
	
	/**
	 * 维护开始时间
	 */
	public java.util.Date getMaintainStartTime() {
		return get("maintain_start_time");
	}

	/**
	 * 免费维护时长
	 */
	public void setFreeMaintainDay(java.lang.Integer freeMaintainDay) {
		set("free_maintain_day", freeMaintainDay);
	}
	
	/**
	 * 免费维护时长
	 */
	public java.lang.Integer getFreeMaintainDay() {
		return getInt("free_maintain_day");
	}

	/**
	 * 维护费用
	 */
	public void setMaintainFee(java.math.BigDecimal maintainFee) {
		set("maintain_fee", maintainFee);
	}
	
	/**
	 * 维护费用
	 */
	public java.math.BigDecimal getMaintainFee() {
		return get("maintain_fee");
	}

	/**
	 * 创建人
	 */
	public void setCreator(java.lang.String creator) {
		set("creator", creator);
	}
	
	/**
	 * 创建人
	 */
	public java.lang.String getCreator() {
		return getStr("creator");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 修改时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	/**
	 * 修改时间
	 */
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

	/**
	 * 修改人
	 */
	public void setUpdator(java.lang.String updator) {
		set("updator", updator);
	}
	
	/**
	 * 修改人
	 */
	public java.lang.String getUpdator() {
		return getStr("updator");
	}

}
