package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.GetMomentListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.MomentImageVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.MomentLinkVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.MomentLocationVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.MomentTextVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.MomentVideoVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.moment.MomentVo;
import com.xykj.enterprise.wechat.core.dao.MomentDao;
import com.xykj.enterprise.wechat.core.dao.MomentImageDao;
import com.xykj.enterprise.wechat.core.dao.MomentLinkDao;
import com.xykj.enterprise.wechat.core.dao.MomentLocationDao;
import com.xykj.enterprise.wechat.core.dao.MomentTextDao;
import com.xykj.enterprise.wechat.core.dao.MomentVideoDao;
import com.xykj.enterprise.wechat.core.model.dodb.Moment;
import com.ydn.dbframe.plugin.activerecord.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author george
 * @create 2021-05-10 11:52
 */
@Service
public class MomentServiceImpl implements MomentService {

    @Autowired
    private MomentDao momentDao;
    @Autowired
    private MomentImageDao momentImageDao;
    @Autowired
    private MomentLinkDao momentLinkDao;
    @Autowired
    private MomentLocationDao momentLocationDao;
    @Autowired
    private MomentTextDao momentTextDao;
    @Autowired
    private MomentVideoDao momentVideoDao;

    @Override
    public Page<Moment> page(Integer page, Integer limit) {
        // TODO 查询
        return null;
    }

    @Override
    public void save(String corpid, GetMomentListVo listVo) {
        if (listVo == null || listVo.getMoment_list().isEmpty()) {
            return;
        }
        for (MomentVo moment : listVo.getMoment_list()) {
            Moment m = momentDao.get(corpid, moment.getMoment_id());
            if (m == null) {
                momentDao.save(
                        moment.getMoment_id(),
                        corpid,
                        String.valueOf(moment.getCreator()),
                        String.valueOf(moment.getCreate_time()),
                        String.valueOf(moment.getCreate_type()),
                        String.valueOf(moment.getVisible_type())
                );

            } else {
                momentDao.update(
                        m.getId(),
                        moment.getMoment_id(),
                        corpid,
                        String.valueOf(moment.getCreator()),
                        String.valueOf(moment.getCreate_time()),
                        String.valueOf(moment.getCreate_type()),
                        String.valueOf(moment.getVisible_type())
                );
            }

            // 文本
            MomentTextVo textVo = moment.getText();
            momentTextDao.delete(moment.getMoment_id(), corpid);
            if (textVo != null) {
                momentTextDao.save(
                        textVo.getContent(),
                        corpid,
                        moment.getMoment_id()
                );
            }

            // 图片
            List<MomentImageVo> imageVoList = moment.getImage();
            momentImageDao.delete(moment.getMoment_id(), corpid);
            if (imageVoList != null){
                for (MomentImageVo vo : imageVoList){
                    momentImageDao.save(
                            vo.getMedia_id(),
                            corpid,
                            moment.getMoment_id()
                    );
                }
            }

            // 视频
            MomentVideoVo videoVo = moment.getVideo();
            momentVideoDao.delete(moment.getMoment_id(), corpid);
            if (videoVo != null) {
                momentVideoDao.save(
                        videoVo.getMedia_id(),
                        videoVo.getThumb_media_id(),
                        corpid,
                        moment.getMoment_id()
                );
            }

            // 链接
            MomentLinkVo linkVo = moment.getLink();
            momentLinkDao.delete(moment.getMoment_id(), corpid);
            if (linkVo != null) {
                momentLinkDao.save(
                        linkVo.getTitle(),
                        linkVo.getUrl(),
                        corpid,
                        moment.getMoment_id()
                );
            }

            // 坐标
            MomentLocationVo locationVo = moment.getLocation();
            momentLocationDao.delete(moment.getMoment_id(), corpid);
            if (locationVo != null) {
                momentLocationDao.save(
                        locationVo.getLatitude(),
                        locationVo.getLongitude(),
                        locationVo.getName(),
                        corpid,
                        moment.getMoment_id()
                );
            }

        }
    }
}
