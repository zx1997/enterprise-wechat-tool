package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author zhouxu
 * @create 2021-04-07 16:58
 */
@Repository
public class CorpExtDaoImpl implements CorpExtDao {

    @Override
    public CorpExt get(String corpid) {
        return CorpExt.dao.findById(corpid);
    }

    @Override
    public void save(String corpid) {
        CorpExt ext = new CorpExt();
        ext.setCorpid(corpid);
        ext.setCreateTime(new Date());
        ext.save();
    }

    @Override
    public void syncDepartmentData(String corpid, String sign) {
        CorpExt ext = CorpExt.dao.findById(corpid);
        ext.setDepartmentSync(sign);
        ext.setUpdateTime(new Date());
        ext.update();
    }

    @Override
    public void syncUserData(String corpid, String sign) {
        CorpExt ext = CorpExt.dao.findById(corpid);
        ext.setUserSync(sign);
        ext.setUpdateTime(new Date());
        ext.update();
    }

    @Override
    public void syncCustomerData(String corpid, String sign) {
        CorpExt ext = CorpExt.dao.findById(corpid);
        ext.setCustomerSync(sign);
        ext.setUpdateTime(new Date());
        ext.update();
    }

    @Override
    public void syncCorpTagData(String corpid, String sign) {
        CorpExt ext = CorpExt.dao.findById(corpid);
        ext.setCorpTagSync(sign);
        ext.setUpdateTime(new Date());
        ext.update();
    }

    @Override
    public void syncGroupChatData(String corpid, String sign) {
        CorpExt ext = CorpExt.dao.findById(corpid);
        ext.setGroupChatSync(sign);
        ext.setUpdateTime(new Date());
        ext.update();
    }
}
