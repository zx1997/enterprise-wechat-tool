package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAuthCorp<M extends BaseAuthCorp<M>> extends Model<M> implements IBean {

	/**
	 * 授权方企业微信id

	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 授权方企业微信id

	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 授权方企业信息\n
	 */
	public void setAuthCorpInfo(java.lang.String authCorpInfo) {
		set("auth_corp_info", authCorpInfo);
	}
	
	/**
	 * 授权方企业信息\n
	 */
	public java.lang.String getAuthCorpInfo() {
		return getStr("auth_corp_info");
	}

	/**
	 * 授权信息
	 */
	public void setAuthInfo(java.lang.String authInfo) {
		set("auth_info", authInfo);
	}
	
	/**
	 * 授权信息
	 */
	public java.lang.String getAuthInfo() {
		return getStr("auth_info");
	}

	/**
	 * 授权管理员的信息
	 */
	public void setAuthUserInfo(java.lang.String authUserInfo) {
		set("auth_user_info", authUserInfo);
	}
	
	/**
	 * 授权管理员的信息
	 */
	public java.lang.String getAuthUserInfo() {
		return getStr("auth_user_info");
	}

	/**
	 * 代理服务商企业信息
	 */
	public void setDealerCorpInfo(java.lang.String dealerCorpInfo) {
		set("dealer_corp_info", dealerCorpInfo);
	}
	
	/**
	 * 代理服务商企业信息
	 */
	public java.lang.String getDealerCorpInfo() {
		return getStr("dealer_corp_info");
	}

	/**
	 * 推广二维码安装相关信息，扫推广二维码安装时返回
	 */
	public void setRegisterCodeInfo(java.lang.String registerCodeInfo) {
		set("register_code_info", registerCodeInfo);
	}
	
	/**
	 * 推广二维码安装相关信息，扫推广二维码安装时返回
	 */
	public java.lang.String getRegisterCodeInfo() {
		return getStr("register_code_info");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
