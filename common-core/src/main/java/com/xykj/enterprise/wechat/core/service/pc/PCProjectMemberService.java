package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectMemberVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcProjectMember;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.Map;

/**
 * @Author george
 * @create 2021-05-12 15:35
 */
public interface PCProjectMemberService {

    Page<PcProjectMember> list(Integer page, Integer limit, Map<String,Object> param);

    PcProjectMember get(Integer id,String corpid,String userId);

    void save(ProjectMemberVo vo,String corpid,String userId);

    void update(ProjectMemberVo vo,String corpid,String userId);

    void delete(Long id,String corpid,String userId);

}
