package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.AccessToken;
import com.xykj.enterprise.wechat.bean.ext.identity.SuiteAccessToken;
import com.xykj.enterprise.wechat.core.dao.CorpSuiteDao;
import com.xykj.enterprise.wechat.core.dao.SuiteTicketDao;
import com.xykj.enterprise.wechat.core.model.dodb.CorpSuite;
import com.xykj.enterprise.wechat.util.biz.BizUtil;
import com.xykj.enterprise.wechat.util.wecom.identity.IdentityUtil;
import com.xykj.enterprise.wechat.util.wecom.identity.SuiteAccessTokenUtil;
import com.ydn.simplecache.SimpleCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author george
 * @create 2021-04-28 15:38
 */
@Slf4j
@Service
public class IdentityServiceImpl implements IdentityService {

    @Autowired
    private CorpSuiteDao corpSuiteDao;
    @Autowired
    private SimpleCache simpleCache;
    @Autowired
    private SuiteTicketDao suiteTicketDao;

    @Override
    public String getAccessToken(String corpid, String suiteId, String secret) {
        CorpSuite param = corpSuiteDao.get(corpid, suiteId);
        if (param == null) {
            BizUtil.exception("未找到token记录，corpid=" + corpid + " 或 suitid=" + suiteId + " 异常");
        }
        // 校验 token 是否失效
        if (param.isExpired()) {
            param = refreshAccessToken(corpid, suiteId, secret);
        }
        if (param == null) {
            BizUtil.exception("access_token 异常");
        }
        return param.getAccessToken();
    }

    @Override
    public CorpSuite refreshAccessToken(String corpid, String suiteId, String secret) {
        // 获取 suite_access_token
        SuiteAccessToken suiteAccessToken = SuiteAccessTokenUtil.get(
                simpleCache,
                suiteId,
                secret,
                suiteTicketDao.getLatest(suiteId).getSuitTicket()
        );
        log.debug("*******************suiteAccess_token:{}", suiteAccessToken.toString());

        CorpSuite param = corpSuiteDao.get(corpid, suiteId);
        if (param == null || param.getPermanentCode() == null) {
            BizUtil.exception("数据异常，CorpSuiteParam 为空或永久授权码异常！");
        }
        // 重新获取 access_token
        AccessToken accessToken = IdentityUtil.getCorpToken(
                corpid,
                param.getPermanentCode(),
                suiteAccessToken.getSuite_access_token()
        );
        log.debug("*******************access_token:{}", accessToken.toString());
        // 更新到数据库
        return corpSuiteDao.resetToken(corpid, suiteId, accessToken.getAccess_token(), accessToken.getExpires_in());
    }
}
