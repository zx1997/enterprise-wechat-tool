package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.config.DbConfig;
import com.xykj.enterprise.wechat.core.model.dodb.MomentLocation;
import com.ydn.dbframe.plugin.activerecord.Db;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author george
 * @create 2021-05-10 11:40
 */
@Repository
public class MomentLocationDaoImpl implements MomentLocationDao {
    @Override
    public MomentLocation get(String corpid, String momentId) {
        String sql = " select * from t_moment_location where corpid = ? and moment_id = ? ";
        return MomentLocation.dao.findFirst(sql, corpid, corpid);
    }

    @Override
    public void save(String latitude, String longitude, String name, String corpid, String momentId) {
        MomentLocation location = new MomentLocation();
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        location.setName(name);
        location.setCorpid(corpid);
        location.setMomentId(momentId);
        location.setCreateTime(new Date());
        location.save();
    }

    @Override
    public void delete(String momentId, String corpid) {
        String sql = " delete from t_moment_location where corpid = ? and moment_id = ? limit 1 ";
        Db.use(DbConfig.DATASOURCE).update(sql, momentId, corpid);
    }
}
