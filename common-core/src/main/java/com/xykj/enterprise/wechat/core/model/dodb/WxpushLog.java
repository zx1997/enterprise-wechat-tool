package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseWxpushLog;

/**
 */
@SuppressWarnings("serial")
public class WxpushLog extends BaseWxpushLog<WxpushLog> {
	public static final WxpushLog dao = new WxpushLog().dao();

	public static final String STATUS_INIT = "0";
	public static final String STATUS_FINISH = "1";

}
