package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseSuiteTicket<M extends BaseSuiteTicket<M>> extends Model<M> implements IBean {

	public void setTicketId(java.lang.Long ticketId) {
		set("ticket_id", ticketId);
	}
	
	public java.lang.Long getTicketId() {
		return getLong("ticket_id");
	}

	public void setSuiteId(java.lang.String suiteId) {
		set("suite_id", suiteId);
	}
	
	public java.lang.String getSuiteId() {
		return getStr("suite_id");
	}

	public void setInfoType(java.lang.String infoType) {
		set("info_type", infoType);
	}
	
	public java.lang.String getInfoType() {
		return getStr("info_type");
	}

	public void setTimeStamp(java.lang.Long timeStamp) {
		set("time_stamp", timeStamp);
	}
	
	public java.lang.Long getTimeStamp() {
		return getLong("time_stamp");
	}

	public void setSuitTicket(java.lang.String suitTicket) {
		set("suit_ticket", suitTicket);
	}
	
	public java.lang.String getSuitTicket() {
		return getStr("suit_ticket");
	}

}
