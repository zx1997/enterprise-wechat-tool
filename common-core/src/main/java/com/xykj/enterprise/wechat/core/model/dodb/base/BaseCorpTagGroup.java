package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCorpTagGroup<M extends BaseCorpTagGroup<M>> extends Model<M> implements IBean {

	/**
	 * id
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * id
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 对应腾讯group_id
	 */
	public void setGroupId(java.lang.String groupId) {
		set("group_id", groupId);
	}
	
	/**
	 * 对应腾讯group_id
	 */
	public java.lang.String getGroupId() {
		return getStr("group_id");
	}

	/**
	 * 标签组名称
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 标签组名称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 标签组排序的次序值，order值大的排序靠前。有效的值范围是[0, 2^32)
	 */
	public void setOrder(java.lang.Integer order) {
		set("order", order);
	}
	
	/**
	 * 标签组排序的次序值，order值大的排序靠前。有效的值范围是[0, 2^32)
	 */
	public java.lang.Integer getOrder() {
		return getInt("order");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 是否已经被删除 0-正常 1-被删除
	 */
	public void setDeleted(java.lang.String deleted) {
		set("deleted", deleted);
	}
	
	/**
	 * 是否已经被删除 0-正常 1-被删除
	 */
	public java.lang.String getDeleted() {
		return getStr("deleted");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
