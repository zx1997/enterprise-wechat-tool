package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectVo;
import com.xykj.enterprise.wechat.core.dao.pc.PCProjectDao;
import com.xykj.enterprise.wechat.core.model.dodb.PcProject;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import com.ydn.dbframe.plugin.activerecord.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-25 14:05
 */
@Service
public class PCProjectServiceImpl implements PCProjectService {

    @Autowired
    private PCProjectDao pcProjectDao;

    @Override
    public Page<PcProject> list(Integer page, Integer limit, String corpid, String userid, String projectProcess) {
        Map<String, Object> param = new HashMap<>();
        param.put("userId", userid);
        param.put("corpid", corpid);
        param.put("projectProcess", projectProcess);
        return pcProjectDao.list(page, limit, param);
    }

    @Override
    public PcProject get(Integer id, String corpid, String userid) {
        return pcProjectDao.get(id, corpid);

    }

    @Override
    public void save(ProjectVo vo, String corpid, String userId) {
        pcProjectDao.save(
                corpid,
                vo.getProjectName(),
                vo.getProjectProcess(),
                vo.getProjectManager(),
                vo.getSignDay(),
                new BigDecimal(vo.getProjectAmount()),
                new BigDecimal(vo.getSalesExpense()),
                TimeUtil.parse(vo.getPlanDevStartTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getCustomerReqEndTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getRealStartTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getRealEndTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getMaintainStartTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                vo.getFreeMaintainDay(),
                new BigDecimal(vo.getMaintainFee()),
                userId
        );
    }

    @Override
    public void update(ProjectVo vo, String corpid, String userId) {
        pcProjectDao.update(
                vo.getId(),
                corpid,
                vo.getProjectName(),
                vo.getProjectProcess(),
                vo.getProjectManager(),
                vo.getSignDay(),
                new BigDecimal(vo.getProjectAmount()),
                new BigDecimal(vo.getSalesExpense()),
                TimeUtil.parse(vo.getPlanDevStartTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getCustomerReqEndTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getRealStartTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getRealEndTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                TimeUtil.parse(vo.getMaintainStartTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS),
                vo.getFreeMaintainDay(),
                new BigDecimal(vo.getMaintainFee()),
                userId
        );
    }

    @Override
    public void delete(Integer id, String corpid, String userId) {
        pcProjectDao.delete(id, corpid);
    }
}
