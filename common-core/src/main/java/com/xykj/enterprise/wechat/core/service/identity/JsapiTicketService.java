package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.core.model.dodb.JsapiTicket;

/**
 * @Author george
 * @create 2021-04-19 21:02
 */
public interface JsapiTicketService {

    JsapiTicket getAgentTicket(String corpid, String agentId);

    JsapiTicket getCorpTicket(String corpid, String agentId);

    void saveAgentTicket(
            String corpid,
            String agentId,
            String jsapTicket,
            Integer expire
    );

    void saveCorpTicket(
            String corpid,
            String agentId,
            String jsapTicket,
            Integer expire
    );

    String getAgentTicket(
            String corpid,
            String agentId,
            String suiteId,
            String secret
    );

    String getCorpTicket(
            String corpid,
            String agentId,
            String suiteId,
            String secret
    );
}
