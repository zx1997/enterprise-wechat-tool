package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowLog;
import com.ydn.dbframe.plugin.activerecord.Page;

/**
 * @Author zhouxu
 * @create 2021-04-12 15:52
 */
public interface CustomerFollowLogDao {
    Page<CustomerFollowLog> list(Integer page, Integer limit, String corpid, String userId, String customerId);

    Long save(
            String corpid,
            String cutomerId,
            String userId,
            String followContent,
            String followStatus,
            String scheduleId
    );

    void update(
            Long logId,
            String corpid,
            String cutomerId,
            String userId,
            String followContent,
            String followStatus,
            String scheduleId
    );

    void delete(Long logId);

}
