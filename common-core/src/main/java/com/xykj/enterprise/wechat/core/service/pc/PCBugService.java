package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectBugVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcBug;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.Map;

/**
 * @Author george
 * @create 2021-05-27 15:23
 */
public interface PCBugService {

    Page<PcBug> list(Integer page, Integer limit, Map<String, Object> param);

    void save(ProjectBugVo vo, String corpid, String userid);

    void update(ProjectBugVo vo, String corpid, String userid);

    void delete(Long id, String corpid, String userId);

}
