package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.MomentLink;

/**
 * @Author george
 * @create 2021-05-10 11:26
 */
public interface MomentLinkDao {

    MomentLink get(String corpid,String momentId);

    void save(
            String title,
            String url,
            String corpid,
            String momentId
    );

    void delete(String momentId, String corpid);
}
