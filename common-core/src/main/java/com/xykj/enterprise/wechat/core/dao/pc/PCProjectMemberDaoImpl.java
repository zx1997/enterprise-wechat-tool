package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcProjectMember;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-13 20:02
 */
@Repository
public class PCProjectMemberDaoImpl implements PCProjectMemberDao {

    @Override
    public Page<PcProjectMember> list(Integer page, Integer limit, Map<String, Object> params) {
        SqlParse sqlParse = new SqlParse(params);
        sqlParse.addSQL(" select * from pc_project_member ");
        sqlParse.addSQL(" where 1=1 ");
        sqlParse.addSQL(" and corpid = :corpid ");
        sqlParse.addSQL(" and project_id = :projectId ");
        sqlParse.addSQL(" order by create_time desc ");

        return PcProjectMember.dao.paginate(page, limit, sqlParse);
    }

    @Override
    public PcProjectMember get(Long id, String corpid) {
        String sql = " select * from pc_project_member where id = ? and corpid = ? ";
        return PcProjectMember.dao.findFirst(sql, id, corpid);
    }

    @Override
    public Long save(
            String corpid,
            Integer projectId,
            String memberId,
            String memberContent,
            Date planStartTime,
            Date realStartTime,
            Date planEndTime,
            Date realEndTime,
            String creator
    ) {
        PcProjectMember m = new PcProjectMember();
        m.setCorpid(corpid);
        m.setProjectId(projectId);
        m.setMemberId(memberId);
        m.setMemberContent(memberContent);
        m.setPlanStartTime(planStartTime);
        m.setRealStartTime(realStartTime);
        m.setPlanEndTime(planEndTime);
        m.setRealEndTime(realEndTime);
        m.setCreator(creator);
        m.save();
        return m.getId();
    }

    @Override
    public void update(
            Long id,
            String corpid,
            Integer projectId,
            String memberId,
            String memberContent,
            Date planStartTime,
            Date realStartTime,
            Date planEndTime,
            Date realEndTime,
            String updator
    ) {
        PcProjectMember m = PcProjectMember.dao.findById(id);
        m.setCorpid(corpid);
        m.setProjectId(projectId);
        m.setMemberId(memberId);
        m.setMemberContent(memberContent);
        m.setPlanStartTime(planStartTime);
        m.setRealStartTime(realStartTime);
        m.setPlanEndTime(planEndTime);
        m.setRealEndTime(realEndTime);
        m.setUpdateor(updator);
        m.setUpdateTime(new Date());
        m.update();

    }

    @Override
    public void delete(Long id, String corpid) {
        PcProjectMember m = PcProjectMember.dao.findById(id);
        if (m != null) {
            m.delete();
        }
    }
}
