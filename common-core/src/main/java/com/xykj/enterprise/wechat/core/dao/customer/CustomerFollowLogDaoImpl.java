package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowLog;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-04-12 15:52
 */
@Repository
public class CustomerFollowLogDaoImpl implements CustomerFollowLogDao {

    @Override
    public Page<CustomerFollowLog> list(Integer page, Integer limit, String corpid, String userId, String customerId) {
        Map<String, Object> param = new HashMap<>();
        param.put("corpid", corpid);
        param.put("userId", userId);
        param.put("customerId", customerId);
        SqlParse sqlParse = new SqlParse(param);
        sqlParse.addSQL(" select * from t_customer_follow_log ");
        sqlParse.addSQL(" where  1 = 1");
        sqlParse.addSQL(" and corpid = :corpid");
        sqlParse.addSQL(" and customer_id = :customerId");
        sqlParse.addSQL(" and user_id = :userId ");
        sqlParse.addSQL(" order by create_time desc ");
        return CustomerFollowLog.dao.paginate(page, limit, sqlParse);
    }

    @Override
    public Long save(
            String corpid,
            String cutomerId,
            String userId,
            String followContent,
            String followStatus,
            String scheduleId
    ) {
        CustomerFollowLog log = new CustomerFollowLog();
        log.setCorpid(corpid);
        log.setCustomerId(cutomerId);
        log.setUserId(userId);
        log.setFollowContent(followContent);
        log.setFollowStatus(followStatus);
        log.setScheduleId(scheduleId);
        log.setCreateTime(new Date());
        log.save();
        return log.getLogId();
    }

    @Override
    public void update(
            Long logId,
            String corpid,
            String cutomerId,
            String userId,
            String followContent,
            String followStatus,
            String scheduleId
    ) {
        CustomerFollowLog log = CustomerFollowLog.dao.findById(logId);
        log.setCorpid(corpid);
        log.setCustomerId(cutomerId);
        log.setUserId(userId);
        log.setFollowContent(followContent);
        log.setFollowStatus(followStatus);
        log.setScheduleId(scheduleId);
        log.setUpdateTime(new Date());
        log.update();

    }

    @Override
    public void delete(Long logId) {
        CustomerFollowLog log = CustomerFollowLog.dao.findById(logId);
        if (log != null) {
            log.delete();
        }
    }

}
