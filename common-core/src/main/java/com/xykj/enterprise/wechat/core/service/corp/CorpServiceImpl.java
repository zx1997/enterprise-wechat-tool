package com.xykj.enterprise.wechat.core.service.corp;

import com.alibaba.fastjson.JSONArray;
import com.xykj.enterprise.wechat.bean.ext.identity.Auth_corp_info;
import com.xykj.enterprise.wechat.bean.ext.identity.GetPermanentCodeVo;
import com.xykj.enterprise.wechat.core.dao.CorpExtDao;
import com.xykj.enterprise.wechat.core.dao.CorpSuiteDao;
import com.xykj.enterprise.wechat.core.dao.corp.AuthCorpDao;
import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;
import com.xykj.enterprise.wechat.core.model.dodb.CorpSuite;
import com.xykj.enterprise.wechat.util.biz.BizUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author zhouxu
 * @create 2021-03-31 14:54
 */
@Slf4j
@Service
public class CorpServiceImpl implements CorpService {

    @Autowired
    private AuthCorpDao authCorpDao;
    @Autowired
    private CorpExtDao corpExtDao;
    @Autowired
    private CorpSuiteDao corpSuiteDao;

    @Override
    public void save(String suiteId, GetPermanentCodeVo getPermanentCodeVo) {

        Auth_corp_info auth_corp_info = getPermanentCodeVo.getAuth_corp_info();
        if (auth_corp_info == null) {
            BizUtil.exception("永久授权数据异常，auth_corp_info 为空");
        }
        // 认证企业信息同步
        AuthCorp authCorp = authCorpDao.getByCorpid(auth_corp_info.getCorpid());
        if (authCorp == null) {
            authCorpDao.save(
                    auth_corp_info.getCorpid(),
                    JSONArray.toJSON(auth_corp_info).toString(),
                    getPermanentCodeVo.getAuth_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getAuth_info()).toString(),
                    getPermanentCodeVo.getAuth_user_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getAuth_user_info()).toString(),
                    getPermanentCodeVo.getDealer_corp_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getDealer_corp_info()).toString(),
                    getPermanentCodeVo.getRegister_code_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getAuth_user_info()).toString()

            );

        } else {
            authCorpDao.update(
                    auth_corp_info.getCorpid(),
                    JSONArray.toJSON(auth_corp_info).toString(),
                    getPermanentCodeVo.getAuth_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getAuth_info()).toString(),
                    getPermanentCodeVo.getAuth_user_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getAuth_user_info()).toString(),
                    getPermanentCodeVo.getDealer_corp_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getDealer_corp_info()).toString(),
                    getPermanentCodeVo.getRegister_code_info() == null ? null : JSONArray.toJSON(getPermanentCodeVo.getAuth_user_info()).toString()

            );

        }

        // 配置参数同步
        CorpSuite param = corpSuiteDao.get(auth_corp_info.getCorpid(), suiteId);
        if (param == null) {
            corpSuiteDao.save(
                    auth_corp_info.getCorpid(),
                    suiteId,
                    getPermanentCodeVo.getPermanent_code(),
                    getPermanentCodeVo.getAccess_token(),
                    getPermanentCodeVo.getExpires_in()
            );
        } else {
            corpSuiteDao.update(
                    auth_corp_info.getCorpid(),
                    suiteId,
                    getPermanentCodeVo.getPermanent_code(),
                    getPermanentCodeVo.getAccess_token(),
                    getPermanentCodeVo.getExpires_in()
            );
        }

    }

    @Override
    public CorpExt getCorpExt(String corpid) {
        return corpExtDao.get(corpid);
    }

    @Override
    public void initExtInfo(String corpid) {
        CorpExt ext = corpExtDao.get(corpid);
        if (ext == null) {
            corpExtDao.save(corpid);
        }

    }
}
