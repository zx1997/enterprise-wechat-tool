package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseAgentAdmin<M extends BaseAgentAdmin<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * suiteId
	 */
	public void setSuiteId(java.lang.String suiteId) {
		set("suite_id", suiteId);
	}
	
	/**
	 * suiteId
	 */
	public java.lang.String getSuiteId() {
		return getStr("suite_id");
	}

	/**
	 * 应用ID
	 */
	public void setAgentid(java.lang.String agentid) {
		set("agentid", agentid);
	}
	
	/**
	 * 应用ID
	 */
	public java.lang.String getAgentid() {
		return getStr("agentid");
	}

	/**
	 * 管理员的userid
	 */
	public void setUserid(java.lang.String userid) {
		set("userid", userid);
	}
	
	/**
	 * 管理员的userid
	 */
	public java.lang.String getUserid() {
		return getStr("userid");
	}

	/**
	 * 管理员的open_userid
	 */
	public void setOpenUserid(java.lang.String openUserid) {
		set("open_userid", openUserid);
	}
	
	/**
	 * 管理员的open_userid
	 */
	public java.lang.String getOpenUserid() {
		return getStr("open_userid");
	}

	/**
	 * 该管理员对应用的权限：0=发消息权限，1=管理权限
	 */
	public void setAuthType(java.lang.String authType) {
		set("auth_type", authType);
	}
	
	/**
	 * 该管理员对应用的权限：0=发消息权限，1=管理权限
	 */
	public java.lang.String getAuthType() {
		return getStr("auth_type");
	}

}
