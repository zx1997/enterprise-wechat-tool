package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMomentImage<M extends BaseMomentImage<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 图片ID
	 */
	public void setMediaId(java.lang.String mediaId) {
		set("media_id", mediaId);
	}
	
	/**
	 * 图片ID
	 */
	public java.lang.String getMediaId() {
		return getStr("media_id");
	}

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	public void setMomentId(java.lang.String momentId) {
		set("moment_id", momentId);
	}
	
	public java.lang.String getMomentId() {
		return getStr("moment_id");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
