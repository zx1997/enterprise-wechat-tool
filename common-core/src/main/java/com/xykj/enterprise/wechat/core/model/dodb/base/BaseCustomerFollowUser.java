package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomerFollowUser<M extends BaseCustomerFollowUser<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	public void setExternalUserid(java.lang.String externalUserid) {
		set("external_userid", externalUserid);
	}
	
	public java.lang.String getExternalUserid() {
		return getStr("external_userid");
	}

	/**
	 * 客户ID
	 */
	public void setCustomerId(java.lang.Long customerId) {
		set("customer_id", customerId);
	}
	
	/**
	 * 客户ID
	 */
	public java.lang.Long getCustomerId() {
		return getLong("customer_id");
	}

	/**
	 * 员工id
	 */
	public void setUserid(java.lang.String userid) {
		set("userid", userid);
	}
	
	/**
	 * 员工id
	 */
	public java.lang.String getUserid() {
		return getStr("userid");
	}

	/**
	 * 客户备注
	 */
	public void setRemark(java.lang.String remark) {
		set("remark", remark);
	}
	
	/**
	 * 客户备注
	 */
	public java.lang.String getRemark() {
		return getStr("remark");
	}

	/**
	 * 客户描述信息
	 */
	public void setDescription(java.lang.String description) {
		set("description", description);
	}
	
	/**
	 * 客户描述信息
	 */
	public java.lang.String getDescription() {
		return getStr("description");
	}

	/**
	 * 创建时间
	 */
	public void setCreatetime(java.lang.Long createtime) {
		set("createtime", createtime);
	}
	
	/**
	 * 创建时间
	 */
	public java.lang.Long getCreatetime() {
		return getLong("createtime");
	}

	/**
	 * 备注企业名称
	 */
	public void setRemarkCorpName(java.lang.String remarkCorpName) {
		set("remark_corp_name", remarkCorpName);
	}
	
	/**
	 * 备注企业名称
	 */
	public java.lang.String getRemarkCorpName() {
		return getStr("remark_corp_name");
	}

	/**
	 * 备注手机号
	 */
	public void setRemarkMobiles(java.lang.String remarkMobiles) {
		set("remark_mobiles", remarkMobiles);
	}
	
	/**
	 * 备注手机号
	 */
	public java.lang.String getRemarkMobiles() {
		return getStr("remark_mobiles");
	}

	/**
	 * 发起添加的userid
	 */
	public void setOperUserid(java.lang.String operUserid) {
		set("oper_userid", operUserid);
	}
	
	/**
	 * 发起添加的userid
	 */
	public java.lang.String getOperUserid() {
		return getStr("oper_userid");
	}

	/**
	 * 添加客户的来源 0-未知来源 1-扫描二维码 2-搜索手机号 3-名片分享 4-群聊 5-手机通讯录 6-微信联系人 7-来自微信的添加好友申请 8-安装第三方应用时自动添加的客服人员 9-搜索邮箱 201-内部成员共享 202
	 */
	public void setAddWay(java.lang.String addWay) {
		set("add_way", addWay);
	}
	
	/**
	 * 添加客户的来源 0-未知来源 1-扫描二维码 2-搜索手机号 3-名片分享 4-群聊 5-手机通讯录 6-微信联系人 7-来自微信的添加好友申请 8-安装第三方应用时自动添加的客服人员 9-搜索邮箱 201-内部成员共享 202
	 */
	public java.lang.String getAddWay() {
		return getStr("add_way");
	}

	/**
	 * 企业自定义的state参数，用于区分客户具体是通过哪个「联系我」添加，由企业通过创建「联系我」方式指定
	 */
	public void setState(java.lang.String state) {
		set("state", state);
	}
	
	/**
	 * 企业自定义的state参数，用于区分客户具体是通过哪个「联系我」添加，由企业通过创建「联系我」方式指定
	 */
	public java.lang.String getState() {
		return getStr("state");
	}

	/**
	 * 该成员添加此外部联系人所打标签
	 */
	public void setTags(java.lang.String tags) {
		set("tags", tags);
	}
	
	/**
	 * 该成员添加此外部联系人所打标签
	 */
	public java.lang.String getTags() {
		return getStr("tags");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
