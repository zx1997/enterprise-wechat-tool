package com.xykj.enterprise.wechat.core.model.dodb;

import com.ydn.dbframe.plugin.activerecord.ActiveRecordPlugin;

/**
 *  do not modify this file.
 * <pre>
 * Example:
 * public void configPlugin(Plugins me) {
 *     ActiveRecordPlugin arp = new ActiveRecordPlugin(...);
 *     _MappingKit.mapping(arp);
 *     me.add(arp);
 * }
 * </pre>
 */
public class _MappingKit {
	
	public static void mapping(ActiveRecordPlugin arp) {
		arp.addMapping("pc_bug", "id", PcBug.class);
		arp.addMapping("pc_project", "id", PcProject.class);
		arp.addMapping("pc_project_log", "id", PcProjectLog.class);
		arp.addMapping("pc_project_member", "id", PcProjectMember.class);
		arp.addMapping("sys_param", "param_code", SysParam.class);
		arp.addMapping("t_agent_admin", "id", AgentAdmin.class);
		arp.addMapping("t_agent_info", "id", AgentInfo.class);
		arp.addMapping("t_auth_corp", "corpid", AuthCorp.class);
		arp.addMapping("t_corp_ext", "corpid", CorpExt.class);
		// Composite Primary Key order: corpid,suite_id
		arp.addMapping("t_corp_suite", "corpid,suite_id", CorpSuite.class);
		arp.addMapping("t_corp_tag", "id", CorpTag.class);
		arp.addMapping("t_corp_tag_group", "id", CorpTagGroup.class);
		arp.addMapping("t_customer", "id", Customer.class);
		arp.addMapping("t_customer_follow_log", "log_id", CustomerFollowLog.class);
		arp.addMapping("t_customer_follow_user", "id", CustomerFollowUser.class);
		arp.addMapping("t_customer_tag", "tag_id", CustomerTag.class);
		arp.addMapping("t_department", "id", Department.class);
		arp.addMapping("t_group_chat", "id", GroupChat.class);
		arp.addMapping("t_group_chat_member", "id", GroupChatMember.class);
		arp.addMapping("t_jsapi_ticket", "id", JsapiTicket.class);
		arp.addMapping("t_moment", "id", Moment.class);
		arp.addMapping("t_moment_image", "id", MomentImage.class);
		arp.addMapping("t_moment_link", "id", MomentLink.class);
		arp.addMapping("t_moment_location", "id", MomentLocation.class);
		arp.addMapping("t_moment_text", "id", MomentText.class);
		arp.addMapping("t_moment_video", "id", MomentVideo.class);
		arp.addMapping("t_pay_log", "id", PayLog.class);
		arp.addMapping("t_suite_info", "suite_id", SuiteInfo.class);
		arp.addMapping("t_suite_ticket", "ticket_id", SuiteTicket.class);
		arp.addMapping("t_user", "id", User.class);
		arp.addMapping("t_wxpush_log", "log_id", WxpushLog.class);
	}
}

