package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.SuiteInfo;
import org.springframework.stereotype.Repository;

/**
 * @Author george
 * @create 2021-05-28 15:25
 */
@Repository
public class SuiteInfoDaoImpl implements SuiteInfoDao{

    @Override
    public SuiteInfo get(String suiteId) {
        return SuiteInfo.dao.findById(suiteId);
    }
}
