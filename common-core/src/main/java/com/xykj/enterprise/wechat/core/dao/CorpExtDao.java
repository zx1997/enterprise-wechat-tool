package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;

/**
 * @Author zhouxu
 * @create 2021-04-07 16:57
 */
public interface CorpExtDao {


    CorpExt get(String corpid);

    void save(String corpid);

    void syncDepartmentData(String corpid,String sign);

    void syncUserData(String corpid,String sign);

    void syncCustomerData(String corpid,String sign);

    void syncCorpTagData(String corpid,String sign);

    void syncGroupChatData(String corpid,String sign);
}
