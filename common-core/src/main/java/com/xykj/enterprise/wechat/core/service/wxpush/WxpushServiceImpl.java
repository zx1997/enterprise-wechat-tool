package com.xykj.enterprise.wechat.core.service.wxpush;

import com.xykj.enterprise.wechat.core.dao.WxpushLogDao;
import com.xykj.enterprise.wechat.core.eao.WxpushLogEao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @Author zhouxu
 * @create 2021-02-25 11:27
 */
@Service
public class WxpushServiceImpl implements WxpushService {

    @Autowired
    private WxpushLogDao wxpushLogDao;
    @Autowired
    private WxpushLogEao wxpushLogEao;

    @Override
    public void saveLog(String msgSignatuer, String timestamp, String nonce, String data, String msg, String jsonMsg, String msgType, String event, String changeType) {
        wxpushLogDao.save(msgSignatuer, timestamp, nonce, data, msg, jsonMsg, msgType, event, changeType);
        wxpushLogEao.save(msgSignatuer, timestamp, nonce, data, msg, jsonMsg, msgType, event, changeType);

    }


}
