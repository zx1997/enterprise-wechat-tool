package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CorpTag;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class CorpTagDaoImpl implements CorpTagDao {

    @Override
    public Page<CorpTag> findByGroup(String corpId, String groupId, Integer pageNumber, Integer pageSize) {
        Map<String, Object> params = new HashMap<>();
        params.put("corpid", corpId);
        params.put("groupId", groupId);
        SqlParse sqlParse = new SqlParse(params);
        sqlParse.addSQL("select * from t_corp_tag ");
        sqlParse.addSQL(" where 1=1");
        sqlParse.addSQL(" corp_id = :corpid ");
        sqlParse.addSQL(" group_id = :groupId ");
        return CorpTag.dao.paginate(pageNumber, pageSize, sqlParse);
    }

    @Override
    public List<CorpTag> listByGroup(String corpId, String groupId) {
        String sql = " select * from t_corp_tag where corpid = ? and group_id = ? ";
        return CorpTag.dao.find(sql, corpId, groupId);
    }


    @Override
    public CorpTag get(String corpid, String tagId, String groupId) {
        String sql = " select * from t_corp_tag where corpid = ? and tag_id = ? and group_id = ?";
        return CorpTag.dao.findFirst(sql, corpid, tagId, groupId);
    }

    @Override
    public CorpTag save(
            String corpid,
            String tagId,
            String groupId,
            String name,
            Integer order,
            String deleted
    ) {
        CorpTag tag = new CorpTag();
        tag.setCorpid(corpid);
        tag.setTagId(tagId);
        tag.setGroupId(groupId);
        tag.setName(name);
        tag.setOrder(order);
        tag.setCreateTime(new Date());
        tag.save();
        return tag;
    }

    @Override
    public void update(
            Long id,
            String corpid,
            String tagId,
            String groupId,
            String name,
            Integer order,
            String deleted
    ) {
        CorpTag tag = CorpTag.dao.findById(id);
        tag.setCorpid(corpid);
        tag.setTagId(tagId);
        tag.setGroupId(groupId);
        tag.setName(name);
        tag.setOrder(order);
        tag.setUpdateTime(new Date());
        tag.update();
    }
}
