package com.xykj.enterprise.wechat.core.service.customer;

import com.xykj.enterprise.wechat.bean.busi.vo.CorpTagGroupVo;
import com.xykj.enterprise.wechat.bean.busi.vo.CorpTagVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.ExternalContactFollowUser;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.ExternalContactVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.External_contact;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.External_contact_list;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.Follow_info;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetExternalContactBatchVo;
import com.xykj.enterprise.wechat.core.convert.Converter;
import com.xykj.enterprise.wechat.core.dao.CorpExtDao;
import com.xykj.enterprise.wechat.core.dao.customer.CorpTagDao;
import com.xykj.enterprise.wechat.core.dao.customer.CorpTagGroupDao;
import com.xykj.enterprise.wechat.core.dao.customer.CustomerDao;
import com.xykj.enterprise.wechat.core.dao.customer.CustomerFollowLogDao;
import com.xykj.enterprise.wechat.core.dao.customer.CustomerFollowUserDao;
import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;
import com.xykj.enterprise.wechat.core.model.dodb.CorpTag;
import com.xykj.enterprise.wechat.core.model.dodb.CorpTagGroup;
import com.xykj.enterprise.wechat.core.model.dodb.Customer;
import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowLog;
import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowUser;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import com.ydn.dbframe.plugin.activerecord.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private CorpTagDao corpTagDao;
    @Autowired
    private CorpTagGroupDao corpTagGroupDao;
    @Autowired
    private CustomerFollowLogDao customerFollowLogDao;
    @Autowired
    private CustomerFollowUserDao customerFollowUserDao;
    @Autowired
    private CorpExtDao corpExtDao;

    @Override
    public Customer findByExternalUserid(String externalUserid, String corpid) {
        return customerDao.findByExternalUserid(externalUserid, corpid);
    }

    @Override
    public List<CorpTagGroupVo> findAllCorpTagGroup(String corpid) {
        List<CorpTagGroup> groupList = corpTagGroupDao.findAll(corpid);
        log.debug("groupList.size:{}", groupList.size());
        List<CorpTagGroupVo> groupVoList = new ArrayList<>();
        if (groupList != null) {
            for (CorpTagGroup group : groupList) {
                CorpTagGroupVo groupVo = new CorpTagGroupVo();
                // group 相关信息
                groupVo.setGroupId(group.getGroupId());
                groupVo.setGroupName(group.getName());

                // tagVoList 标签列表信息
                List<CorpTag> tags = corpTagDao.listByGroup(corpid, group.getGroupId());
                log.debug("tags.size:{}", tags.size());
                List<CorpTagVo> voList = ((Converter<List<CorpTag>, List<CorpTagVo>>) voTags -> {
                    List<CorpTagVo> vos = new ArrayList<>();
                    voTags.forEach((v) -> {
                        CorpTagVo vo = new CorpTagVo();
                        vo.setId(v.getId());
                        vo.setCorpId(v.getCorpid());
                        vo.setGroupId(v.getGroupId());
                        vo.setTagId(v.getTagId());
                        vo.setTagName(v.getName());
                        if (v.getCreateTime() != null) {
                            vo.setCreateTime(TimeUtil.format(v.getCreateTime(), TimeUtil.FORMAT_YYYYMMDDHHMMSS));
                        }
                        vo.setOrder(v.getOrder());
                        vo.setIsDeleted(v.getDeleted());
                        vos.add(vo);
                    });
                    return vos;
                }).convert(tags);
                groupVo.setTagList(voList);

                groupVoList.add(groupVo);
            }
        }
        return groupVoList;
    }

    @Override
    public Page<CustomerFollowLog> getFollowLog(Integer page, Integer limit, String corpid, String userId, String customerId) {
        return customerFollowLogDao.list(page, limit, corpid, userId, customerId);
    }

    @Override
    public void saveFollowLog(Long logId, String corpid, String userId, String customerId, String followContent, String followStatus, String scheduleId) {
        if (logId == null) {
            customerFollowLogDao.save(corpid, customerId, userId, followContent, followStatus, scheduleId);
        } else {
            customerFollowLogDao.update(logId, corpid, customerId, userId, followContent, followStatus, scheduleId);
        }
    }

    @Override
    public void deleteFollowId(Long logId) {
        customerFollowLogDao.delete(logId);
    }

    @Override
    public CustomerFollowUser getUserFollowInfo(String externalUserid, String corpid, String userid) {
        return customerFollowUserDao.get(externalUserid, corpid, userid);
    }


    @Override
    public void syncCustomerList(GetExternalContactBatchVo batchVo, String corpid, String userid) {
        if (batchVo != null && !batchVo.getExternal_contact_list().isEmpty()) {
            for (External_contact_list vo : batchVo.getExternal_contact_list()) {
                External_contact contact = vo.getExternal_contact();
                // customer 同步
                Customer c = customerDao.get(corpid, contact.getExternal_userid());
                if (c == null) {
                    c = customerDao.save(
                            corpid,
                            contact.getExternal_userid(),
                            contact.getName(),
                            contact.getPosition(),
                            contact.getAvatar(),
                            String.valueOf(contact.getType()),
                            contact.getAvatar(),
                            contact.getUnionid(),
                            contact.getCorp_name(),
                            contact.getCorp_full_name(),
                            null,
                            null,
                            null

                    );
                } else {
                    c = customerDao.update(
                            c.getId(),
                            corpid,
                            contact.getExternal_userid(),
                            contact.getName(),
                            contact.getPosition(),
                            contact.getAvatar(),
                            String.valueOf(contact.getType()),
                            contact.getAvatar(),
                            contact.getUnionid(),
                            contact.getCorp_name(),
                            contact.getCorp_full_name(),
                            null,
                            null,
                            null

                    );
                }

                Follow_info info = vo.getFollow_info();
                // follow_user 同步
                CustomerFollowUser followUser = customerFollowUserDao.get(c.getExternalUserid(), c.getCorpid());
                if (followUser == null) {
                    customerFollowUserDao.save(
                            c.getCorpid(),
                            c.getExternalUserid(),
                            c.getId(),
                            userid,
                            info.getRemark(),
                            info.getDescription(),
                            info.getCreatetime(),
                            info.getRemark_corp_name(),
                            info.getRemark_mobiles().toString(),
                            info.getOper_userid(),
                            info.getAdd_way(),
                            null,
                            info.getTag_id().toString()
                    );
                } else {
                    customerFollowUserDao.update(
                            followUser.getId(),
                            c.getCorpid(),
                            c.getExternalUserid(),
                            c.getId(),
                            userid,
                            info.getRemark(),
                            info.getDescription(),
                            info.getCreatetime(),
                            info.getRemark_corp_name(),
                            info.getRemark_mobiles().toString(),
                            info.getOper_userid(),
                            info.getAdd_way(),
                            null,
                            info.getTag_id().toString()
                    );
                }
            }
        }

        // 同步完成标记
        corpExtDao.syncCustomerData(corpid, CorpExt.SUCC);
    }

    @Override
    public void save(String corpid, ExternalContactVo contact) {
        // 1.新增/修改 客户记录
        Customer c = customerDao.get(corpid, contact.getExternal_userid());
        if (c == null) {
            c = customerDao.save(
                    corpid,
                    contact.getExternal_userid(),
                    contact.getName(),
                    contact.getPosition(),
                    contact.getAvatar(),
                    String.valueOf(contact.getType()),
                    contact.getAvatar(),
                    contact.getUnionid(),
                    contact.getCorp_name(),
                    contact.getCorp_full_name(),
                    null,
                    null,
                    null

            );
        } else {
            c = customerDao.update(
                    c.getId(),
                    corpid,
                    contact.getExternal_userid(),
                    contact.getName(),
                    contact.getPosition(),
                    contact.getAvatar(),
                    String.valueOf(contact.getType()),
                    contact.getAvatar(),
                    contact.getUnionid(),
                    contact.getCorp_name(),
                    contact.getCorp_full_name(),
                    null,
                    null,
                    null

            );
        }

        // 2.先新增，再新增员工关系记录
        customerFollowUserDao.batchDelete(contact.getExternal_userid(), corpid);
        List<ExternalContactFollowUser> followUserList = contact.getFollow_user();
        if (followUserList != null) {
            c = customerDao.save(
                    corpid,
                    contact.getExternal_userid(),
                    contact.getName(),
                    contact.getPosition(),
                    contact.getAvatar(),
                    String.valueOf(contact.getType()),
                    contact.getAvatar(),
                    contact.getUnionid(),
                    contact.getCorp_name(),
                    contact.getCorp_full_name(),
                    null,
                    null,
                    null

            );
        }
    }

    @Override
    public void delete(String corpid, String externalUserid) {
        // 1.删除客户
        customerDao.delete(corpid, externalUserid);
        // 2.删除followUser
        customerFollowUserDao.batchDelete(externalUserid, corpid);
    }

    @Override
    public void deleteFollowUser(String corpid, String externalUserid, String userid) {
        CustomerFollowUser followUser = customerFollowUserDao.get(externalUserid,corpid,userid);
        customerFollowUserDao.delete(followUser);

    }


}
