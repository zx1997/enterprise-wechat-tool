package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetCorpTagListVo;

/**
 * @Author zhouxu
 * @create 2021-04-12 10:46
 */
public interface CorpTagService {

    /**
     * 同步企业标签数据
     *
     * @param listVo
     */
    void syncCorpTag(GetCorpTagListVo listVo, String corpid);
}
