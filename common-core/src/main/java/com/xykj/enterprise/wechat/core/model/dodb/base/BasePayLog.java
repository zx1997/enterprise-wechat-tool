package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BasePayLog<M extends BasePayLog<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpId(java.lang.Long corpId) {
		set("corp_id", corpId);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.Long getCorpId() {
		return getLong("corp_id");
	}

	/**
	 * 客户ID
	 */
	public void setCustomerId(java.lang.Long customerId) {
		set("customer_id", customerId);
	}
	
	/**
	 * 客户ID
	 */
	public java.lang.Long getCustomerId() {
		return getLong("customer_id");
	}

	/**
	 * 订单ID
	 */
	public void setOrderId(java.lang.Long orderId) {
		set("order_id", orderId);
	}
	
	/**
	 * 订单ID
	 */
	public java.lang.Long getOrderId() {
		return getLong("order_id");
	}

	/**
	 * 跟进详情、内容
	 */
	public void setContent(java.lang.String content) {
		set("content", content);
	}
	
	/**
	 * 跟进详情、内容
	 */
	public java.lang.String getContent() {
		return getStr("content");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 备注信息
	 */
	public void setMark(java.lang.String mark) {
		set("mark", mark);
	}
	
	/**
	 * 备注信息
	 */
	public java.lang.String getMark() {
		return getStr("mark");
	}

}
