package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.SuiteTicket;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:09
 */
public interface SuiteTicketDao {

    Long save(String suiteId, String infoType, Long timeStamp, String suiteTicket);

    SuiteTicket getLatest(String suiteId);

}
