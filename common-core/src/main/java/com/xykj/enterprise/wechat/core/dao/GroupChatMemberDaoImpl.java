package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.GroupChatMember;
import com.ydn.dbframe.plugin.activerecord.Db;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-04-22 20:44
 */
@Repository
public class GroupChatMemberDaoImpl implements GroupChatMemberDao {
    @Override
    public GroupChatMember get(String corpid, String chatId, String userid) {
        String sql = " select * from t_group_chat_member where corpid = ? and chat_id = ? and userid = ? ";
        return GroupChatMember.dao.findFirst(sql, corpid, chatId, userid);
    }

    @Override
    public Long save(
            String chatId,
            String corpid,
            String userid,
            String type,
            String unionid,
            String join_scene,
            String join_time,
            String invitor
    ) {
        GroupChatMember m = new GroupChatMember();
        m.setChatId(chatId);
        m.setCorpid(corpid);
        m.setUserid(userid);
        m.setType(type);
        m.setUnionid(unionid);
        m.setJoinScene(join_scene);
        m.setJoinTime(join_time);
        m.setInvitor(invitor);
        m.setCreateTime(new Date());
        m.save();
        return m.getId();
    }

    @Override
    public void update(
            Long id,
            String chatId,
            String corpid,
            String userid,
            String type,
            String unionid,
            String join_scene,
            String join_time,
            String invitor
    ) {
        GroupChatMember m = GroupChatMember.dao.findById(id);
        m.setChatId(chatId);
        m.setCorpid(corpid);
        m.setUserid(userid);
        m.setType(type);
        m.setUnionid(unionid);
        m.setJoinScene(join_scene);
        m.setJoinTime(join_time);
        m.setInvitor(invitor);
        m.setCreateTime(new Date());
        m.update();

    }

    @Override
    public Page<GroupChatMember> list(Integer page, Integer limit, String corpid) {
        Map param = new HashMap();
        param.put("corpid", corpid);
        SqlParse sqlParse = new SqlParse(param);
        sqlParse.addSQL(" select * from t_group_chat_member ");
        sqlParse.addSQL(" where 1=1 ");
        sqlParse.addSQL(" corpid = :corpid ");

        return GroupChatMember.dao.paginate(page, limit, sqlParse);
    }

    @Override
    public void delete(String chatId, String corpid) {
        String sql = " delete from t_group_chat_member where chat_id = ? and corpid = ? ";
        Db.update(sql, chatId, corpid);
    }


}
