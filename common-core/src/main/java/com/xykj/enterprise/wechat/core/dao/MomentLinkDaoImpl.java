package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.config.DbConfig;
import com.xykj.enterprise.wechat.core.model.dodb.MomentLink;
import com.ydn.dbframe.plugin.activerecord.Db;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author george
 * @create 2021-05-10 11:42
 */
@Repository
public class MomentLinkDaoImpl implements MomentLinkDao {
    @Override
    public MomentLink get(String corpid, String momentId) {
        String sql = " select * from t_moment_link where corpid = ? and moment_id = ? ";
        return MomentLink.dao.findFirst(sql, corpid, momentId);
    }

    @Override
    public void save(String title, String url, String corpid, String momentId) {
        MomentLink link = new MomentLink();
        link.setTitle(title);
        link.setUrl(url);
        link.setCorpid(corpid);
        link.setMomentId(momentId);
        link.setCreateTime(new Date());
        link.save();
    }

    @Override
    public void delete(String momentId, String corpid) {
        String sql = " delete from t_moment_link where corpid = ? and moment_id = ? limit 1 ";
        Db.use(DbConfig.DATASOURCE).update(sql, momentId, corpid);
    }
}
