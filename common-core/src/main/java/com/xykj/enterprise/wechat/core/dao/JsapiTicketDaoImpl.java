package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.JsapiTicket;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author george
 * @create 2021-04-19 20:58
 */
@Repository
public class JsapiTicketDaoImpl implements JsapiTicketDao {
    @Override
    public JsapiTicket get(String corpid, String agentId) {
        String sql = "select * from t_jsapi_ticket where corpid = ? and agent_id = ? ";
        return JsapiTicket.dao.findFirst(sql, corpid, agentId);
    }

    @Override
    public JsapiTicket saveAgentTicket(String corpid, String agentId, String jsapTicket, Integer expire) {
        JsapiTicket ticket = new JsapiTicket();
        ticket.setCorpid(corpid);
        ticket.setAgentId(agentId);
        ticket.setAgentJsapiTicket(jsapTicket);
        Date now = new Date();
        ticket.setCreateTime(now);
        ticket.setAgentTicketExp(TimeUtil.addTime(now, expire));
        ticket.save();
        return ticket;
    }

    @Override
    public void updateAgentTicket(Integer id, String corpid, String agentId, String jsapTicket, Integer expire) {
        JsapiTicket ticket = JsapiTicket.dao.findById(id);
        ticket.setCorpid(corpid);
        ticket.setAgentId(agentId);
        ticket.setAgentJsapiTicket(jsapTicket);
        Date now = new Date();
        ticket.setAgentTicketReset(now);
        ticket.setAgentTicketExp(TimeUtil.addTime(now, expire));
        ticket.update();
    }

    @Override
    public JsapiTicket saveCorpTicket(String corpid, String agentId, String jsapTicket, Integer expire) {
        JsapiTicket ticket = new JsapiTicket();
        ticket.setCorpid(corpid);
        ticket.setAgentId(agentId);
        ticket.setCorpJsapiTicket(jsapTicket);
        Date now = new Date();
        ticket.setCreateTime(now);
        ticket.setCorpTicketExp(TimeUtil.addTime(now, expire));
        ticket.save();
        return ticket;
    }

    @Override
    public void updateCorpTicket(Integer id, String corpid, String agentId, String jsapTicket, Integer expire) {
        JsapiTicket ticket = JsapiTicket.dao.findById(id);
        ticket.setCorpid(corpid);
        ticket.setAgentId(agentId);
        ticket.setCorpJsapiTicket(jsapTicket);
        Date now = new Date();
        ticket.setCorpTicketReset(now);
        ticket.setCorpTicketExp(TimeUtil.addTime(now, expire));
        ticket.update();
    }

}
