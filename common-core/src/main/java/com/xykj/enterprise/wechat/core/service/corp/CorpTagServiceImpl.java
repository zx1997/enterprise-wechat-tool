package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetCorpTagListVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.Tag;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.Tag_group;
import com.xykj.enterprise.wechat.core.dao.CorpExtDao;
import com.xykj.enterprise.wechat.core.dao.customer.CorpTagDao;
import com.xykj.enterprise.wechat.core.dao.customer.CorpTagGroupDao;
import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;
import com.xykj.enterprise.wechat.core.model.dodb.CorpTag;
import com.xykj.enterprise.wechat.core.model.dodb.CorpTagGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author zhouxu
 * @create 2021-04-12 10:47
 */
@Service
public class CorpTagServiceImpl implements CorpTagService {


    @Autowired
    private CorpTagDao corpTagDao;
    @Autowired
    private CorpTagGroupDao corpTagGroupDao;
    @Autowired
    private CorpExtDao corpExtDao;

    @Override
    public void syncCorpTag(GetCorpTagListVo listVo, String corpid) {

        if (listVo != null && !listVo.getTag_group().isEmpty()) {
            for (Tag_group group : listVo.getTag_group()) {
                // tag_group
                CorpTagGroup g = corpTagGroupDao.get(corpid, group.getGroup_id());

                if (g == null) {
                    corpTagGroupDao.save(
                            corpid,
                            group.getGroup_id(),
                            group.getGroup_name(),
                            group.getOrder(),
                            group.isDeleted() ? "1" : "0"
                    );
                } else {
                    corpTagGroupDao.update(
                            g.getId(),
                            corpid,
                            group.getGroup_id(),
                            group.getGroup_name(),
                            group.getOrder(),
                            group.isDeleted() ? "1" : "0"
                    );
                }

                if (group.getTag() != null) {
                    for (Tag tag : group.getTag()) {
                        // tag
                        CorpTag t = corpTagDao.get(corpid, tag.getId(), group.getGroup_id());
                        if (t == null) {
                            corpTagDao.save(
                                    corpid,
                                    tag.getId(),
                                    group.getGroup_id(),
                                    tag.getName(),
                                    tag.getOrder(),
                                    tag.isDeleted() ? "1" : "0"
                            );
                        } else {
                            corpTagDao.update(
                                    t.getId(),
                                    corpid,
                                    tag.getId(),
                                    group.getGroup_id(),
                                    tag.getName(),
                                    tag.getOrder(),
                                    tag.isDeleted() ? "1" : "0"
                            );
                        }

                    }
                }

            }
        }

        // 同步完成标记
        corpExtDao.syncCorpTagData(corpid, CorpExt.SUCC);
    }


}
