package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseJsapiTicket<M extends BaseJsapiTicket<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Integer id) {
		set("id", id);
	}
	
	public java.lang.Integer getId() {
		return getInt("id");
	}

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	public void setAgentId(java.lang.String agentId) {
		set("agent_id", agentId);
	}
	
	public java.lang.String getAgentId() {
		return getStr("agent_id");
	}

	public void setAgentJsapiTicket(java.lang.String agentJsapiTicket) {
		set("agent_jsapi_ticket", agentJsapiTicket);
	}
	
	public java.lang.String getAgentJsapiTicket() {
		return getStr("agent_jsapi_ticket");
	}

	public void setAgentTicketReset(java.util.Date agentTicketReset) {
		set("agent_ticket_reset", agentTicketReset);
	}
	
	public java.util.Date getAgentTicketReset() {
		return get("agent_ticket_reset");
	}

	public void setAgentTicketExp(java.util.Date agentTicketExp) {
		set("agent_ticket_exp", agentTicketExp);
	}
	
	public java.util.Date getAgentTicketExp() {
		return get("agent_ticket_exp");
	}

	public void setCorpJsapiTicket(java.lang.String corpJsapiTicket) {
		set("corp_jsapi_ticket", corpJsapiTicket);
	}
	
	public java.lang.String getCorpJsapiTicket() {
		return getStr("corp_jsapi_ticket");
	}

	public void setCorpTicketReset(java.util.Date corpTicketReset) {
		set("corp_ticket_reset", corpTicketReset);
	}
	
	public java.util.Date getCorpTicketReset() {
		return get("corp_ticket_reset");
	}

	public void setCorpTicketExp(java.util.Date corpTicketExp) {
		set("corp_ticket_exp", corpTicketExp);
	}
	
	public java.util.Date getCorpTicketExp() {
		return get("corp_ticket_exp");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
