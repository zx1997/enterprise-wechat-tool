package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectMemberVo;
import com.xykj.enterprise.wechat.core.dao.pc.PCProjectMemberDao;
import com.xykj.enterprise.wechat.core.model.dodb.PcProjectMember;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import com.ydn.dbframe.plugin.activerecord.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-13 20:18
 */
@Service
public class PCProjectMemberServiceImpl implements PCProjectMemberService {

    @Autowired
    private PCProjectMemberDao pcProjectMemberDao;

    @Override
    public Page<PcProjectMember> list(Integer page, Integer limit, Map<String,Object> param) {
        return pcProjectMemberDao.list(page, limit, param);
    }

    @Override
    public PcProjectMember get(Integer id, String corpid, String userId) {
        return pcProjectMemberDao.get(id.longValue(),corpid);
    }

    @Override
    public void save(ProjectMemberVo vo, String corpid, String userId) {
        pcProjectMemberDao.save(
                corpid,
                vo.getProjectId(),
                vo.getMemberId(),
                vo.getMemberContent(),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getPlanStartTime()),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getRealStartTime()),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getPlanEndTime()),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getRealEndTime()),
                userId
        );
    }

    @Override
    public void update(ProjectMemberVo vo, String corpid, String userId) {
        pcProjectMemberDao.update(
                vo.getId(),
                corpid,
                vo.getProjectId(),
                vo.getMemberId(),
                vo.getMemberContent(),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getPlanStartTime()),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getRealStartTime()),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getPlanEndTime()),
                TimeUtil.parse(TimeUtil.FORMAT_YYYYMMDDHHMMSS, vo.getRealEndTime()),
                userId
        );
    }

    @Override
    public void delete(Long id, String corpid, String userId) {
        pcProjectMemberDao.delete(id, corpid);
    }
}
