package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.contacts.member.UserListVo;
import com.xykj.enterprise.wechat.bean.ext.contacts.member.UserVo;
import com.xykj.enterprise.wechat.bean.ext.identity.SuiteAccessToken;
import com.xykj.enterprise.wechat.bean.ext.identity.UserInfo3rd;
import com.xykj.enterprise.wechat.core.dao.CorpExtDao;
import com.xykj.enterprise.wechat.core.dao.SuiteTicketDao;
import com.xykj.enterprise.wechat.core.dao.UserDao;
import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;
import com.xykj.enterprise.wechat.core.model.dodb.User;
import com.xykj.enterprise.wechat.util.wecom.identity.SuiteAccessTokenUtil;
import com.xykj.enterprise.wechat.util.wecom.identity.UserInfo3rdUtil;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import com.ydn.simplecache.SimpleCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private SuiteTicketDao suiteTicketDao;
    @Autowired
    private SimpleCache simpleCache;
    @Autowired
    private CorpExtDao corpExtDao;

    @Override
    public User getById(String userId) {
        return userDao.getById(userId);
    }

    @Override
    public Page<User> list(Integer page, Integer limit, Map<String, Object> param) {
        SqlParse sqlParse = new SqlParse(param);
        sqlParse.addSQL("select * from t_user ");
        sqlParse.addSQL(" where 1=1 ");
        sqlParse.addSQL(" and corpid = :corpid ");
        return User.dao.paginate(page, limit, sqlParse);
    }

    @Override
    public UserInfo3rd get(String suiteId, String code, String secret) {
        // 获取suiteAccessToken
        SuiteAccessToken suiteAccessToken = SuiteAccessTokenUtil.get(
                simpleCache,
                suiteId,
                secret,
                suiteTicketDao.getLatest(suiteId).getSuitTicket()
        );
        // 获取permanentCode
        UserInfo3rd userInfo3rd = UserInfo3rdUtil.get(suiteAccessToken.getSuite_access_token(), code);
        return userInfo3rd;

    }

    @Override
    public void syncUserList(UserListVo listVo, String corpid) {
        if (listVo != null && !listVo.getUserlist().isEmpty()) {
            for (UserVo vo : listVo.getUserlist()) {
                User user = userDao.get(vo.getUserid(), corpid);
                if (user == null) {
                    userDao.save(
                            corpid,
                            vo.getUserid(),
                            vo.getName(),
                            vo.getMobile(),
                            vo.getDepartment().toString(),
                            vo.getOrder().toString(),
                            vo.getPosition(),
                            vo.getGender(),
                            vo.getEmail(),
                            vo.getIs_leader_in_dept().toString(),
                            vo.getAvatar(),
                            vo.getThumb_avatar(),
                            vo.getTelephone(),
                            vo.getAlias(),
                            vo.getStatus(),
                            vo.getQr_code(),
                            vo.getExternal_profile(),
                            vo.getAddress(),
                            vo.getOpen_userid(),
                            vo.getMain_department()
                    );
                } else {
                    userDao.update(
                            user.getId(),
                            corpid,
                            vo.getUserid(),
                            vo.getName(),
                            vo.getMobile(),
                            vo.getDepartment().toString(),
                            vo.getOrder().toString(),
                            vo.getPosition(),
                            vo.getGender(),
                            vo.getEmail(),
                            vo.getIs_leader_in_dept().toString(),
                            vo.getAvatar(),
                            vo.getThumb_avatar(),
                            vo.getTelephone(),
                            vo.getAlias(),
                            vo.getStatus(),
                            vo.getQr_code(),
                            vo.getExternal_profile(),
                            vo.getAddress(),
                            vo.getOpen_userid(),
                            vo.getMain_department()
                    );
                }
            }
        }

        // 同步完成标记
        corpExtDao.syncUserData(corpid, CorpExt.SUCC);
    }
}
