package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectLogVo;
import com.xykj.enterprise.wechat.core.dao.pc.PCProjectLogDao;
import com.xykj.enterprise.wechat.core.model.dodb.PcProjectLog;
import com.ydn.dbframe.plugin.activerecord.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-13 20:08
 */
@Repository
public class PCProjectLogServiceImpl implements PCProjectLogService {

    @Autowired
    private PCProjectLogDao pcProjectLogDao;

    @Override
    public Page<PcProjectLog> list(Integer page, Integer limit, Map<String,Object> param) {

        return pcProjectLogDao.list(page,limit,param);
    }

    @Override
    public void save(ProjectLogVo vo, String corpid, String userId) {
        pcProjectLogDao.save(
                corpid,
                vo.getWorkDay(),
                vo.getWorker(),
                vo.getProjectId(),
                vo.getWorkContent(),
                new BigDecimal(vo.getWorkHour()),
                userId
        );
    }

    @Override
    public void update(ProjectLogVo vo, String corpid, String userId) {
        pcProjectLogDao.update(
                vo.getId(),
                corpid,
                vo.getWorkDay(),
                vo.getWorker(),
                vo.getProjectId(),
                vo.getWorkContent(),
                new BigDecimal(vo.getWorkHour()),
                userId
        );
    }

    @Override
    public void delete(Long id, String corpid, String userId) {
        pcProjectLogDao.delete(id,corpid);
    }
}
