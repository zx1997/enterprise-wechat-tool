package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.CorpSuite;

/**
 * @Author george
 * @create 2021-04-28 15:18
 */
public interface CorpSuiteDao {

    CorpSuite get(String corpid, String suiteId);

    /**
     * 新增
     * @param corpid
     * @param suiteId
     * @param permanentCode
     * @param accessToken
     * @param expireTime 有效时间，单位：秒
     */
    void save(String corpid, String suiteId, String permanentCode, String accessToken, Integer expireTime);

    /**
     * 修改
     * @param corpid
     * @param suiteId
     * @param permanentCode
     * @param accessToken
     * @param expireTime 有效时间，单位：秒
     */
    void update(String corpid, String suiteId, String permanentCode, String accessToken, Integer expireTime);

    /**
     * 更新access_token
     * @param corpid
     * @param suiteId
     * @param accessToken
     * @param expireTime
     */
    CorpSuite resetToken(String corpid, String suiteId, String accessToken, Integer expireTime);

}
