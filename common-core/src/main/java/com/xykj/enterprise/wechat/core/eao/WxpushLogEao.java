package com.xykj.enterprise.wechat.core.eao;

import com.xykj.enterprise.wechat.core.model.eos.WxpushLogEo;
import com.ydn.dbframe.plugin.activerecord.Page;

/**
 * @Author zhouxu
 * @create 2021-02-26 10:00
 */
public interface WxpushLogEao {

    void save(String msgSignatuer, String timestamp, String nonce, String data, String msg, String jsonMsg,
              String msgType, String event, String changeType);

    Page<WxpushLogEo> listByDayTime(Integer dayTime, Integer pageNumber, Integer pageSize, Class<WxpushLogEo> clazz);

    WxpushLogEo queryById(String logId);
}
