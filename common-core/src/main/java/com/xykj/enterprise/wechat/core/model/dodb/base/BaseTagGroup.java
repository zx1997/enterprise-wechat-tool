package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseTagGroup<M extends BaseTagGroup<M>> extends Model<M> implements IBean {

	/**
	 * id
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * id
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpId(java.lang.Long corpId) {
		set("corp_id", corpId);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.Long getCorpId() {
		return getLong("corp_id");
	}

	/**
	 * 标签组名称
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 标签组名称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 标签组排序的次序值，order值大的排序靠前。有效的值范围是[0, 2^32)
	 */
	public void setOrder(java.lang.Long order) {
		set("order", order);
	}
	
	/**
	 * 标签组排序的次序值，order值大的排序靠前。有效的值范围是[0, 2^32)
	 */
	public java.lang.Long getOrder() {
		return getLong("order");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 是否已经被删除 0-正常 1-被删除
	 */
	public void setDeleted(java.lang.String deleted) {
		set("deleted", deleted);
	}
	
	/**
	 * 是否已经被删除 0-正常 1-被删除
	 */
	public java.lang.String getDeleted() {
		return getStr("deleted");
	}

}
