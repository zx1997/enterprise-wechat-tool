package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomerFollowStage<M extends BaseCustomerFollowStage<M>> extends Model<M> implements IBean {

	/**
	 * stage ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * stage ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 跟进阶段code
	 */
	public void setCode(java.lang.String code) {
		set("code", code);
	}
	
	/**
	 * 跟进阶段code
	 */
	public java.lang.String getCode() {
		return getStr("code");
	}

	/**
	 * 跟进阶段
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 跟进阶段
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 备注信息
	 */
	public void setMark(java.lang.String mark) {
		set("mark", mark);
	}
	
	/**
	 * 备注信息
	 */
	public java.lang.String getMark() {
		return getStr("mark");
	}

}
