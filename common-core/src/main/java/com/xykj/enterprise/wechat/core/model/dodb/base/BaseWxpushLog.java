package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseWxpushLog<M extends BaseWxpushLog<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setLogId(java.lang.Long logId) {
		set("log_id", logId);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getLogId() {
		return getLong("log_id");
	}

	/**
	 * 签名文本
	 */
	public void setMsgSignature(java.lang.String msgSignature) {
		set("msg_signature", msgSignature);
	}
	
	/**
	 * 签名文本
	 */
	public java.lang.String getMsgSignature() {
		return getStr("msg_signature");
	}

	/**
	 * 时间戳
	 */
	public void setTimestamp(java.lang.String timestamp) {
		set("timestamp", timestamp);
	}
	
	/**
	 * 时间戳
	 */
	public java.lang.String getTimestamp() {
		return getStr("timestamp");
	}

	/**
	 * 随机数
	 */
	public void setNonce(java.lang.String nonce) {
		set("nonce", nonce);
	}
	
	/**
	 * 随机数
	 */
	public java.lang.String getNonce() {
		return getStr("nonce");
	}

	/**
	 * 消息文本（xml格式，加密）
	 */
	public void setData(java.lang.String data) {
		set("data", data);
	}
	
	/**
	 * 消息文本（xml格式，加密）
	 */
	public java.lang.String getData() {
		return getStr("data");
	}

	/**
	 * 消息的类型
	 */
	public void setMsgType(java.lang.String msgType) {
		set("msg_type", msgType);
	}
	
	/**
	 * 消息的类型
	 */
	public java.lang.String getMsgType() {
		return getStr("msg_type");
	}

	/**
	 * 事件的类型
	 */
	public void setEvent(java.lang.String event) {
		set("event", event);
	}
	
	/**
	 * 事件的类型
	 */
	public java.lang.String getEvent() {
		return getStr("event");
	}

	/**
	 * 动作的类型
	 */
	public void setChangeType(java.lang.String changeType) {
		set("change_type", changeType);
	}
	
	/**
	 * 动作的类型
	 */
	public java.lang.String getChangeType() {
		return getStr("change_type");
	}

	/**
	 * 消息文本（xml，解密）
	 */
	public void setMsg(java.lang.String msg) {
		set("msg", msg);
	}
	
	/**
	 * 消息文本（xml，解密）
	 */
	public java.lang.String getMsg() {
		return getStr("msg");
	}

	/**
	 * json格式内容
	 */
	public void setJsonMsg(java.lang.String jsonMsg) {
		set("json_msg", jsonMsg);
	}
	
	/**
	 * json格式内容
	 */
	public java.lang.String getJsonMsg() {
		return getStr("json_msg");
	}

	/**
	 * 状态（0未处理 1已处理）
	 */
	public void setStatus(java.lang.String status) {
		set("status", status);
	}
	
	/**
	 * 状态（0未处理 1已处理）
	 */
	public java.lang.String getStatus() {
		return getStr("status");
	}

	/**
	 * 创建日期
	 */
	public void setDayTime(java.lang.Integer dayTime) {
		set("day_time", dayTime);
	}
	
	/**
	 * 创建日期
	 */
	public java.lang.Integer getDayTime() {
		return getInt("day_time");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 修改时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	/**
	 * 修改时间
	 */
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
