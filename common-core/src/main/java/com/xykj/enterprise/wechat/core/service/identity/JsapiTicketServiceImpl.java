package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.bean.ext.identity.JsapiTicketRsp;
import com.xykj.enterprise.wechat.core.dao.JsapiTicketDao;
import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.model.dodb.JsapiTicket;
import com.xykj.enterprise.wechat.util.wecom.identity.IdentityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author george
 * @create 2021-04-19 21:04
 */
@Slf4j
@Service
public class JsapiTicketServiceImpl implements JsapiTicketService {
    @Autowired
    private JsapiTicketDao jsapiTicketDao;
    @Autowired
    private IdentityService identityService;

    @Override
    public JsapiTicket getAgentTicket(String corpid, String agentId) {
        JsapiTicket ticket = jsapiTicketDao.get(corpid, agentId);
        if (ticket == null) {
            return null;
        }
        if (ticket.isAgentTicketExpire()) {
            return null;
        }
        return ticket;
    }

    @Override
    public JsapiTicket getCorpTicket(String corpid, String agentId) {
        JsapiTicket ticket = jsapiTicketDao.get(corpid, agentId);
        if (ticket == null) {
            return null;
        }
        if (ticket.isCorpTicketExpire()) {
            return null;
        }
        return ticket;
    }

    @Override
    public void saveAgentTicket(String corpid, String agentId, String jsapTicket, Integer expire) {
        JsapiTicket ticket = jsapiTicketDao.get(corpid, agentId);
        if (ticket == null) {
            jsapiTicketDao.saveAgentTicket(corpid, agentId, jsapTicket, expire);
        } else {
            jsapiTicketDao.updateAgentTicket(ticket.getId(), corpid, agentId, jsapTicket, expire);
        }
    }

    @Override
    public void saveCorpTicket(String corpid, String agentId, String jsapTicket, Integer expire) {
        JsapiTicket ticket = jsapiTicketDao.get(corpid, agentId);
        if (ticket == null) {
            jsapiTicketDao.saveCorpTicket(corpid, agentId, jsapTicket, expire);
        } else {
            jsapiTicketDao.updateCorpTicket(ticket.getId(), corpid, agentId, jsapTicket, expire);
        }
    }

    @Override
    public String getAgentTicket(
            String corpid,
            String agentId,
            String suiteId,
            String secret
    ) {
        JsapiTicket ticket = getAgentTicket(corpid, agentId);
        String jsapi_ticket;
        if (ticket == null) {
            // 重新获取 应用 jsapi_ticket
            JsapiTicketRsp rsp = IdentityUtil.getAgentJsapiTicket(
                    identityService.getAccessToken(corpid, suiteId, secret)
            );
            saveAgentTicket(corpid, agentId, rsp.getTicket(), rsp.getExpires_in());

            jsapi_ticket = rsp.getTicket();
        } else {
            jsapi_ticket = ticket.getAgentJsapiTicket();
        }
        return jsapi_ticket;
    }

    /**
     * 获取 应用 jsapi_ticket
     *
     * @param corpid
     * @param agentId
     * @param suiteId
     * @param secret
     * @return
     */
    @Override
    public String getCorpTicket(
            String corpid,
            String agentId,
            String suiteId,
            String secret
    ) {
        JsapiTicket ticket = getCorpTicket(corpid, agentId);
        String jsapi_ticket;
        if (ticket == null) {
            JsapiTicketRsp rsp = IdentityUtil.getCorpJsapiTicket(
                    identityService.getAccessToken(corpid, suiteId, secret)
            );
            log.debug("企业 jsapi ticket :", rsp);
            saveCorpTicket(corpid, agentId, rsp.getTicket(), rsp.getExpires_in());

            jsapi_ticket = rsp.getTicket();
        } else {
            jsapi_ticket = ticket.getCorpJsapiTicket();
        }
        return jsapi_ticket;
    }
}
