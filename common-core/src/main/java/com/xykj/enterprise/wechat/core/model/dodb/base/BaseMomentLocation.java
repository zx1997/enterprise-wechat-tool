package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMomentLocation<M extends BaseMomentLocation<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 地理位置纬度
	 */
	public void setLatitude(java.lang.String latitude) {
		set("latitude", latitude);
	}
	
	/**
	 * 地理位置纬度
	 */
	public java.lang.String getLatitude() {
		return getStr("latitude");
	}

	/**
	 * 地理位置经度
	 */
	public void setLongitude(java.lang.String longitude) {
		set("longitude", longitude);
	}
	
	/**
	 * 地理位置经度
	 */
	public java.lang.String getLongitude() {
		return getStr("longitude");
	}

	/**
	 * 地理位置名称
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 地理位置名称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 朋友圈ID
	 */
	public void setMomentId(java.lang.String momentId) {
		set("moment_id", momentId);
	}
	
	/**
	 * 朋友圈ID
	 */
	public java.lang.String getMomentId() {
		return getStr("moment_id");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
