package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseDepartment;

/**
 */
@SuppressWarnings("serial")
public class Department extends BaseDepartment<Department> {
	public static final Department dao = new Department().dao();

}
