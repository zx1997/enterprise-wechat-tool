package com.xykj.enterprise.wechat.core.config;

import com.xykj.enterprise.wechat.core.es.EsClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Configuration
public class EsConfig {

    @Value("${es.addrs}")
    private String addrs;

    @Value("${es.cluster}")
    private String cluster;

    @Bean(value = "esClient")
    public EsClient create() {
        EsClient esClient = new EsClient(cluster, addrs);
        return esClient;
    }

}
