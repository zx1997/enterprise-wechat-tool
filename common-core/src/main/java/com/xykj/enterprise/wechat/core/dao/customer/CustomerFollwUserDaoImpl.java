package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowUser;
import com.ydn.dbframe.plugin.activerecord.Db;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-09 10:16
 */
@Repository
public class CustomerFollwUserDaoImpl implements CustomerFollowUserDao {

    @Override
    public CustomerFollowUser get(String externalUserid, String corpid) {
        String sql = " select * from t_customer_follow_user where external_userid = ? and corpid = ? ";
        return CustomerFollowUser.dao.findFirst(sql, externalUserid, corpid);
    }

    @Override
    public CustomerFollowUser get(String externalUserid, String corpid, String userid) {
        String sql = " select * from t_customer_follow_user where external_userid = ? and corpid = ? and userid = ? ";
        return CustomerFollowUser.dao.findFirst(sql, externalUserid, corpid, userid);
    }

    @Override
    public List<CustomerFollowUser> list(String externalUserid, String corpid) {
        String sql = " select * from t_customer_follow_user where external_userid = ? and corpid = ? ";
        return CustomerFollowUser.dao.find(sql, externalUserid, corpid);
    }

    @Override
    public void delete(CustomerFollowUser followUser) {
        if (followUser != null) {
            followUser.delete();
        }

    }

    @Override
    public int batchDelete(String externalUserid, String corpid) {
        String sql = " delete from t_customer_follow_user where external_userid = ? and corpid = ? ";
        return Db.update(sql, externalUserid, corpid);
    }

    @Override
    public void save(
            String corpid,
            String externalUserid,
            Long customerId,
            String userid,
            String remark,
            String description,
            Long createtime,
            String remarkCorpName,
            String remarkMobile,
            String openUserid,
            String addWay,
            String state,
            String tags
    ) {

        CustomerFollowUser followUser = new CustomerFollowUser();
        followUser.setCorpid(corpid);
        followUser.setExternalUserid(externalUserid);
        followUser.setCustomerId(customerId);
        followUser.setUserid(userid);
        followUser.setRemark(remark);
        followUser.setDescription(description);
        followUser.setCreatetime(createtime);
        followUser.setRemarkCorpName(remarkCorpName);
        followUser.setRemarkMobiles(remarkMobile);
        followUser.setOperUserid(openUserid);
        followUser.setAddWay(addWay);
        followUser.setState(state);
        followUser.setTags(tags);
        followUser.setCreateTime(new Date());
        followUser.save();
    }

    @Override
    public void update(Long id, String corpid, String externalUserid, Long customerId, String userid, String remark, String description, Long createtime, String remarkCorpName, String remarkMobile, String openUserid, String addWay, String state, String tags) {
        CustomerFollowUser followUser = CustomerFollowUser.dao.findById(id);
        followUser.setCorpid(corpid);
        followUser.setExternalUserid(externalUserid);
        followUser.setCustomerId(customerId);
        followUser.setUserid(userid);
        followUser.setRemark(remark);
        followUser.setDescription(description);
        followUser.setCreatetime(createtime);
        followUser.setRemarkCorpName(remarkCorpName);
        followUser.setRemarkMobiles(remarkMobile);
        followUser.setOperUserid(openUserid);
        followUser.setAddWay(addWay);
        followUser.setState(state);
        followUser.setTags(tags);
        followUser.setUpdateTime(new Date());
        followUser.update();
    }
}
