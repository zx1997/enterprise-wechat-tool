package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.bean.busi.vo.wxpush.SuiteTicketVo;
import com.xykj.enterprise.wechat.core.dao.SuiteTicketDao;
import com.xykj.enterprise.wechat.core.model.dodb.SuiteTicket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author zhouxu
 * @create 2021-03-29 21:46
 */
@Service
public class SuiteTicketServiceImpl implements SuiteTicketService {

    @Autowired
    private SuiteTicketDao suiteTicketDao;

    @Override
    public SuiteTicket getLatest(String suiteId) {
        return suiteTicketDao.getLatest(suiteId);
    }

    @Override
    public Long create(SuiteTicketVo vo) {
        return suiteTicketDao.save(
                vo.getSuiteId(),
                vo.getInfoType(),
                vo.getTimeStamp(),
                vo.getSuiteTicket()
        );
    }


}
