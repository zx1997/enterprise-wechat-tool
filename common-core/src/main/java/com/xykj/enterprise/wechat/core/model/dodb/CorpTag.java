package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseCorpTag;

/**
 */
@SuppressWarnings("serial")
public class CorpTag extends BaseCorpTag<CorpTag> {
	public static final CorpTag dao = new CorpTag().dao();

}
