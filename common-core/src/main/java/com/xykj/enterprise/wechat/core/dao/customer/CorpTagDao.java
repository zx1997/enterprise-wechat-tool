package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CorpTag;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.List;

public interface CorpTagDao {

    Page<CorpTag> findByGroup(String corpId,String groupId, Integer pageNumber, Integer pageSize);

    List<CorpTag> listByGroup(String corpId, String groupId);

    CorpTag get(String corpid, String tagId, String groupId);

    CorpTag save(
            String corpid,
            String tagId,
            String groupId,
            String name,
            Integer order,
            String deleted
    );

    void update(
            Long id,
            String corpid,
            String tagId,
            String groupId,
            String name,
            Integer order,
            String deleted
    );
}
