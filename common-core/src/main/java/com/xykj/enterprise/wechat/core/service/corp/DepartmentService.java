package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.contacts.department.DepartmentListVo;
import com.xykj.enterprise.wechat.core.model.dodb.Department;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-09 09:10
 */
public interface DepartmentService {

    List<Department> list(String corpid, Integer parentid);

    void syncDepartList(DepartmentListVo listVo, String corpid);
}
