package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.identity.GetPermanentCodeVo;
import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.model.dodb.CorpExt;

/**
 * @Author zhouxu
 * @create 2021-03-31 14:49
 */
public interface CorpService {

    /**
     * 保存认证企业信息
     * @param getPermanentCodeVo
     */
    void save(String suiteId, GetPermanentCodeVo getPermanentCodeVo);


    /**
     * 企业扩展信息
     * @param corpid
     * @return
     */
    CorpExt getCorpExt(String corpid);

    /**
     * 初始化企业扩展信息
     * @param corpid
     */
    void initExtInfo(String corpid);
}
