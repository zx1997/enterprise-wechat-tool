package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.JsapiTicket;

/**
 * @Author george
 * @create 2021-04-19 20:56
 */
public interface JsapiTicketDao {

    JsapiTicket get(String corpid, String agentId);

    JsapiTicket saveAgentTicket(
            String corpid,
            String agentId,
            String jsapTicket,
            Integer expire
    );

    void updateAgentTicket(
            Integer id,
            String corpid,
            String agentId,
            String jsapTicket,
            Integer expire
    );

    JsapiTicket saveCorpTicket(
            String corpid,
            String agentId,
            String jsapTicket,
            Integer expire
    );

    void updateCorpTicket(
            Integer id,
            String corpid,
            String agentId,
            String jsapTicket,
            Integer expire
    );
}
