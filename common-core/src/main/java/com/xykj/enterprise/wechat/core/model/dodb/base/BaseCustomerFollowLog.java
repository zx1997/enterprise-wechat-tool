package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomerFollowLog<M extends BaseCustomerFollowLog<M>> extends Model<M> implements IBean {

	public void setLogId(java.lang.Long logId) {
		set("log_id", logId);
	}
	
	public java.lang.Long getLogId() {
		return getLong("log_id");
	}

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	public void setCustomerId(java.lang.String customerId) {
		set("customer_id", customerId);
	}
	
	public java.lang.String getCustomerId() {
		return getStr("customer_id");
	}

	public void setUserId(java.lang.String userId) {
		set("user_id", userId);
	}
	
	public java.lang.String getUserId() {
		return getStr("user_id");
	}

	public void setFollowContent(java.lang.String followContent) {
		set("follow_content", followContent);
	}
	
	public java.lang.String getFollowContent() {
		return getStr("follow_content");
	}

	public void setFollowStatus(java.lang.String followStatus) {
		set("follow_status", followStatus);
	}
	
	public java.lang.String getFollowStatus() {
		return getStr("follow_status");
	}

	public void setScheduleId(java.lang.String scheduleId) {
		set("schedule_id", scheduleId);
	}
	
	public java.lang.String getScheduleId() {
		return getStr("schedule_id");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
