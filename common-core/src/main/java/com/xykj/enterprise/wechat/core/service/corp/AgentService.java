package com.xykj.enterprise.wechat.core.service.corp;

import com.xykj.enterprise.wechat.bean.ext.identity.Admin;
import com.xykj.enterprise.wechat.bean.ext.identity.Agent;
import com.xykj.enterprise.wechat.core.model.dodb.AgentAdmin;
import com.xykj.enterprise.wechat.core.model.dodb.AgentInfo;

import java.util.List;

/**
 * @Author george
 * @create 2021-05-28 17:49
 */
public interface AgentService {

    void saveAgentInfo(Agent agent, String corpid, String suiteId);

    List<AgentAdmin> listAdmin(String corpid, String suiteId, String agentId);

    AgentInfo getAgent(String corpid, String suiteId);

    void saveAgentAdmin(List<Admin> adminList,String agentId, String corpid, String suiteId);

}
