package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.Customer;

public interface CustomerDao {

    Customer findById(Long id);

    Customer findByExternalUserid(String externalUserid, String corpid);

    Customer get(String corpId, String externalUserid);

    Customer save(
            String corpid,
            String externalUserid,
            String name,
            String position,
            String avatar,
            String type,
            String gender,
            String unionid,
            String corpName,
            String corpFullName,
            String tag,
            String mobile,
            String mark
    );

    Customer update(
            Long id,
            String corpid,
            String externalUserid,
            String name,
            String position,
            String avatar,
            String type,
            String gender,
            String unionid,
            String corpName,
            String corpFullName,
            String tag,
            String mobile,
            String mark
    );

    int delete(String corpid, String externalUserid);

}
