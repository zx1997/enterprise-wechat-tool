package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.GroupChatMember;
import com.ydn.dbframe.plugin.activerecord.Page;

/**
 * 聊天群
 *
 * @Author george
 * @create 2021-04-22 13:44
 */
public interface GroupChatMemberDao {

    GroupChatMember get(String corpid, String chatId, String userid);


    Long save(
            String chatId,
            String corpid,
            String userid,
            String type,
            String unionid,
            String join_scene,
            String join_time,
            String invitor

    );

    void update(
            Long id,
            String chatId,
            String corpid,
            String userid,
            String type,
            String unionid,
            String join_scene,
            String join_time,
            String invitor
    );

    Page<GroupChatMember> list(Integer page, Integer limit, String corpid);

    void delete(String chatId,String corpid);

}
