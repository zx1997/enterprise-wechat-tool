package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseGroupChat<M extends BaseGroupChat<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 客户群ID
	 */
	public void setChatId(java.lang.String chatId) {
		set("chat_id", chatId);
	}
	
	/**
	 * 客户群ID
	 */
	public java.lang.String getChatId() {
		return getStr("chat_id");
	}

	/**
	 * 群名
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 群名
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 群主ID
	 */
	public void setOwner(java.lang.String owner) {
		set("owner", owner);
	}
	
	/**
	 * 群主ID
	 */
	public java.lang.String getOwner() {
		return getStr("owner");
	}

	/**
	 * 群公告
	 */
	public void setNotice(java.lang.String notice) {
		set("notice", notice);
	}
	
	/**
	 * 群公告
	 */
	public java.lang.String getNotice() {
		return getStr("notice");
	}

	/**
	 * 管理员ID列表-数组
	 */
	public void setAdminList(java.lang.String adminList) {
		set("admin_list", adminList);
	}
	
	/**
	 * 管理员ID列表-数组
	 */
	public java.lang.String getAdminList() {
		return getStr("admin_list");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 更新时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	/**
	 * 更新时间
	 */
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
