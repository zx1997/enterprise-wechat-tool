package com.xykj.enterprise.wechat.core.service.customer;

import com.xykj.enterprise.wechat.bean.busi.vo.CorpTagGroupVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.ExternalContactVo;
import com.xykj.enterprise.wechat.bean.ext.externalcontact.GetExternalContactBatchVo;
import com.xykj.enterprise.wechat.core.model.dodb.Customer;
import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowLog;
import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowUser;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.List;

public interface CustomerService {


    /**
     * 客户查询
     *
     * @param externalUserid 客户userid
     * @param corpid         企业id
     * @return
     */
    Customer findByExternalUserid(String externalUserid, String corpid);


    List<CorpTagGroupVo> findAllCorpTagGroup(String corpid);

    Page<CustomerFollowLog> getFollowLog(Integer page, Integer limit, String corpid, String userId, String customerId);

    /**
     * 保存跟进记录
     *
     * @param logId
     * @param corpid
     * @param userId
     * @param customerId
     * @param followContent
     * @param followStatus
     * @param scheduleId
     */
    void saveFollowLog(
            Long logId,
            String corpid,
            String userId,
            String customerId,
            String followContent,
            String followStatus,
            String scheduleId
    );

    /**
     * 删除跟进记录
     *
     * @param logId
     */
    void deleteFollowId(Long logId);

    /**
     * 员工个人信息--跟踪客户信息
     *
     * @param externalUserid
     * @param userid
     * @param corpid
     * @return
     */
    CustomerFollowUser getUserFollowInfo(String externalUserid, String corpid, String userid);

    void syncCustomerList(GetExternalContactBatchVo batchVo, String corpid, String userid);

    void save(String corpid, ExternalContactVo contact);

    void delete(String corpid, String externalUserid);

    void deleteFollowUser(String corpid, String externalUserid, String userid);
}
