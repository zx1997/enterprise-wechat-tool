package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCorpSuite<M extends BaseCorpSuite<M>> extends Model<M> implements IBean {

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	public void setSuiteId(java.lang.String suiteId) {
		set("suite_id", suiteId);
	}
	
	public java.lang.String getSuiteId() {
		return getStr("suite_id");
	}

	/**
	 * 企业永久授权码（根据corpid，suite_id不同）
	 */
	public void setPermanentCode(java.lang.String permanentCode) {
		set("permanent_code", permanentCode);
	}
	
	/**
	 * 企业永久授权码（根据corpid，suite_id不同）
	 */
	public java.lang.String getPermanentCode() {
		return getStr("permanent_code");
	}

	public void setAccessToken(java.lang.String accessToken) {
		set("access_token", accessToken);
	}
	
	public java.lang.String getAccessToken() {
		return getStr("access_token");
	}

	/**
	 * access_token重置时间
	 */
	public void setTokenResetTime(java.util.Date tokenResetTime) {
		set("token_reset_time", tokenResetTime);
	}
	
	/**
	 * access_token重置时间
	 */
	public java.util.Date getTokenResetTime() {
		return get("token_reset_time");
	}

	/**
	 * access_token失效时间
	 */
	public void setTokenExpTime(java.util.Date tokenExpTime) {
		set("token_exp_time", tokenExpTime);
	}
	
	/**
	 * access_token失效时间
	 */
	public java.util.Date getTokenExpTime() {
		return get("token_exp_time");
	}

}
