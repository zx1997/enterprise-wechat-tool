package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.User;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class UserDaoImpl implements UserDao {

    @Override
    public User getById(String id) {
        String sql = "select * from t_user where id = ? ";
        return User.dao.findFirst(sql, id);
    }

    @Override
    public User get(String userid, String corpid) {
        String sql = " select * from t_user where userid = ? and corpid = ? ";
        return User.dao.findFirst(sql, userid, corpid);
    }

    @Override
    public Long save(
            String corpid,
            String userid,
            String name,
            String mobile,
            String department,
            String order,
            String position,
            String gender,
            String email,
            String isLeaderInDept,
            String avatar,
            String thumbAvatar,
            String telephone,
            String alias,
            String status,
            String qrCode,
            String externalPosition,
            String address,
            String openUserid,
            String mainDepartment
    ) {
        User user = new User();
        user.setCorpid(corpid);
        user.setUserid(userid);
        user.setName(name);
        user.setMobile(mobile);
        user.setDepartment(department);
        user.setOrder(order);
        user.setPosition(position);
        user.setGender(gender);
        user.setEmail(email);
        user.setIsLeaderInDept(isLeaderInDept);
        user.setAvatar(avatar);
        user.setThumbAvatar(thumbAvatar);
        user.setTelephone(telephone);
        user.setAlias(alias);
        user.setStatus(status);
        user.setQrCode(qrCode);
        user.setExternalPosition(externalPosition);
        user.setAddress(address);
        user.setOpenUserid(openUserid);
        user.setMainDepartment(mainDepartment);
        user.setCreateTime(new Date());
        user.save();

        return user.getId();
    }

    @Override
    public void update(
            Long id,
            String corpid,
            String userid,
            String name,
            String mobile,
            String department,
            String order,
            String position,
            String gender,
            String email,
            String isLeaderInDept,
            String avatar,
            String thumbAvatar,
            String telephone,
            String alias,
            String status,
            String qrCode,
            String externalPosition,
            String address,
            String openUserid,
            String mainDepartment
    ) {
        User user = User.dao.findById(id);
        user.setCorpid(corpid);
        user.setUserid(userid);
        user.setName(name);
        user.setMobile(mobile);
        user.setDepartment(department);
        user.setOrder(order);
        user.setPosition(position);
        user.setGender(gender);
        user.setEmail(email);
        user.setIsLeaderInDept(isLeaderInDept);
        user.setAvatar(avatar);
        user.setThumbAvatar(thumbAvatar);
        user.setTelephone(telephone);
        user.setAlias(alias);
        user.setStatus(status);
        user.setQrCode(qrCode);
        user.setExternalPosition(externalPosition);
        user.setAddress(address);
        user.setOpenUserid(openUserid);
        user.setMainDepartment(mainDepartment);
        user.setUpdateTime(new Date());
        user.update();

    }


}
