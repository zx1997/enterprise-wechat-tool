package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.Department;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-06 17:08
 */
public interface DepartmentDao {

    void save(
            Integer departId,
            String name,
            String nameEn,
            Integer parentid,
            Integer order,
            String corpid
    );

    Department findByDepartId(Integer departId,String corpid);

    void update(
            Long id,
            Integer departId,
            String name,
            String nameEn,
            Integer parentid,
            Integer order,
            String corpid
    );

    List<Department> list(String corpid,Integer parentid);

}
