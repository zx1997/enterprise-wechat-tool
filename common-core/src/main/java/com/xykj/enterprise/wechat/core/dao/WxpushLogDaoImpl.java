package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.WxpushLog;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author zhouxu
 * @create 2021-02-25 11:21
 */
@Repository
public class WxpushLogDaoImpl implements WxpushLogDao {

    @Override
    public void save(String msgSignatuer, String timestamp, String nonce, String data, String msg, String jsonMsg,
                     String msgType, String event, String changeType) {
        WxpushLog log = new WxpushLog();
        log.setMsgSignature(msgSignatuer);
        log.setTimestamp(timestamp);
        log.setNonce(nonce);
        log.setData(data);
        log.setMsg(msg);
        log.setJsonMsg(jsonMsg);
        log.setStatus(WxpushLog.STATUS_INIT);
        Date now = new Date();
        log.setCreateTime(now);
        log.setDayTime(TimeUtil.getTimeYYMMDD(now));
        log.setMsgType(msgType);
        log.setEvent(event);
        log.setChangeType(changeType);
        log.save();
    }
}
