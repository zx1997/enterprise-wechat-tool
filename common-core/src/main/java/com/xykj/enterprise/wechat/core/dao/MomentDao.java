package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.Moment;

/**
 * @Author george
 * @create 2021-05-07 19:47
 */
public interface MomentDao {

    Moment get(String corpid, String momentId);

    void save(
            String momentId,
            String corpid,
            String creator,
            String createTime,
            String createType,
            String visibleType

    );

    void update(
            Long id,
            String momentId,
            String corpid,
            String creator,
            String createTime,
            String createType,
            String visibleType

    );

}
