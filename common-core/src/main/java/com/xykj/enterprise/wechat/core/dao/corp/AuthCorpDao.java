package com.xykj.enterprise.wechat.core.dao.corp;

import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;

/**
 * @Author zhouxu
 * @create 2021-03-31 14:43
 */
public interface AuthCorpDao {

    String save(
            String corpid,
            String authCorpInfo,
            String authInfo,
            String authUserInfo,
            String dealerCorpInfo,
            String registerCodeInfo
    );

    void update(
            String corpid,
            String authCorpInfo,
            String authInfo,
            String authUserInfo,
            String dealerCorpInfo,
            String registerCodeInfo
    );

    AuthCorp getByCorpid(String corpid);



}
