package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcProject;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-13 19:48
 */
@Repository
public class PCProjectDaoImpl implements PCProjectDao {

    @Override
    public Page<PcProject> list(Integer page, Integer limit, Map<String, Object> params) {
        SqlParse sqlParse = new SqlParse(params);
        sqlParse.addSQL(" select * from pc_project ");
        sqlParse.addSQL(" where 1=1 ");
        sqlParse.addSQL(" and corpid = :corpid ");
        sqlParse.addSQL(" order by create_time desc ");
        return PcProject.dao.paginate(page, limit, sqlParse);
    }

    @Override
    public PcProject get(Integer id, String corpid) {
        String sql = "select * from pc_project where id = ? corpid = ? ";
        return PcProject.dao.findFirst(sql, id, corpid);
    }

    @Override
    public Integer save(
            String corpid,
            String projectName,
            String projectProcess,
            String projectManager,
            Integer signDay,
            BigDecimal projectAmount,
            BigDecimal salesExpense,
            Date planDevStartTime,
            Date customerReqEndTime,
            Date realStartTime,
            Date realEndTime,
            Date maintainStartTime,
            Integer freeMaintainDay,
            BigDecimal maintainFee,
            String creator
    ) {
        PcProject p = new PcProject();
        p.setCorpid(corpid);
        p.setProjectName(projectName);
        p.setProjectProcess(projectProcess);
        p.setProjectManager(projectManager);
        p.setSignDay(signDay);
        p.setProjectAmount(projectAmount);
        p.setSalesExpense(salesExpense);
        p.setPlanDevStartTime(planDevStartTime);
        p.setCustomerReqEndTime(customerReqEndTime);
        p.setRealStartTime(realStartTime);
        p.setRealEndTime(realEndTime);
        p.setMaintainStartTime(maintainStartTime);
        p.setFreeMaintainDay(freeMaintainDay);
        p.setMaintainFee(maintainFee);
        p.setCreator(creator);
        p.setCreateTime(new Date());
        p.save();
        return p.getId();
    }

    @Override
    public void update(
            Integer id,
            String corpid,
            String projectName,
            String projectProcess,
            String projectManager,
            Integer signDay,
            BigDecimal projectAmount,
            BigDecimal salesExpense,
            Date planDevStartTime,
            Date customerReqEndTime,
            Date realStartTime,
            Date realEndTime,
            Date maintainStartTime,
            Integer freeMaintainDay,
            BigDecimal maintainFee,
            String updator
    ) {
        PcProject p = PcProject.dao.findById(id);
        p.setCorpid(corpid);
        p.setProjectName(projectName);
        p.setProjectProcess(projectProcess);
        p.setProjectManager(projectManager);
        p.setSignDay(signDay);
        p.setProjectAmount(projectAmount);
        p.setSalesExpense(salesExpense);
        p.setPlanDevStartTime(planDevStartTime);
        p.setCustomerReqEndTime(customerReqEndTime);
        p.setRealStartTime(realStartTime);
        p.setRealEndTime(realEndTime);
        p.setMaintainStartTime(maintainStartTime);
        p.setFreeMaintainDay(freeMaintainDay);
        p.setMaintainFee(maintainFee);
        p.setUpdator(updator);
        p.setUpdateTime(new Date());
        p.save();


    }

    @Override
    public void delete(Integer id, String corpid) {
        PcProject p = PcProject.dao.findById(id);
        if (p != null) {
            p.delete();
        }
    }
}
