package com.xykj.enterprise.wechat.core.dao.corp;

import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author zhouxu
 * @create 2021-03-31 14:46
 */
@Repository
public class AuthCorpDaoImpl implements AuthCorpDao {
    @Override
    public String save(
            String corpid,
            String authCorpInfo,
            String authInfo,
            String authUserInfo,
            String dealerCorpInfo,
            String registerCodeInfo
    ) {
        AuthCorp authCorp = new AuthCorp();
        authCorp.setCorpid(corpid);
        authCorp.setAuthCorpInfo(authCorpInfo);
        authCorp.setAuthInfo(authInfo);
        authCorp.setAuthUserInfo(authUserInfo);
        authCorp.setDealerCorpInfo(dealerCorpInfo);
        authCorp.setRegisterCodeInfo(registerCodeInfo);
        Date now = new Date();
        authCorp.setCreateTime(now);
        authCorp.save();
        return authCorp.getCorpid();
    }

    @Override
    public void update(
            String corpid,
            String authCorpInfo,
            String authInfo,
            String authUserInfo,
            String dealerCorpInfo,
            String registerCodeInfo
    ) {
        AuthCorp authCorp = AuthCorp.dao.findById(corpid);
        authCorp.setCorpid(corpid);
        authCorp.setAuthCorpInfo(authCorpInfo);
        authCorp.setAuthInfo(authInfo);
        authCorp.setAuthUserInfo(authUserInfo);
        authCorp.setDealerCorpInfo(dealerCorpInfo);
        authCorp.setRegisterCodeInfo(registerCodeInfo);
        Date now = new Date();
        authCorp.setCreateTime(now);
        authCorp.update();
    }

    @Override
    public AuthCorp getByCorpid(String corpid) {
        return AuthCorp.dao.findById(corpid);
    }


}
