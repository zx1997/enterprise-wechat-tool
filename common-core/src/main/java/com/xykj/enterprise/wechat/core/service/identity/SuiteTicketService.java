package com.xykj.enterprise.wechat.core.service.identity;

import com.xykj.enterprise.wechat.bean.busi.vo.wxpush.SuiteTicketVo;
import com.xykj.enterprise.wechat.core.model.dodb.SuiteTicket;

/**
 * @Author zhouxu
 * @create 2021-03-29 21:45
 */
public interface SuiteTicketService {

    /**
     * 最新 suite_ticket
     * @return
     */
    SuiteTicket getLatest(String suiteId);

    Long create(SuiteTicketVo vo);
}
