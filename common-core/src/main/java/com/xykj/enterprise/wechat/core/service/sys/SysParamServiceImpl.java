package com.xykj.enterprise.wechat.core.service.sys;

import com.xykj.enterprise.wechat.core.dao.SysParamDao;
import com.xykj.enterprise.wechat.core.model.dodb.SysParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author zhouxu
 * @create 2021-04-01 16:20
 */
@Service
public class SysParamServiceImpl implements SysParamService {

    @Autowired
    private SysParamDao sysParamDao;

    @Override
    public SysParam getByCode(String code) {
        return sysParamDao.getByCode(code);
    }

}
