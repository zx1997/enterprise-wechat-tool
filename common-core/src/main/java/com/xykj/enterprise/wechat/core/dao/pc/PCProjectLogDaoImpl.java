package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcProjectLog;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-13 19:57
 */
@Repository
public class PCProjectLogDaoImpl implements PCProjectLogDao {

    @Override
    public Page<PcProjectLog> list(Integer page, Integer limit, Map<String, Object> params) {
        SqlParse sqlParse = new SqlParse(params);
        sqlParse.addSQL(" select * from pc_project_log ");
        sqlParse.addSQL(" where 1=1 ");
        sqlParse.addSQL(" and corpid = :corpid ");
        sqlParse.addSQL(" and project_id = :projectId ");
        sqlParse.addSQL(" order by create_time desc ");
        return PcProjectLog.dao.paginate(page, limit, sqlParse);
    }

    @Override
    public Long save(
            String corpid,
            Integer workDay,
            String worker,
            Integer projectId,
            String workContent,
            BigDecimal workHour,
            String creator
    ) {
        PcProjectLog log = new PcProjectLog();
        log.setCorpid(corpid);
        log.setWorkDay(workDay);
        log.setWorker(worker);
        log.setProjectId(projectId);
        log.setWorkContent(workContent);
        log.setWorkHour(workHour);
        log.setCreator(creator);
        log.setCreateTime(new Date());
        log.save();
        return log.getId();
    }

    @Override
    public void update(
            Long id,
            String corpid,
            Integer workDay,
            String worker,
            Integer projectId,
            String workContent,
            BigDecimal workHour,
            String updator
    ) {
        PcProjectLog log = PcProjectLog.dao.findById(id);
        log.setCorpid(corpid);
        log.setWorkDay(workDay);
        log.setWorker(worker);
        log.setProjectId(projectId);
        log.setWorkContent(workContent);
        log.setWorkHour(workHour);
        log.setUpdator(updator);
        log.setUpdateTime(new Date());
        log.update();
    }

    @Override
    public void delete(Long id, String corpid) {
        PcProjectLog log = PcProjectLog.dao.findById(id);
        if (log != null) {
            log.delete();
        }
    }
}
