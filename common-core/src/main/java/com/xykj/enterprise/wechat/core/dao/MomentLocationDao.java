package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.MomentLocation;

/**
 * @Author george
 * @create 2021-05-10 11:29
 */
public interface MomentLocationDao {

    MomentLocation get(String corpid,String momentId);

    void  save(
            String latitude,
            String longitude,
            String name,
            String corpid,
            String momentId

    );

    void delete(String momentId, String corpid);
}
