package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.SysParam;

/**
 * @Author zhouxu
 * @create 2021-04-01 16:16
 */
public interface SysParamDao {

    SysParam getByCode(String code);

}
