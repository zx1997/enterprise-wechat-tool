package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowUser;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-09 10:05
 */
public interface CustomerFollowUserDao {

    CustomerFollowUser get(String externalUserid, String corpid);

    CustomerFollowUser get(String externalUserid, String corpid,String userid);

    List<CustomerFollowUser> list(String externalUserid,String corpid);

    void delete(CustomerFollowUser followUser);

    int batchDelete(String externalUserid, String corpid);

    void save(
            String corpid,
            String externalUserid,
            Long customerId,
            String userid,
            String remark,
            String description,
            Long createtime,
            String remarkCorpName,
            String remarkMobile,
            String openUserid,
            String addWay,
            String state,
            String tags
    );

    void update(
            Long id,
            String corpid,
            String externalUserid,
            Long customerId,
            String userid,
            String remark,
            String description,
            Long createtime,
            String remarkCorpName,
            String remarkMobile,
            String openUserid,
            String addWay,
            String state,
            String tags
    );

}
