package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseMoment<M extends BaseMoment<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 朋友圈ID（企业微信）
	 */
	public void setMomentId(java.lang.String momentId) {
		set("moment_id", momentId);
	}
	
	/**
	 * 朋友圈ID（企业微信）
	 */
	public java.lang.String getMomentId() {
		return getStr("moment_id");
	}

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 朋友圈创建者userid
	 */
	public void setCreator(java.lang.String creator) {
		set("creator", creator);
	}
	
	/**
	 * 朋友圈创建者userid
	 */
	public java.lang.String getCreator() {
		return getStr("creator");
	}

	/**
	 * 发表时间
	 */
	public void setCreateTime(java.lang.String createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 发表时间
	 */
	public java.lang.String getCreateTime() {
		return getStr("create_time");
	}

	/**
	 * 朋友圈创建来源 0企业，1个人
	 */
	public void setCreateType(java.lang.String createType) {
		set("create_type", createType);
	}
	
	/**
	 * 朋友圈创建来源 0企业，1个人
	 */
	public java.lang.String getCreateType() {
		return getStr("create_type");
	}

	/**
	 * 可见范围类型； 0部分可见 1公开
	 */
	public void setVisibleType(java.lang.String visibleType) {
		set("visible_type", visibleType);
	}
	
	/**
	 * 可见范围类型； 0部分可见 1公开
	 */
	public java.lang.String getVisibleType() {
		return getStr("visible_type");
	}

	/**
	 * 文本
	 */
	public void setTextId(java.lang.Long textId) {
		set("text_id", textId);
	}
	
	/**
	 * 文本
	 */
	public java.lang.Long getTextId() {
		return getLong("text_id");
	}

	/**
	 * 图片
	 */
	public void setImageId(java.lang.Long imageId) {
		set("image_id", imageId);
	}
	
	/**
	 * 图片
	 */
	public java.lang.Long getImageId() {
		return getLong("image_id");
	}

	/**
	 * 视频
	 */
	public void setVideoId(java.lang.Long videoId) {
		set("video_id", videoId);
	}
	
	/**
	 * 视频
	 */
	public java.lang.Long getVideoId() {
		return getLong("video_id");
	}

	/**
	 * 链接
	 */
	public void setLinkId(java.lang.Long linkId) {
		set("link_id", linkId);
	}
	
	/**
	 * 链接
	 */
	public java.lang.Long getLinkId() {
		return getLong("link_id");
	}

	/**
	 * 地理位置
	 */
	public void setLocationId(java.lang.Long locationId) {
		set("location_id", locationId);
	}
	
	/**
	 * 地理位置
	 */
	public java.lang.Long getLocationId() {
		return getLong("location_id");
	}

}
