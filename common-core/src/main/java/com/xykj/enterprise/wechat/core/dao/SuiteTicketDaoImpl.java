package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.SuiteTicket;
import org.springframework.stereotype.Repository;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:10
 */
@Repository
public class SuiteTicketDaoImpl implements SuiteTicketDao {

    @Override
    public Long save(String suiteId, String infoType, Long timeStamp, String suiteTicket) {
        SuiteTicket ticket = new SuiteTicket();
        ticket.setSuiteId(suiteId);
        ticket.setInfoType(infoType);
        ticket.setTimeStamp(timeStamp);
        ticket.setSuitTicket(suiteTicket);
        ticket.save();
        return ticket.getTicketId();
    }

    @Override
    public SuiteTicket getLatest(String suiteId) {
        String sql = "select * from t_suite_ticket where suite_id = ?  order by time_stamp desc ";
        return SuiteTicket.dao.findFirst(sql, suiteId);
    }
}
