package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.AgentAdmin;

import java.util.List;

/**
 * @Author george
 * @create 2021-05-28 17:35
 */
public interface AgentAdminDao {

    List<AgentAdmin> list(String corpid, String suiteId, String agentId);

    void save(
            String corpid,
            String suiteId,
            String agentId,
            String userid,
            String openUserid,
            String authType
    );

}
