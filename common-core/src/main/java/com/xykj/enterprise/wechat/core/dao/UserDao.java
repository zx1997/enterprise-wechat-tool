package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.User;

public interface UserDao {


    User getById(String id);

    User get(String userid,String corpid);

    Long save(
            String corpid,
            String userid,
            String name,
            String mobile,
            String department,
            String order,
            String position,
            String gender,
            String email,
            String isLeaderInDept,
            String avatar,
            String thumbAvatar,
            String telephone,
            String alias,
            String status,
            String qrCode,
            String externalPosition,
            String address,
            String openUserid,
            String mainDepartment
    );

    void update(
            Long id,
            String corpid,
            String userid,
            String name,
            String mobile,
            String department,
            String order,
            String position,
            String gender,
            String email,
            String isLeaderInDept,
            String avatar,
            String thumbAvatar,
            String telephone,
            String alias,
            String status,
            String qrCode,
            String externalPosition,
            String address,
            String openUserid,
            String mainDepartment
    );

}
