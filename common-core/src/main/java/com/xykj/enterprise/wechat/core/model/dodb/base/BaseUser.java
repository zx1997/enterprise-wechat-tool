package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseUser<M extends BaseUser<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 员工userid
	 */
	public void setUserid(java.lang.String userid) {
		set("userid", userid);
	}
	
	/**
	 * 员工userid
	 */
	public java.lang.String getUserid() {
		return getStr("userid");
	}

	/**
	 * 成员名称；第三方不可获取，调用时返回userid以代替name；对于非第三方创建的成员，第三方通讯录应用也不可获取；第三方页面需要通过通讯录展示组件来展示名字
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 成员名称；第三方不可获取，调用时返回userid以代替name；对于非第三方创建的成员，第三方通讯录应用也不可获取；第三方页面需要通过通讯录展示组件来展示名字
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	public void setMobile(java.lang.String mobile) {
		set("mobile", mobile);
	}
	
	public java.lang.String getMobile() {
		return getStr("mobile");
	}

	/**
	 * 成员所属部门id列表，仅返回该应用有查看权限的部门id
	 */
	public void setDepartment(java.lang.String department) {
		set("department", department);
	}
	
	/**
	 * 成员所属部门id列表，仅返回该应用有查看权限的部门id
	 */
	public java.lang.String getDepartment() {
		return getStr("department");
	}

	/**
	 * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0, 2^32)
	 */
	public void setOrder(java.lang.String order) {
		set("order", order);
	}
	
	/**
	 * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0, 2^32)
	 */
	public java.lang.String getOrder() {
		return getStr("order");
	}

	/**
	 * 职务信息；第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setPosition(java.lang.String position) {
		set("position", position);
	}
	
	/**
	 * 职务信息；第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getPosition() {
		return getStr("position");
	}

	/**
	 * 性别。0表示未定义，1表示男性，2表示女性
	 */
	public void setGender(java.lang.String gender) {
		set("gender", gender);
	}
	
	/**
	 * 性别。0表示未定义，1表示男性，2表示女性
	 */
	public java.lang.String getGender() {
		return getStr("gender");
	}

	/**
	 * 邮箱，第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setEmail(java.lang.String email) {
		set("email", email);
	}
	
	/**
	 * 邮箱，第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getEmail() {
		return getStr("email");
	}

	/**
	 * 表示在所在的部门内是否为上级。0-否；1-是。是一个列表，数量必须与department一致。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setIsLeaderInDept(java.lang.String isLeaderInDept) {
		set("is_leader_in_dept", isLeaderInDept);
	}
	
	/**
	 * 表示在所在的部门内是否为上级。0-否；1-是。是一个列表，数量必须与department一致。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getIsLeaderInDept() {
		return getStr("is_leader_in_dept");
	}

	/**
	 * 头像url。 第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setAvatar(java.lang.String avatar) {
		set("avatar", avatar);
	}
	
	/**
	 * 头像url。 第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getAvatar() {
		return getStr("avatar");
	}

	/**
	 * 头像缩略图url。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setThumbAvatar(java.lang.String thumbAvatar) {
		set("thumb_avatar", thumbAvatar);
	}
	
	/**
	 * 头像缩略图url。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getThumbAvatar() {
		return getStr("thumb_avatar");
	}

	/**
	 * 座机。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setTelephone(java.lang.String telephone) {
		set("telephone", telephone);
	}
	
	/**
	 * 座机。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getTelephone() {
		return getStr("telephone");
	}

	/**
	 * 别名；第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setAlias(java.lang.String alias) {
		set("alias", alias);
	}
	
	/**
	 * 别名；第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getAlias() {
		return getStr("alias");
	}

	/**
	 * 	激活状态: 1=已激活，2=已禁用，4=未激活，5=退出企业。
	 */
	public void setStatus(java.lang.String status) {
		set("status", status);
	}
	
	/**
	 * 	激活状态: 1=已激活，2=已禁用，4=未激活，5=退出企业。
	 */
	public java.lang.String getStatus() {
		return getStr("status");
	}

	/**
	 * 员工个人二维码，扫描可添加为外部联系人(注意返回的是一个url，可在浏览器上打开该url以展示二维码)；第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setQrCode(java.lang.String qrCode) {
		set("qr_code", qrCode);
	}
	
	/**
	 * 员工个人二维码，扫描可添加为外部联系人(注意返回的是一个url，可在浏览器上打开该url以展示二维码)；第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getQrCode() {
		return getStr("qr_code");
	}

	/**
	 * 对外职务，如果设置了该值，则以此作为对外展示的职务，否则以position来展示。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setExternalPosition(java.lang.String externalPosition) {
		set("external_position", externalPosition);
	}
	
	/**
	 * 对外职务，如果设置了该值，则以此作为对外展示的职务，否则以position来展示。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getExternalPosition() {
		return getStr("external_position");
	}

	/**
	 * 地址。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public void setAddress(java.lang.String address) {
		set("address", address);
	}
	
	/**
	 * 地址。第三方仅通讯录应用可获取；对于非第三方创建的成员，第三方通讯录应用也不可获取
	 */
	public java.lang.String getAddress() {
		return getStr("address");
	}

	/**
	 * 全局唯一。对于同一个服务商，不同应用获取到企业内同一个成员的open_userid是相同的，最多64个字节。仅第三方应用可获取
	 */
	public void setOpenUserid(java.lang.String openUserid) {
		set("open_userid", openUserid);
	}
	
	/**
	 * 全局唯一。对于同一个服务商，不同应用获取到企业内同一个成员的open_userid是相同的，最多64个字节。仅第三方应用可获取
	 */
	public java.lang.String getOpenUserid() {
		return getStr("open_userid");
	}

	/**
	 * 主部门
	 */
	public void setMainDepartment(java.lang.String mainDepartment) {
		set("main_department", mainDepartment);
	}
	
	/**
	 * 主部门
	 */
	public java.lang.String getMainDepartment() {
		return getStr("main_department");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
