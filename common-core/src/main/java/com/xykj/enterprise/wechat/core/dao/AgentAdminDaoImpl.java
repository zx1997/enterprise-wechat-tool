package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.AgentAdmin;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author george
 * @create 2021-05-28 17:43
 */
@Repository
public class AgentAdminDaoImpl implements AgentAdminDao {
    @Override
    public List<AgentAdmin> list(String corpid, String suiteId, String agentId) {
        String sql = "select * from t_agent_admin where corpid = ? and suite_id = ? and agent_id = ? ";
        return AgentAdmin.dao.find(sql, corpid, suiteId, agentId);
    }

    @Override
    public void save(
            String corpid,
            String suiteId,
            String agentId,
            String userid,
            String openUserid,
            String authType
    ) {
        AgentAdmin admin = new AgentAdmin();
        admin.setCorpid(corpid);
        admin.setSuiteId(suiteId);
        admin.setAgentid(agentId);
        admin.setUserid(userid);
        admin.setOpenUserid(openUserid);
        admin.setAuthType(authType);
        admin.save();

    }
}
