package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomerFollow<M extends BaseCustomerFollow<M>> extends Model<M> implements IBean {

	/**
	 * 跟进主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 跟进主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 客户ID
	 */
	public void setCustomerId(java.lang.Long customerId) {
		set("customer_id", customerId);
	}
	
	/**
	 * 客户ID
	 */
	public java.lang.Long getCustomerId() {
		return getLong("customer_id");
	}

	/**
	 * 跟进人ID
	 */
	public void setFollowUserId(java.lang.Long followUserId) {
		set("follow_user_id", followUserId);
	}
	
	/**
	 * 跟进人ID
	 */
	public java.lang.Long getFollowUserId() {
		return getLong("follow_user_id");
	}

	/**
	 * 跟进阶段，对应t_customer_follow_stage中的code
	 */
	public void setStage(java.lang.String stage) {
		set("stage", stage);
	}
	
	/**
	 * 跟进阶段，对应t_customer_follow_stage中的code
	 */
	public java.lang.String getStage() {
		return getStr("stage");
	}

	/**
	 * 跟进详情、内容
	 */
	public void setContent(java.lang.String content) {
		set("content", content);
	}
	
	/**
	 * 跟进详情、内容
	 */
	public java.lang.String getContent() {
		return getStr("content");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 备注信息
	 */
	public void setMark(java.lang.String mark) {
		set("mark", mark);
	}
	
	/**
	 * 备注信息
	 */
	public java.lang.String getMark() {
		return getStr("mark");
	}

}
