package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.MomentVideo;

/**
 * @Author george
 * @create 2021-05-10 11:34
 */
public interface MomentVideoDao {

    MomentVideo get(String corpid,String momentId);

    void save(
            String media_id,
            String thumb_media_id,
            String corpid,
            String momentId
    );

    void delete(String momentId, String corpid);
}
