package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.config.DbConfig;
import com.xykj.enterprise.wechat.core.model.dodb.MomentText;
import com.ydn.dbframe.plugin.activerecord.Db;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author george
 * @create 2021-05-10 11:38
 */
@Repository
public class MomentTextDaoImpl implements MomentTextDao {
    @Override
    public MomentText get(String corpid, String momentId) {
        String sql = " select * from t_moment_text where corpid = ? and moment_id = ? ";
        return MomentText.dao.findFirst(sql, corpid, momentId);
    }

    @Override
    public void save(String content, String corpid, String momentId) {
        MomentText text = new MomentText();
        text.setContent(content);
        text.setCorpid(corpid);
        text.setMomentId(momentId);
        text.setCreateTime(new Date());
        text.save();
    }

    @Override
    public void delete(String momentId, String corpid) {
        String sql = " delete from t_moment_text where corpid = ? and moment_id = ? limit 1 ";
        Db.use(DbConfig.DATASOURCE).update(sql, momentId, corpid);
    }
}
