package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseCustomer<M extends BaseCustomer<M>> extends Model<M> implements IBean {

	/**
	 * id
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * id
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 外部联系人id，对应企业微信
	 */
	public void setExternalUserid(java.lang.String externalUserid) {
		set("external_userid", externalUserid);
	}
	
	/**
	 * 外部联系人id，对应企业微信
	 */
	public java.lang.String getExternalUserid() {
		return getStr("external_userid");
	}

	/**
	 * 外部联系人的名称
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 外部联系人的名称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}

	/**
	 * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
	 */
	public void setPosition(java.lang.String position) {
		set("position", position);
	}
	
	/**
	 * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
	 */
	public java.lang.String getPosition() {
		return getStr("position");
	}

	/**
	 * 外部联系人头像，第三方不可获取
	 */
	public void setAvatar(java.lang.String avatar) {
		set("avatar", avatar);
	}
	
	/**
	 * 外部联系人头像，第三方不可获取
	 */
	public java.lang.String getAvatar() {
		return getStr("avatar");
	}

	/**
	 * 外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户
	 */
	public void setType(java.lang.String type) {
		set("type", type);
	}
	
	/**
	 * 外部联系人的类型，1表示该外部联系人是微信用户，2表示该外部联系人是企业微信用户
	 */
	public java.lang.String getType() {
		return getStr("type");
	}

	/**
	 * 外部联系人性别 0-未知 1-男性 2-女性
	 */
	public void setGender(java.lang.String gender) {
		set("gender", gender);
	}
	
	/**
	 * 外部联系人性别 0-未知 1-男性 2-女性
	 */
	public java.lang.String getGender() {
		return getStr("gender");
	}

	/**
	 * 	外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业或第三方服务商绑定了微信开发者ID有此字段
	 */
	public void setUnionid(java.lang.String unionid) {
		set("unionid", unionid);
	}
	
	/**
	 * 	外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业或第三方服务商绑定了微信开发者ID有此字段
	 */
	public java.lang.String getUnionid() {
		return getStr("unionid");
	}

	/**
	 * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
	 */
	public void setCorpName(java.lang.String corpName) {
		set("corp_name", corpName);
	}
	
	/**
	 * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
	 */
	public java.lang.String getCorpName() {
		return getStr("corp_name");
	}

	/**
	 * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
	 */
	public void setCorpFullName(java.lang.String corpFullName) {
		set("corp_full_name", corpFullName);
	}
	
	/**
	 * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
	 */
	public java.lang.String getCorpFullName() {
		return getStr("corp_full_name");
	}

	/**
	 * 企业客户标签，多个标签以,分割
	 */
	public void setTag(java.lang.String tag) {
		set("tag", tag);
	}
	
	/**
	 * 企业客户标签，多个标签以,分割
	 */
	public java.lang.String getTag() {
		return getStr("tag");
	}

	/**
	 * 客户手机号
	 */
	public void setMobile(java.lang.String mobile) {
		set("mobile", mobile);
	}
	
	/**
	 * 客户手机号
	 */
	public java.lang.String getMobile() {
		return getStr("mobile");
	}

	/**
	 * 客户备注
	 */
	public void setMark(java.lang.String mark) {
		set("mark", mark);
	}
	
	/**
	 * 客户备注
	 */
	public java.lang.String getMark() {
		return getStr("mark");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
