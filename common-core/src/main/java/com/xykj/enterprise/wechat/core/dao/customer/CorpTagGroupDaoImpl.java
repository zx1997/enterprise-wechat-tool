package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.CorpTagGroup;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-12 10:26
 */
@Repository
public class CorpTagGroupDaoImpl implements CorpTagGroupDao {
    @Override
    public CorpTagGroup get(String corpid, String groupId) {
        String sql = " select * from t_corp_tag_group where corpid = ? and group_id = ? ";
        return CorpTagGroup.dao.findFirst(sql, corpid, groupId);
    }

    @Override
    public List<CorpTagGroup> findAll(String corpid) {
        String sql = "select * from t_corp_tag_group where corpid = ? ";
        return CorpTagGroup.dao.find(sql, corpid);
    }

    @Override
    public CorpTagGroup save(
            String corpid,
            String groupId,
            String name,
            Integer order,
            String deleted
    ) {
        CorpTagGroup group = new CorpTagGroup();
        group.setCorpid(corpid);
        group.setGroupId(groupId);
        group.setName(name);
        group.setOrder(order);
        group.setCreateTime(new Date());
        group.setDeleted(deleted);
        group.save();
        return group;
    }

    @Override
    public void update(
            Long id,
            String corpid,
            String groupId,
            String name,
            Integer order,
            String deleted
    ) {
        CorpTagGroup group = CorpTagGroup.dao.findById(id);
        group.setCorpid(corpid);
        group.setGroupId(groupId);
        group.setName(name);
        group.setOrder(order);
        group.setUpdateTime(new Date());
        group.setDeleted(deleted);
        group.update();

    }
}
