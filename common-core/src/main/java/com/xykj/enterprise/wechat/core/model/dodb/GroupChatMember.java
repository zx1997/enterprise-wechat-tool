package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseGroupChatMember;

/**
 */
@SuppressWarnings("serial")
public class GroupChatMember extends BaseGroupChatMember<GroupChatMember> {
	public static final GroupChatMember dao = new GroupChatMember().dao();

}
