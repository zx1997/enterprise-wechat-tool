package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.Moment;
import org.springframework.stereotype.Repository;

/**
 * @Author george
 * @create 2021-05-07 19:55
 */
@Repository
public class MomentDaoImpl implements MomentDao {

    @Override
    public Moment get(String corpid, String momentId) {
        String sql = " select * from t_moment where moment_id = ? and corpid = ? ";
        return Moment.dao.findFirst(sql, momentId, corpid);
    }

    @Override
    public void save(String momentId, String corpid, String creator, String createTime, String createType, String visibleType) {
        Moment moment = new Moment();
        moment.setMomentId(momentId);
        moment.setCorpid(corpid);
        moment.setCreator(creator);
        moment.setCreateTime(createTime);
        moment.setCreateType(createType);
        moment.setVisibleType(visibleType);
        moment.save();
    }

    @Override
    public void update(Long id, String momentId, String corpid, String creator, String createTime, String createType, String visibleType) {
        Moment moment = Moment.dao.findById(id);
        moment.setMomentId(momentId);
        moment.setCorpid(corpid);
        moment.setCreator(creator);
        moment.setCreateTime(createTime);
        moment.setCreateType(createType);
        moment.setVisibleType(visibleType);
        moment.update();
    }
}
