package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.AgentInfo;

/**
 * @Author george
 * @create 2021-05-28 17:36
 */
public interface AgentInfoDao {

    AgentInfo get(String corpid, String suiteId);

    void save(
            String corpid,
            String suiteId,
            String agentId,
            String name,
            String roundLogoUrl,
            String squareLogoUrl,
            String appid,
            String privilege,
            String shareFrom

    );

}
