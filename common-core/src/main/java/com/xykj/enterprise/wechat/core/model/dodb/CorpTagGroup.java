package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseCorpTagGroup;

/**
 */
@SuppressWarnings("serial")
public class CorpTagGroup extends BaseCorpTagGroup<CorpTagGroup> {
	public static final CorpTagGroup dao = new CorpTagGroup().dao();

}
