package com.xykj.enterprise.wechat.core.convert;

/**
 * @author Feng Chen
 */
public interface Converter<T, R>  {

    R convert(T t);

}
