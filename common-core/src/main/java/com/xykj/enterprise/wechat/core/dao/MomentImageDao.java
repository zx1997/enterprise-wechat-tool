package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.MomentImage;

/**
 * @Author george
 * @create 2021-05-10 11:22
 */
public interface MomentImageDao {

    MomentImage get(String momentId, String corpid);

    void save(
            String mediaId,
            String corpid,
            String momentId
    );

    void delete(String momentId, String corpid);
}
