package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcProjectLog;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-12 14:50
 */
public interface PCProjectLogDao {

    Page<PcProjectLog> list(Integer page, Integer limit, Map<String,Object> params);

    Long save(
            String corpid,
            Integer workDay,
            String worker,
            Integer projectId,
            String workContent,
            BigDecimal workHour,
            String creator
    );

    void update(
            Long id,
            String corpid,
            Integer workDay,
            String worker,
            Integer projectId,
            String workContent,
            BigDecimal workHour,
            String updator
    );

    void delete(Long id,String corpid);

}
