package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.SuiteInfo;

/**
 * @Author george
 * @create 2021-05-28 15:24
 */
public interface SuiteInfoDao {

    SuiteInfo get(String suiteId);


}
