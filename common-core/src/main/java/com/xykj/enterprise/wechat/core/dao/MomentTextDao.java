package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.MomentText;

/**
 * @Author george
 * @create 2021-05-10 11:32
 */
public interface MomentTextDao {

    MomentText get(String corpid,String momentId);

    void save(
            String content,
            String corpid,
            String momentId
    );

    void delete(String momentId,String corpid);
}
