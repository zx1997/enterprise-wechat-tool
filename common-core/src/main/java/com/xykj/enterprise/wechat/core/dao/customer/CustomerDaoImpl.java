package com.xykj.enterprise.wechat.core.dao.customer;

import com.xykj.enterprise.wechat.core.model.dodb.Customer;
import com.ydn.dbframe.plugin.activerecord.Db;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public class CustomerDaoImpl implements CustomerDao {

    @Override
    public Customer findById(Long id) {
        return Customer.dao.findById(id);
    }

    @Override
    public Customer findByExternalUserid(String externalUserid, String corpid) {
        String sql = "select * from t_customer where external_userid = ?  and corpid = ?";
        return Customer.dao.findFirst(sql, externalUserid, corpid);
    }

    @Override
    public Customer get(String corpId, String externalUserid) {
        String sql = " select * from t_customer where corpid = ? and external_userid = ? ";
        return Customer.dao.findFirst(sql, corpId, externalUserid);
    }

    @Override
    public Customer save(
            String corpid,
            String externalUserid,
            String name,
            String position,
            String avatar,
            String type,
            String gender,
            String unionid,
            String corpName,
            String corpFullName,
            String tag,
            String mobile,
            String mark
    ) {
        Customer c = new Customer();
        c.setCorpid(corpid);
        c.setExternalUserid(externalUserid);
        c.setName(name);
        c.setPosition(position);
        c.setAvatar(avatar);
        c.setType(type);
        c.setGender(gender);
        c.setUnionid(unionid);
        c.setCorpName(corpName);
        c.setCorpFullName(corpFullName);
        c.setTag(tag);
        c.setMobile(mobile);
        c.setMark(mark);
        c.setCreateTime(new Date());
        c.save();
        return c;
    }

    @Override
    public Customer update(
            Long id,
            String corpid,
            String externalUserid,
            String name,
            String position,
            String avatar,
            String type,
            String gender,
            String unionid,
            String corpName,
            String corpFullName,
            String tag,
            String mobile,
            String mark
    ) {
        Customer c = Customer.dao.findById(id);
        c.setCorpid(corpid);
        c.setExternalUserid(externalUserid);
        c.setName(name);
        c.setPosition(position);
        c.setAvatar(avatar);
        c.setType(type);
        c.setGender(gender);
        c.setUnionid(unionid);
        c.setCorpName(corpName);
        c.setCorpFullName(corpFullName);
        c.setTag(tag);
        c.setMobile(mobile);
        c.setMark(mark);
        c.setUpdateTime(new Date());
        c.update();
        return c;
    }

    @Override
    public int delete(String corpid, String externalUserid) {
        String sql = " delete from t_customer where external_userid = ? and corpid = ? ";
        return Db.update(sql, externalUserid, corpid);
    }
}
