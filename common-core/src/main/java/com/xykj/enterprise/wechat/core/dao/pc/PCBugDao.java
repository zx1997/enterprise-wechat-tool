package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcBug;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.Map;

/**
 * @Author george
 * @create 2021-05-27 14:18
 */
public interface PCBugDao {


    PcBug get(Long id, String corpid);

    Page<PcBug> list(Integer page, Integer limit, Map<String, Object> param);

    void save(
            String corpid,
            Long projectId,
            String proposer,
            String bugName,
            String bugDesc,
            String attachment,
            String handler,
            String bugSource

    );

    void updateStatus(PcBug bug,String status);

    void update(
            Long id,
            String corpid,
            Long projectId,
            String proposer,
            String bugName,
            String bugDesc,
            String attachment,
            String handler,
            String bugSource
    );

    void delete(Long id, String corpid);

}
