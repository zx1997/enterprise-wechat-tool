package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseDepartment<M extends BaseDepartment<M>> extends Model<M> implements IBean {

	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	public java.lang.Long getId() {
		return getLong("id");
	}

	public void setDepartId(java.lang.Integer departId) {
		set("depart_id", departId);
	}
	
	public java.lang.Integer getDepartId() {
		return getInt("depart_id");
	}

	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	public java.lang.String getName() {
		return getStr("name");
	}

	public void setNameEn(java.lang.String nameEn) {
		set("name_en", nameEn);
	}
	
	public java.lang.String getNameEn() {
		return getStr("name_en");
	}

	public void setParentid(java.lang.Integer parentid) {
		set("parentid", parentid);
	}
	
	public java.lang.Integer getParentid() {
		return getInt("parentid");
	}

	public void setOrder(java.lang.Integer order) {
		set("order", order);
	}
	
	public java.lang.Integer getOrder() {
		return getInt("order");
	}

	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

}
