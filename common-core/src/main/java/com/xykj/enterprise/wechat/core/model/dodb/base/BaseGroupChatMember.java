package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseGroupChatMember<M extends BaseGroupChatMember<M>> extends Model<M> implements IBean {

	/**
	 * 主键ID
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键ID
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 客户群ID
	 */
	public void setChatId(java.lang.String chatId) {
		set("chat_id", chatId);
	}
	
	/**
	 * 客户群ID
	 */
	public java.lang.String getChatId() {
		return getStr("chat_id");
	}

	/**
	 * 企业ID
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业ID
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

	/**
	 * 群成员id
	 */
	public void setUserid(java.lang.String userid) {
		set("userid", userid);
	}
	
	/**
	 * 群成员id
	 */
	public java.lang.String getUserid() {
		return getStr("userid");
	}

	/**
	 * 成员类型1 - 企业成员 2 - 外部联系人
	 */
	public void setType(java.lang.String type) {
		set("type", type);
	}
	
	/**
	 * 成员类型1 - 企业成员 2 - 外部联系人
	 */
	public java.lang.String getType() {
		return getStr("type");
	}

	/**
	 * 外部联系人在微信开放平台的唯一身份标识（微信unionid）
	 */
	public void setUnionid(java.lang.String unionid) {
		set("unionid", unionid);
	}
	
	/**
	 * 外部联系人在微信开放平台的唯一身份标识（微信unionid）
	 */
	public java.lang.String getUnionid() {
		return getStr("unionid");
	}

	/**
	 * 入群方式。
1 - 由成员邀请入群（直接邀请入群）
2 - 由成员邀请入群（通过邀请链接入群）
3 - 通过扫描群二维码入群
	 */
	public void setJoinScene(java.lang.String joinScene) {
		set("join_scene", joinScene);
	}
	
	/**
	 * 入群方式。
1 - 由成员邀请入群（直接邀请入群）
2 - 由成员邀请入群（通过邀请链接入群）
3 - 通过扫描群二维码入群
	 */
	public java.lang.String getJoinScene() {
		return getStr("join_scene");
	}

	/**
	 * 入群时间
	 */
	public void setJoinTime(java.lang.String joinTime) {
		set("join_time", joinTime);
	}
	
	/**
	 * 入群时间
	 */
	public java.lang.String getJoinTime() {
		return getStr("join_time");
	}

	/**
	 * 邀请者
	 */
	public void setInvitor(java.lang.String invitor) {
		set("invitor", invitor);
	}
	
	/**
	 * 邀请者
	 */
	public java.lang.String getInvitor() {
		return getStr("invitor");
	}

	/**
	 * 加入时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 加入时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

}
