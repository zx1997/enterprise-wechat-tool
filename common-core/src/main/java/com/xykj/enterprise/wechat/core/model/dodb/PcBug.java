package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BasePcBug;

/**
 */
@SuppressWarnings("serial")
public class PcBug extends BasePcBug<PcBug> {
	public static final PcBug dao = new PcBug().dao();

	//状态（1-已解决 0-未解决）
	public static final String STATUS_UNDO = "0";
	public static final String STATUS_SUCC = "1";

}
