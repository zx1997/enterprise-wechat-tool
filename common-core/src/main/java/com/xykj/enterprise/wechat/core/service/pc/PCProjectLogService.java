package com.xykj.enterprise.wechat.core.service.pc;

import com.xykj.enterprise.wechat.bean.wap.pc.ProjectLogVo;
import com.xykj.enterprise.wechat.core.model.dodb.PcProjectLog;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.util.Map;

/**
 * @Author george
 * @create 2021-05-12 15:38
 */
public interface PCProjectLogService {

    Page<PcProjectLog> list(Integer page, Integer limit, Map<String,Object> param);

    void save(ProjectLogVo vo, String corpid, String userId);

    void update(ProjectLogVo vo, String corpid, String userId);

    void delete(Long id, String corpid, String userId);

}
