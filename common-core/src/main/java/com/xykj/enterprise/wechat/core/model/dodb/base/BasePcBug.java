package com.xykj.enterprise.wechat.core.model.dodb.base;

import com.ydn.dbframe.plugin.activerecord.Model;
import com.ydn.dbframe.plugin.activerecord.IBean;

/**
 *  do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BasePcBug<M extends BasePcBug<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}

	/**
	 * 项目标识
	 */
	public void setProjectId(java.lang.Long projectId) {
		set("project_id", projectId);
	}
	
	/**
	 * 项目标识
	 */
	public java.lang.Long getProjectId() {
		return getLong("project_id");
	}

	/**
	 * bug提出方
	 */
	public void setProposer(java.lang.String proposer) {
		set("proposer", proposer);
	}
	
	/**
	 * bug提出方
	 */
	public java.lang.String getProposer() {
		return getStr("proposer");
	}

	/**
	 * bug名称
	 */
	public void setBugName(java.lang.String bugName) {
		set("bug_name", bugName);
	}
	
	/**
	 * bug名称
	 */
	public java.lang.String getBugName() {
		return getStr("bug_name");
	}

	/**
	 * bug描述
	 */
	public void setBugDesc(java.lang.String bugDesc) {
		set("bug_desc", bugDesc);
	}
	
	/**
	 * bug描述
	 */
	public java.lang.String getBugDesc() {
		return getStr("bug_desc");
	}

	/**
	 * 附件
	 */
	public void setAttachment(java.lang.String attachment) {
		set("attachment", attachment);
	}
	
	/**
	 * 附件
	 */
	public java.lang.String getAttachment() {
		return getStr("attachment");
	}

	/**
	 * 处理人标识
	 */
	public void setHandler(java.lang.String handler) {
		set("handler", handler);
	}
	
	/**
	 * 处理人标识
	 */
	public java.lang.String getHandler() {
		return getStr("handler");
	}

	/**
	 * 状态（1-已解决 0-未解决）
	 */
	public void setBugStatus(java.lang.String bugStatus) {
		set("bug_status", bugStatus);
	}
	
	/**
	 * 状态（1-已解决 0-未解决）
	 */
	public java.lang.String getBugStatus() {
		return getStr("bug_status");
	}

	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}

	/**
	 * 修改时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	/**
	 * 修改时间
	 */
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}

	/**
	 * bug来源
	 */
	public void setBugSource(java.lang.String bugSource) {
		set("bug_source", bugSource);
	}
	
	/**
	 * bug来源
	 */
	public java.lang.String getBugSource() {
		return getStr("bug_source");
	}

	/**
	 * 企业标识
	 */
	public void setCorpid(java.lang.String corpid) {
		set("corpid", corpid);
	}
	
	/**
	 * 企业标识
	 */
	public java.lang.String getCorpid() {
		return getStr("corpid");
	}

}
