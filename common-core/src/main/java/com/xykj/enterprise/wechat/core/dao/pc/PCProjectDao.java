package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcProject;
import com.ydn.dbframe.plugin.activerecord.Page;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-12 14:34
 */
public interface PCProjectDao {

    Page<PcProject> list(Integer page, Integer limit, Map<String,Object> params);

    PcProject get(Integer id, String corpid);

    Integer save(
            String corpid,
            String projectName,
            String projectProcess,
            String projectManager,
            Integer signDay,
            BigDecimal projectAmount,
            BigDecimal salesExpense,
            Date planDevStartTime,
            Date customerReqEndTime,
            Date realStartTime,
            Date realEndTime,
            Date maintainStartTime,
            Integer freeMaintainDay,
            BigDecimal maintainFee,
            String creator
    );

    void update(
            Integer id,
            String corpid,
            String projectName,
            String projectProcess,
            String projectManager,
            Integer signDay,
            BigDecimal projectAmount,
            BigDecimal salesExpense,
            Date planDevStartTime,
            Date customerReqEndTime,
            Date realStartTime,
            Date realEndTime,
            Date maintainStartTime,
            Integer freeMaintainDay,
            BigDecimal maintainFee,
            String updator
    );

    void delete(Integer id, String corpid);

}
