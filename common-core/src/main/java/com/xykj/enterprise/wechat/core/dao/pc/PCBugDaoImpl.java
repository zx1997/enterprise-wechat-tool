package com.xykj.enterprise.wechat.core.dao.pc;

import com.xykj.enterprise.wechat.core.model.dodb.PcBug;
import com.ydn.dbframe.plugin.activerecord.Page;
import com.ydn.dbframe.plugin.activerecord.parse.SqlParse;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Map;

/**
 * @Author george
 * @create 2021-05-27 14:45
 */
@Repository
public class PCBugDaoImpl implements PCBugDao {

    @Override
    public Page<PcBug> list(Integer page, Integer limit, Map<String, Object> params) {
        SqlParse sqlParse = new SqlParse(params);
        sqlParse.addSQL(" select * from pc_bug ");
        sqlParse.addSQL(" where 1=1 ");
        sqlParse.addSQL(" and corpid = :corpid ");
        sqlParse.addSQL(" and project_id =: projectId ");
        sqlParse.addSQL(" order by create_time desc ");
        return PcBug.dao.paginate(page, limit, sqlParse);
    }

    @Override
    public PcBug get(Long id, String corpid) {
        String sql = "select * from pc_bug where id = ? and corpid = ? ";
        return PcBug.dao.findFirst(sql, id, corpid);
    }

    @Override
    public void save(
            String corpid,
            Long projectId,
            String proposer,
            String bugName,
            String bugDesc,
            String attachment,
            String handler,
            String bugSource
    ) {
        PcBug bug = new PcBug();
        bug.setCorpid(corpid);
        bug.setProjectId(projectId);
        bug.setProposer(proposer);
        bug.setBugName(bugName);
        bug.setBugDesc(bugDesc);
        bug.setAttachment(attachment);
        bug.setHandler(handler);
        bug.setBugSource(bugSource);

        bug.setBugStatus(PcBug.STATUS_UNDO);
        bug.setCreateTime(new Date());
        bug.save();

    }

    @Override
    public void updateStatus(PcBug bug, String status) {
        bug.setBugStatus(status);
        bug.setUpdateTime(new Date());
        bug.update();
    }

    @Override
    public void update(
            Long id,
            String corpid,
            Long projectId,
            String proposer,
            String bugName,
            String bugDesc,
            String attachment,
            String handler,
            String bugSource
    ) {
        PcBug bug = PcBug.dao.findById(id);
        bug.setCorpid(corpid);
        bug.setProjectId(projectId);
        bug.setProposer(proposer);
        bug.setBugName(bugName);
        bug.setBugDesc(bugDesc);
        bug.setAttachment(attachment);
        bug.setHandler(handler);
        bug.setBugSource(bugSource);

        bug.setUpdateTime(new Date());
        bug.update();
    }

    @Override
    public void delete(Long id, String corpid) {
        PcBug bug = PcBug.dao.findById(id);
        if (bug != null) {
            bug.delete();
        }

    }
}
