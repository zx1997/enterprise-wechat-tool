package com.xykj.enterprise.wechat.core.dao;

import com.xykj.enterprise.wechat.core.model.dodb.CorpSuite;
import com.xykj.enterprise.wechat.util.time.TimeUtil;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * @Author george
 * @create 2021-04-28 15:21
 */
@Repository
public class CorpSuiteDaoImpl implements CorpSuiteDao {
    @Override
    public CorpSuite get(String corpid, String suiteId) {
        String sql = " select * from t_corp_suite where corpid = ? and suite_id = ? ";
        return CorpSuite.dao.findFirst(sql, corpid, suiteId);
    }

    @Override
    public void save(String corpid, String suiteId, String permanentCode, String accessToken, Integer expireTime) {
        CorpSuite param = new CorpSuite();
        param.setCorpid(corpid);
        param.setSuiteId(suiteId);
        param.setPermanentCode(permanentCode);
        param.setAccessToken(accessToken);
        Date now = new Date();
        param.setTokenExpTime(TimeUtil.addTime(now, expireTime));
        param.setTokenResetTime(now);
        param.save();

    }

    @Override
    public void update(String corpid, String suiteId, String permanentCode, String accessToken, Integer expireTime) {
        CorpSuite param = get(corpid, suiteId);
        if (param != null) {
            param.setPermanentCode(permanentCode);
            param.setAccessToken(accessToken);
            Date now = new Date();
            param.setTokenExpTime(TimeUtil.addTime(now, expireTime));
            param.setTokenResetTime(now);
            param.update();
        }

    }

    @Override
    public CorpSuite resetToken(String corpid, String suiteId, String accessToken, Integer expireTime) {
        CorpSuite param = get(corpid, suiteId);
        if (param != null) {
            param.setAccessToken(accessToken);
            Date now = new Date();
            param.setTokenExpTime(TimeUtil.addTime(now, expireTime));
            param.setTokenResetTime(now);
            param.update();
        }
        return param;
    }


}
