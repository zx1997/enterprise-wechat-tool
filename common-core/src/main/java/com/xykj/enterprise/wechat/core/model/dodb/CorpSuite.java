package com.xykj.enterprise.wechat.core.model.dodb;

import com.xykj.enterprise.wechat.core.model.dodb.base.BaseCorpSuite;

/**
 */
@SuppressWarnings("serial")
public class CorpSuite extends BaseCorpSuite<CorpSuite> {
	public static final CorpSuite dao = new CorpSuite().dao();

	public boolean isExpired() {
		return System.currentTimeMillis() > getTokenExpTime().getTime();
	}
}
