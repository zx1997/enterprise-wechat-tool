package com.xykj.enterprise.wechat.core.es;


import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

/**
 * @author Feng Chen
 */
public class EsClient {

    private TransportClient client;

    public EsClient(String cluster, String addrs) {

        System.setProperty("es.set.netty.runtime.available.processors", "false");

        client = new PreBuiltTransportClient(
                Settings.builder()
                        .put("cluster.name", cluster)
                        .build()
        );
        for (TransportAddress addr : TransportAddressSupport.getAddrs(addrs)) {
            client.addTransportAddress(addr);
        }
    }

    public TransportClient getClient() {
        return client;
    }

    /**
     * 释放资源
     */
    public void close() {
        if (client != null) {
            client.close();
            client = null;
        }
    }

}
