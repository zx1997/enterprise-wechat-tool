package com.xykj.enterprise.wechat.core.model;

import com.ydn.dbframe.kit.PathKit;
import com.ydn.dbframe.plugin.activerecord.DruidPlugin;
import com.ydn.dbframe.plugin.activerecord.dialect.MysqlDialect;
import com.ydn.dbframe.plugin.activerecord.generator.Generator;

import javax.sql.DataSource;
import java.io.File;

/**
 * 在数据库表有任何变动时，运行一下 main 方法，极速响应变化进行代码重构
 */
public class _Generator {
    static String jdbcUrl = "jdbc:mysql://119.8.236.1/enterprise_wechat_tool?characterEncoding=utf8&useSSL=false";
    static String user = "enterprise_wechat_tool";
    static String password = "enterprise_wechat_tool";

    private static String[] excludes = new String[]{

    };

    public static DataSource getDataSource() {
        DruidPlugin druidPlugin = new DruidPlugin(jdbcUrl, user, password);
        druidPlugin.start();
        return druidPlugin.getDataSource();
    }

    public static void main(String[] args) {

        // base model 所使用的包名
        String baseModelPackageName = "com.xykj.enterprise.wechat.core.model.dodb.base";

        String path = detectWebRootPath();

        // base model 文件保存路径，注意该路径在实际开发时要指向  src/main/java/....
        // 此处仅为 demo 演示，指向了 src/test/java/....
        String baseModelOutputDir = path + "/src/main/java/com/xykj/enterprise/wechat/core/model/dodb/base";

        // model 所使用的包名 (MappingKit 默认使用的包名)
        String modelPackageName = "com.xykj.enterprise.wechat.core.model.dodb";
        // model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
        String modelOutputDir = baseModelOutputDir + "/..";

        // 创建生成器
        Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);

        // 配置是否生成备注
        generator.setGenerateRemarks(true);

        // 设置数据库方言
        generator.setDialect(new MysqlDialect());

        // 设置是否生成链式 setter 方法
        generator.setGenerateChainSetter(false);

        // 添加不需要生成的表名
        generator.addExcludedTable(excludes);

        // 设置是否在 Model 中生成 dao 对象
        generator.setGenerateDaoInModel(false);

        // 设置是否生成字典文件
        generator.setGenerateDataDictionary(false);

        // 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
        generator.setRemovedTableNamePrefixes("t_", "d_");

        // 生成
        generator.generate();
    }

    private static String detectWebRootPath() {
        try {
            String path = PathKit.class.getResource("/").toURI().getPath();
            return new File(path).getParentFile().getParentFile().getParentFile().getCanonicalPath();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}




