package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @Author zhouxu
 * @create 2021-04-06 22:41
 */
@Getter
@Setter
public class UserVo implements Serializable {
    private Integer errcode;
    private String errmsg;
    private String userid;
    private String name;
    private ArrayList<Integer> department;
    private String position;
    private String mobile;
    private String gender;
    private String email;
    private String avatar;
    private String status;
    private Integer isleader;
    private String telephone;
    private Integer enable;
    private Integer hide_mobile;
    private ArrayList<Integer> order;
    private String main_department;
    private UserAttr extattr;
    private String qr_code;
    private String external_profile;
    private String address;
    private String open_userid;
    private String alias;
    private ArrayList<Integer> is_leader_in_dept;
    private String thumb_avatar;

}
