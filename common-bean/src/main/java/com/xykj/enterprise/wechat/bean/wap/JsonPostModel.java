package com.xykj.enterprise.wechat.bean.wap;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author george
 * @create 2021-04-21 10:08
 */
@ApiModel
@Data
public class JsonPostModel {

    @ApiModelProperty
    private String jsonData;

}
