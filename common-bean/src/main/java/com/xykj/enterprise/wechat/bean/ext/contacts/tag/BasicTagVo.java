package com.xykj.enterprise.wechat.bean.ext.contacts.tag;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BasicTagVo extends BaseResp implements Serializable {
    private Integer tagid;
    private String tagname;
}
