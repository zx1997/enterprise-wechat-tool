package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GetCorpTagListVo {

    private int errcode;

    private String errmsg;

    private List<Tag_group> tag_group;


}

