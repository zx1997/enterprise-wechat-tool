package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-04-02 11:28
 */
@Data
public class Auth_user_info implements Serializable {
    private String userid;

    private String name;

    private String avatar;

    private String open_userid;


}

