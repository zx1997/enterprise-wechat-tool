package com.xykj.enterprise.wechat.bean.wap.customer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-04-07 14:06
 */
@Data
@ApiModel(description = "登录参数")
public class SaveFollowLogModel {

    @ApiModelProperty("logId")
    private Long logId;
    @ApiModelProperty("客户ID")
    private String customerId;
    @ApiModelProperty("跟进内容")
    private String followContent;
    @ApiModelProperty("跟进状态")
    private String followStatus;
    @ApiModelProperty("日程ID")
    private String scheduleId;

}