package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ExternalContactFollowUserTag implements Serializable {
    private String group_name;
    private String tag_name;
    private String tag_id;
    private Integer type;
}
