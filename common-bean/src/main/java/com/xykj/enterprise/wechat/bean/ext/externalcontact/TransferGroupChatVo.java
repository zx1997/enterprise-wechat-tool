package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class TransferGroupChatVo extends BaseResp implements Serializable {
    ArrayList<FailedChatListVo> failed_chat_list;
}
