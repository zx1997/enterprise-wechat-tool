package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author zhouxu
 * @create 2021-04-08 15:19
 */
@Getter
@Setter
public class External_attr {
    private int type;

    private String name;

    private Text text;
}