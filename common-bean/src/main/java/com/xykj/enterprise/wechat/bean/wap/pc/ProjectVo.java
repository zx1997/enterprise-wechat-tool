package com.xykj.enterprise.wechat.bean.wap.pc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author george
 * @create 2021-05-12 15:17
 */
@Data
@ApiModel(description = "项目")
public class ProjectVo {

    @ApiModelProperty("项目ID")
    private Integer id;
    @ApiModelProperty("项目名称")
    private String projectName;
    @ApiModelProperty("项目阶段")
    private String projectProcess;
    @ApiModelProperty("项目负责人")
    private String projectManager;
    @ApiModelProperty("项目签订日期")
    private Integer signDay;
    @ApiModelProperty("项目金额")
    private String projectAmount;
    @ApiModelProperty("销售费用")
    private String salesExpense;
    @ApiModelProperty("开发预计开始时间")
    private String planDevStartTime;
    @ApiModelProperty("客户要求结束时间")
    private String customerReqEndTime;
    @ApiModelProperty("实际开始时间")
    private String realStartTime;
    @ApiModelProperty("实际结束时间")
    private String realEndTime;
    @ApiModelProperty("维护开始时间")
    private String maintainStartTime;
    @ApiModelProperty("免费维护时长")
    private Integer freeMaintainDay;
    @ApiModelProperty("维护费用")
    private String maintainFee;


}
