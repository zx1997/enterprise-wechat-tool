package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

@Data
public class ConvertToOpenIdVo {
    String userid;
}
