package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Data;

import java.util.List;

/**
 * @Author george
 * @create 2021-04-30 16:53
 */
@Data
public class MomentVo {
    private String moment_id;
    private String creator;
    private long create_time;
    private int create_type;
    private int visible_type;
    private MomentTextVo text;
    private List<MomentImageVo> image;
    private MomentVideoVo video;
    private MomentLinkVo link;
    private MomentLocationVo location;

}
