package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConvertToUserIdVo extends BaseResp {

    private String userid;

}

