package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class GroupChatListVo extends BaseResp implements Serializable {
    private ArrayList<GroupChatInfo> group_chat_list;
    private String next_cursor;
}
