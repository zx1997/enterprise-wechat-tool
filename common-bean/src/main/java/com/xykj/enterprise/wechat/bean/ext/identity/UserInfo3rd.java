package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-03-31 16:09
 */
@Data
public class UserInfo3rd implements Serializable {

    private int errcode;
    private String errmsg;
    private String CorpId;
    private String UserId;
    private String DeviceId;
    private String user_ticket;
    private int expires_in;
    private String open_userid;

}