package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author zhouxu
 * @create 2021-02-23 10:12
 */
@Getter
@Setter
@ToString
public class AccessToken {

    public static final int STATUS_SUCCESS = 0;

    private Integer errcode;
    private String errmsg;
    private String access_token;
    private Integer expires_in;

}
