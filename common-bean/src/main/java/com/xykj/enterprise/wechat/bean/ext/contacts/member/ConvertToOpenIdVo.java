package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConvertToOpenIdVo extends BaseResp {

    private String openid;

}
