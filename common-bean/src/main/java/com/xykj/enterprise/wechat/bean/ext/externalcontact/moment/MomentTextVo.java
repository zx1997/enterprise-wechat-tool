package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author george
 * @create 2021-04-30 16:54
 */
@Getter
@Setter
public class MomentTextVo {

    private String content;

}
