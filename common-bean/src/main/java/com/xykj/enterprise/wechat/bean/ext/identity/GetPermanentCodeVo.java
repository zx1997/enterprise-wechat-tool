package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:40
 */
@Data
public class GetPermanentCodeVo implements Serializable {
    private String access_token;

    private int expires_in;

    private String permanent_code;

    private Auth_corp_info auth_corp_info;

    private Auth_info auth_info;

    private Auth_user_info auth_user_info;

    private Dealer_corp_info dealer_corp_info;

    private Register_code_info register_code_info;
}




