package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

@Data
public class AgentConfigVo {

    private String corpid;
    private Long agentid;
    private Long timestamp;
    private String nonceStr;
    private String signature;

}
