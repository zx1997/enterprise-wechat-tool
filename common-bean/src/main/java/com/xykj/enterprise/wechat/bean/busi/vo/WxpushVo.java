package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-02-25 14:42
 */
@Data
public class WxpushVo {

    private String msgType;
    private String event;
    private String changeType;
    // 异步动作
    private String action;

}
