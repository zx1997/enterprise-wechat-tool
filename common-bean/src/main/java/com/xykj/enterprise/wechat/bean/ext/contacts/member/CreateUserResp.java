package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUserResp {
    private Integer errcode;
    private String errmsg;
}
