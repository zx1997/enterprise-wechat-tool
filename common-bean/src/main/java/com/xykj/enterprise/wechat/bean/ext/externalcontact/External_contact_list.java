package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author zhouxu
 * @create 2021-04-08 15:29
 */
@Getter
@Setter
public class External_contact_list {

    private External_contact external_contact;

    private Follow_info follow_info;



}
