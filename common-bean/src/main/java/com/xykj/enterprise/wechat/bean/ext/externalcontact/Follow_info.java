package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-08 15:28
 */
@Getter
@Setter
public class Follow_info {

    private String remark;

    private String description;

    private long createtime;

    private List<String> tag_id;

    private String remark_corp_name;

    private List<String> remark_mobiles;

    private String oper_userid;

    private String add_way;

}
