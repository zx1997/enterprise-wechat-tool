package com.xykj.enterprise.wechat.bean.busi;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 企业微信的企业授权地址
 *
 * @Author zhouxu
 * @create 2021-03-31 14:12
 */
@Getter
@Setter
@ToString
public class WeComEnterpriseAuthUrl implements Serializable {

    // 预授权码
    private String preAuthCode;
    // 第三方服务商 suite_id
    private String suiteId;
    // redirect_uri是授权完成后的回调网址
    private String redirectUri;
    // state可填a-zA-Z0-9的参数值（不超过128个字节），用于第三方自行校验session，防止跨域攻击
    private String state;

    // 授权完整地址
    private String wholeUrl;

    public static final String DOMAIN = "https://open.work.weixin.qq.com/3rdapp/install";

    public WeComEnterpriseAuthUrl(String preAuthCode, String suiteId, String redirectUri, String state) {
        this.preAuthCode = preAuthCode;
        this.suiteId = suiteId;
        this.redirectUri = redirectUri;
        this.state = state;
        buildWholeUrl();
    }

    public void buildWholeUrl() {
        StringBuilder url = new StringBuilder(DOMAIN)
                .append("?")
                .append("suite_id=")
                .append(suiteId)
                .append("&pre_auth_code=")
                .append(preAuthCode)
                .append("&redirect_uri=")
                .append(redirectUri)
                .append("&state=")
                .append(state);
        this.wholeUrl = url.toString();
    }

}
