package com.xykj.enterprise.wechat.bean.ext.schedule;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ScheduleRemindersExcludeTimeInfo implements Serializable {
    private Integer start_time;
}
