package com.xykj.enterprise.wechat.bean.wap.pc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author george
 * @create 2021-05-12 15:25
 */
@Data
@ApiModel(description = "成员项目日志")
public class ProjectLogVo {
    @ApiModelProperty("日志ID")
    private Long id;
    @ApiModelProperty("工作日期")
    private Integer workDay;
    @ApiModelProperty("参与人")
    private String worker;
    @ApiModelProperty("参与项目ID")
    private Integer projectId;
    @ApiModelProperty("项目内容")
    private String workContent;
    @ApiModelProperty("工作时长")
    private String workHour;


}
