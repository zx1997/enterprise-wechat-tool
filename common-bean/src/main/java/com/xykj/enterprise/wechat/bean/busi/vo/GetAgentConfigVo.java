package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

/**
 * @Author george
 * @create 2021-04-19 20:51
 */
@Data
public class GetAgentConfigVo {

    private String corpid;
    private String agentid;
    private String timestamp;
    private String nonceStr;
    private String signature;

}
