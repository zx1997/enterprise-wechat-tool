package com.xykj.enterprise.wechat.bean.ext;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BasicUserVo implements Serializable {
    private String userid;
    private String name;
}