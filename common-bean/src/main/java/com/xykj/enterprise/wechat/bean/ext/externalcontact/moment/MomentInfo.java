package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MomentInfo implements Serializable {
    private String moment_id;
    private String creator;
    private String create_time;
    private Integer create_type;
    private Integer visible_type;
    private MomentContentText text;
    private MomentContentImage image;
    private MomentContentVideo video;
    private MomentContentLink link;
    private MomentContentLocation location;
}
