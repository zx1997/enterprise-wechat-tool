package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author zhouxu
 * @create 2021-04-12 09:30
 */
@Getter
@Setter
public class Tag {

    private String id;

    private String name;

    private int create_time;

    private int order;

    private boolean deleted;

}
