package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author zhouxu
 * @create 2021-02-21 14:45
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class People {

    private int sex;

    private String name;

    private int age;

    private String drlType;

}
