package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GroupChatVo extends BaseResp implements Serializable {
    private GroupChatInfo group_chat;

}

