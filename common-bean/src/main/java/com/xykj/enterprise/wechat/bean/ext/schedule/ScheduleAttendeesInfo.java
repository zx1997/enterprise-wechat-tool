package com.xykj.enterprise.wechat.bean.ext.schedule;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ScheduleAttendeesInfo implements Serializable {
    private String userid;
    private Integer response_status;
}
