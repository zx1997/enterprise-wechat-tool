package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class GetMomentCustomerListVo extends BaseResp implements Serializable {
    private String next_cursor;
    private ArrayList<MomentCustomerInfo> customer_list;
}
