package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class CorpTagGroupVo implements Serializable {
    private String group_id;
    private String group_name;
    private Integer create_time;
    private Integer order;
    private Boolean deleted;
    private ArrayList<CorpTagVo> tag;
}
