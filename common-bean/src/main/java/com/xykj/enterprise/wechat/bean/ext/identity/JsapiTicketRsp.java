package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class JsapiTicketRsp implements Serializable {

    public static final int STATUS_SUCCESS = 0;
    private Integer errcode;
    private String errmsg;
    private String ticket;
    private Integer expires_in;
}
