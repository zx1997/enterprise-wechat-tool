package com.xykj.enterprise.wechat.bean.ext.contacts.department;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class DepartmentVo implements Serializable {
    private Integer id;
    private String name;
    private String nameEn;
    private Integer parentId;
    private Integer order;
}
