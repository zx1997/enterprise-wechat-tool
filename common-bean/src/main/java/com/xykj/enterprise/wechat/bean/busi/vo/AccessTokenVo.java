package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-02-23 10:12
 */
@Data
public class AccessTokenVo implements Serializable {

    public static final int STATUS_SUCCESS = 0;

    private Integer errcode;
    private String errmsg;
    private String access_token;
    private Integer expires_in;

}
