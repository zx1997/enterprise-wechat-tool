package com.xykj.enterprise.wechat.bean.busi.vo.wxpush;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:02
 */
@Data
public class SuiteTicketVo implements Serializable {

    private String SuiteId;
    private String InfoType;
    private Long TimeStamp;
    private String SuiteTicket;

}
