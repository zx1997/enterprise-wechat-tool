package com.xykj.enterprise.wechat.bean.busi.vo.wxpush.user;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CreateUserVo implements Serializable {

    private String SuiteId;
    private String AuthCorpId;
    private String InfoType;
    private String TimeStamp;
    private String ChangeType;
    private String UserID;
    private String OpenUserID;
    private String Name;
    private String Department;
    private String MainDepartment;
    private String IsLeaderInDept;
    private String Mobile;
    private String Position;
    private String Gender;
    private String Email;
    private String Avatar;
    private String Alias;
    private String Telephone;
    private ExtAttr ExtAttr;


}

@Data
class ExtAttr {

    private List<Item> Item;

}

@Data
class Item {

    private String Name;
    private String Type;
    private Text Text;


}

@Data
class Text {

    private String Value;

}