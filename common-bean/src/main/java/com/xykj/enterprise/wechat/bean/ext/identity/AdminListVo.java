package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.util.List;

/**
 * @Author george
 * @create 2021-05-29 10:02
 */
@Data
public class AdminListVo {
    private int errcode;
    private String errmsg;
    private List<Admin> admin;

}
