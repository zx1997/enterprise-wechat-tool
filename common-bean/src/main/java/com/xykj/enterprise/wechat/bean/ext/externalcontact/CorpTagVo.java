package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CorpTagVo implements Serializable {
    private String id;
    private String name;
    private Integer create_time;
    private Integer order;
    private Boolean deleted;
}
