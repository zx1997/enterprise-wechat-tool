package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GetExternalContactBatchVo {

    private int errcode;

    private String errmsg;

    private List<External_contact_list> external_contact_list;

    private String next_cursor;

}


