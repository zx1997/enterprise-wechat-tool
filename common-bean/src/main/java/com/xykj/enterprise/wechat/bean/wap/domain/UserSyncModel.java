package com.xykj.enterprise.wechat.bean.wap.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-04-07 14:06
 */
@Data
@ApiModel(description = "企业成员数据同步参数")
public class UserSyncModel {
    @ApiModelProperty("员工code")
    private String code;
    @ApiModelProperty("部门ID")
    private String department_id;
    @ApiModelProperty("1/0：是否递归获取子部门下面的成员")
    private String fetch_child;


}