package com.xykj.enterprise.wechat.bean.ext.contacts.department;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateDepartmentVo extends BaseResp {
    private Integer id;
}
