package com.xykj.enterprise.wechat.bean.wap.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "用户登录信息")
public class LoginInfoVo {

    @ApiModelProperty("登录类型 1 员工 2 客户")
    private Integer userType; //登录类型 1 员工 2 客户
    @ApiModelProperty("用户标识")
    private Long id; //用户标识
    @ApiModelProperty("用户企业微信标识")
    private String userId; //用户企业微信标识
    @ApiModelProperty("企业标识")
    private Long corpId; //企业标识
    @ApiModelProperty("用户名称")
    private String name; //用户名称
    @ApiModelProperty("手机号码")
    private String mobile; //手机号码
    @ApiModelProperty("部门")
    private String department; //部门
    @ApiModelProperty("排序")
    private Long order; //排序
    @ApiModelProperty("职位")
    private String position; //职位
    @ApiModelProperty("性别")
    private String gender; //性别。0表示未定义，1表示男性，2表示女性
    @ApiModelProperty("邮箱")
    private String email; //邮箱
    @ApiModelProperty("是否是部门领导")
    private String isLeaderInDept;
    private String avatar;
    private String thumbAvatar;
    private String telephone;
    private String alias;
    private String status;
    private String qrCode;
    private String externalPosition;
    private String address;
    private String openUserid;
    private String mainDepartment;
    @ApiModelProperty("token")
    private String token;
}
