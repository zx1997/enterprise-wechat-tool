package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class ExternalContactVo extends BaseResp {
    private String external_userid;
    private String name;
    private String position;
    private String avatar;
    private String corp_name;
    private String corp_full_name;
    private Integer type;
    private Integer gender;
    private String unionid;
    private String external_profile;
    private ArrayList<ExternalContactFollowUser> follow_user;
}
