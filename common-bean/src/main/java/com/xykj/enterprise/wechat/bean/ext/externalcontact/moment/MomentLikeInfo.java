package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MomentLikeInfo implements Serializable {
    private String external_userid;
    private Integer create_time;
}
