package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Data;

/**
 * @Author george
 * @create 2021-05-10 13:43
 */
@Data
public class MomentLinkVo {

    private String title;
    private String url;

}
