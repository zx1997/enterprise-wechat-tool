package com.xykj.enterprise.wechat.bean.ext.contacts.member;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;


@Getter
@Setter
public class UserListVo implements Serializable {
    private Integer errcode;
    private String errmsg;
    private ArrayList<UserVo> userlist;

}

