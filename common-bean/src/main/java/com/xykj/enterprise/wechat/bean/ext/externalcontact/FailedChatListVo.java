package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FailedChatListVo implements Serializable {
    private String chat_id;
    private Integer errcode;
    private String errmsg;
}
