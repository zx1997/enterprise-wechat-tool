package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MomentContentVideo implements Serializable {
    private String media_id;
    private String thumb_media_id;
}
