package com.xykj.enterprise.wechat.bean.ext.contacts.tag;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import com.xykj.enterprise.wechat.bean.ext.BasicUserVo;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class GetTagUserVo extends BaseResp implements Serializable {
    private String tagname;
    private ArrayList<BasicUserVo> userlist;
    private ArrayList<Integer> partylist;
}
