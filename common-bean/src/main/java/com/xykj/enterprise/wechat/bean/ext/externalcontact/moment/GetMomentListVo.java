package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Data;

import java.util.List;

@Data
public class GetMomentListVo {

    private int errcode;
    private String errmsg;
    private String next_cursor;
    private List<MomentVo> moment_list;

}
