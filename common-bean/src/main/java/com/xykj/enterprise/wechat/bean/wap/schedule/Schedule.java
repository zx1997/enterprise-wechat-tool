package com.xykj.enterprise.wechat.bean.wap.schedule;

import lombok.Data;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-14 09:19
 */
@Data
public class Schedule
{
    private String organizer;

    private int start_time;

    private int end_time;

    private List<Attendees> attendees;

    private String summary;

    private String description;

    private Reminders reminders;

    private String location;

    private String cal_id;


}
