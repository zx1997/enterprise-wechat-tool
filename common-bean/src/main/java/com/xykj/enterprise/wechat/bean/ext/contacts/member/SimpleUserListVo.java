package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
class SimpleUserVo implements Serializable {
    private String userid;
    private String name;
    private ArrayList<Integer> department;

}

@Getter
@Setter
public class SimpleUserListVo implements Serializable {
    private Integer errcode;
    private String errmsg;
    private ArrayList<SimpleUserVo> userlist;
}
