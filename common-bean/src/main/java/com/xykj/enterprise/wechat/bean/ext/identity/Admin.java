package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

/**
 * @Author george
 * @create 2021-05-29 10:03
 */
@Data
public class Admin {
    private String userid;
    private String open_userid;
    private int auth_type;

}

