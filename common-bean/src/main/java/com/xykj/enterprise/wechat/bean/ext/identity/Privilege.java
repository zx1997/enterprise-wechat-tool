package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-02 11:29
 */

@Data
public class Privilege implements Serializable {
    private int level;

    private List<String> allow_party;

    private List<String> allow_user;

    private List<String> allow_tag;

    private List<String> extra_party;

    private List<String> extra_user;

    private List<String> extra_tag;

}
