package com.xykj.enterprise.wechat.bean.wap.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-04-07 14:06
 */
@Data
@ApiModel(description = "登录参数")
public class DepartmentSyncModel {
    @ApiModelProperty("员工code")
    private String code;

}