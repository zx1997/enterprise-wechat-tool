package com.xykj.enterprise.wechat.bean.wap.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(description = "客户视图基本信息")
public class GetCustomerInfoVo {

    // 基本信息：头像、名称、地区、电话、状态
    @ApiModelProperty("主键ID")
    private Long id;
    @ApiModelProperty("外部联系人id")
    private String externalUserid;
    @ApiModelProperty("名字")
    private String name;
    @ApiModelProperty("头像")
    private String avatar;
    @ApiModelProperty("手机号")
    private String mobile;
    @ApiModelProperty("地区")
    private String position;
    @ApiModelProperty("状态")
    private String status;
    @ApiModelProperty("备注")
    private String remark;

    // 统计信息：好友天数、已加员工个数、已加群聊数
    @ApiModelProperty("备注")
    private Integer friendDayCount;
    @ApiModelProperty("已加员工个数")
    private Integer staffAddCount;
    @ApiModelProperty("已加群聊数")
    private Integer groupAddCount;

    // 企业标签信息
    private String tags;

}
