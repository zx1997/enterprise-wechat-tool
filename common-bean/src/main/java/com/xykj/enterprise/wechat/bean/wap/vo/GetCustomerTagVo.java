package com.xykj.enterprise.wechat.bean.wap.vo;

import com.xykj.enterprise.wechat.bean.busi.vo.CustomerCorpTagVo;
import lombok.Data;

import java.util.List;

@Data
public class GetCustomerTagVo {
    private List<CustomerCorpTagVo> corpTagVo;
    private List<CustomerTagVo> customerTagVo;
}
