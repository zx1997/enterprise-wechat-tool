package com.xykj.enterprise.wechat.bean.wap.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "企业标签")
public class CorpTagVo {
    @ApiModelProperty("主键ID")
    private Long id;
    @ApiModelProperty("企业ID")
    private String corpId;
    @ApiModelProperty("标签组ID")
    private String groupId;
    @ApiModelProperty("标签组名")
    private String groupName;
    @ApiModelProperty("标签ID")
    private String tagId;
    @ApiModelProperty("标签名")
    private String tagName;
    @ApiModelProperty("排序值")
    private Long order;
    @ApiModelProperty("创建时间")
    private String createTime;
    @ApiModelProperty("是否已经被删除")
    private String isDeleted;
}
