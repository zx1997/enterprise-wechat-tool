package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-04-02 11:27
 */
@Data
public class Agent implements Serializable {
    private int agentid;

    private String name;

    private String round_logo_url;

    private String square_logo_url;

    private String appid;

    private Privilege privilege;

}
