package com.xykj.enterprise.wechat.bean.wap.schedule;

import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-04-14 09:17
 */
@Data
public class Attendees
{
    private String userid;

}