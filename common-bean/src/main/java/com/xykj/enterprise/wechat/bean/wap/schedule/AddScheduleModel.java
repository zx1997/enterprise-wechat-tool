package com.xykj.enterprise.wechat.bean.wap.schedule;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @Author zhouxu
 * @create 2021-04-14 09:17
 */
@Data
public class AddScheduleModel {

    private Schedule schedule;

    private int agentid;

}
