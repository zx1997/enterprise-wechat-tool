package com.xykj.enterprise.wechat.bean.ext.externalcontact;

/**
 * @Author zhouxu
 * @create 2021-04-08 15:19
 */
public class Text {
    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}