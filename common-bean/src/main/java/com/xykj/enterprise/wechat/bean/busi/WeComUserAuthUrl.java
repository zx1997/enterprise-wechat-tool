package com.xykj.enterprise.wechat.bean.busi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-03-31 15:21
 */
@Getter
@Setter
@ToString
public class WeComUserAuthUrl implements Serializable {

    private String appid;
    private String redirectUri;
    private String responseType = "code";
    private String scope = "snsapi_base";
    private String agentid;
    private String state;

    private static final String end_with = "#wechat_redirect";
    private static final String domain = "https://open.weixin.qq.com/connect/oauth2/authorize";

    private String wholeUrl;

    public WeComUserAuthUrl(String appid, String redirectUri, String agentid, String state) {
        this.appid = appid;
        this.redirectUri = redirectUri;
        this.agentid = agentid;
        this.state = state;
        buildWholeUrl();

    }

    private void buildWholeUrl() {
        StringBuilder url = new StringBuilder(domain);
        url.append("?")
                .append("appid=")
                .append(appid)
                .append("&redirect_uri=")
                .append(redirectUri)
                .append("&response_type=")
                .append(responseType)
                .append("&scope=")
                .append(scope)
                .append("&agentid=")
                .append(agentid)
                .append("&state=")
                .append(state)
                .append(end_with)
        ;
        this.wholeUrl = url.toString();
    }


}
