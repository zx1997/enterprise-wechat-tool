package com.xykj.enterprise.wechat.bean.ext.contacts.tag;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class AddTagUserVo extends BaseResp implements Serializable {
    private String invalidlist;
    private ArrayList<Integer> invalidparty;
}
