package com.xykj.enterprise.wechat.bean.wap.schedule;

import lombok.Data;

import java.util.List;


/**
 * @Author zhouxu
 * @create 2021-04-14 09:18
 */
@Data
public class Reminders
{
    private int is_remind;

    private int remind_before_event_secs;

    private int is_repeat;

    private int repeat_type;

    private int repeat_until;

    private int is_custom_repeat;

    private int repeat_interval;

    private List<Integer> repeat_day_of_week;

    private List<Integer> repeat_day_of_month;


}

