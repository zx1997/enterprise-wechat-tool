package com.xykj.enterprise.wechat.bean.wap.pc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author george
 * @create 2021-05-12 15:17
 */
@Data
@ApiModel(description = "项目Bug")
public class ProjectBugVo {

    @ApiModelProperty("bug ID")
    private Long id;
    @ApiModelProperty("项目ID")
    private Long projectId;
    @ApiModelProperty("bug提出方")
    private String proposer;
    @ApiModelProperty("bug名称")
    private String bugName;
    @ApiModelProperty(" bug描述")
    private String bugDesc;
    @ApiModelProperty("附件")
    private String attachment;
    @ApiModelProperty("处理人")
    private String handler;
    @ApiModelProperty("bug状态")
    private String bugStatus;
    @ApiModelProperty("bug来源")
    private String bugSource;


}
