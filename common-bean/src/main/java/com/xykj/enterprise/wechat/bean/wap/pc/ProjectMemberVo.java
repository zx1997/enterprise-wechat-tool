package com.xykj.enterprise.wechat.bean.wap.pc;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author george
 * @create 2021-05-12 15:22
 */
@Data
@ApiModel(description = "项目成员")
public class ProjectMemberVo {
    @ApiModelProperty("项目成员ID")
    private Long id;
    @ApiModelProperty("项目ID")
    private Integer projectId;
    @ApiModelProperty("参与人标识")
    private String memberId;
    @ApiModelProperty("参与人内容")
    private String memberContent;
    @ApiModelProperty("计划开始时间")
    private String planStartTime;
    @ApiModelProperty("实际开始时间")
    private String realStartTime;
    @ApiModelProperty("计划结束时间")
    private String planEndTime;
    @ApiModelProperty("实际结束时间")
    private String realEndTime;


}
