package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

/**
 * @Author zhouxu
 * @create 2021-04-08 15:27
 */
@Getter
@Setter
public class External_contact {
    private String external_userid;

    private String name;

    private String position;

    private String avatar;

    private String corp_name;

    private String corp_full_name;

    private int type;

    private int gender;

    private String unionid;

    private External_profile external_profile;

}
