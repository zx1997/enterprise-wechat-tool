package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Data;

/**
 * @Author george
 * @create 2021-05-10 13:44
 */
@Data
public class MomentLocationVo {

    private String latitude;
    private String longitude;
    private  String name;

}
