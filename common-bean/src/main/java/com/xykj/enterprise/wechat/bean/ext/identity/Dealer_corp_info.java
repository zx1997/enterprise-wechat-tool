package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-04-02 13:46
 */
@Data
public class Dealer_corp_info {

    private String corpid;
    private String corp_name;

}