package com.xykj.enterprise.wechat.bean.ext;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BasicAttrWebVo implements Serializable {
    private String url;
    private String title;
}
