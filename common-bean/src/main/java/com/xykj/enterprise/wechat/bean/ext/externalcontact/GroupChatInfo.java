package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class GroupChatInfo implements Serializable {
    private String chat_id;
    private String name;
    private String owner;
    private Integer create_time;
    private String notice;
    private ArrayList<GroupChatMemberInfo> member_list;

    private ArrayList<GroupChatAdmin> admin_list;
}
