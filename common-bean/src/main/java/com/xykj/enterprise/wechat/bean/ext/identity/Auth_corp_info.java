package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-04-02 11:26
 */
@Data
public class Auth_corp_info implements Serializable {
    private String corpid;

    private String corp_name;

    private String corp_type;

    private String corp_round_logo_url;

    private String corp_square_logo_url;

    private int corp_user_max;

    private int corp_agent_max;

    private String corp_wxqrcode;

    private String corp_full_name;

    private int subject_type;

    private int verified_end_time;

    private String corp_scale;

    private String corp_industry;

    private String corp_sub_industry;

    private String location;


}