package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:22
 */
@Data
public class SuiteAccessToken implements Serializable {

    private Integer errcode;
    private String errmsg;

    private String suite_access_token;
    private Integer expires_in;
}
