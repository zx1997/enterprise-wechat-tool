package com.xykj.enterprise.wechat.bean.ext.identity;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Author zhouxu
 * @create 2021-03-29 16:29
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PreAuthCode extends BaseResp {

    private String pre_auth_code;
    private String expires_in;

}
