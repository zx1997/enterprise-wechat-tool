package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class TransferCustomerInfo implements Serializable {
    private String external_userid;
    private Integer errcode;
}
