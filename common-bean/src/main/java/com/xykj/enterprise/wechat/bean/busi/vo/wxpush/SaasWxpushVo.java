package com.xykj.enterprise.wechat.bean.busi.vo.wxpush;

import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-03-29 15:55
 */
@Data
public class SaasWxpushVo {

    private String InfoType;
    private String ChangeType;

    private String action;

}
