package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

@Data
public class CustomerCorpTagVo {
    private String userId;
    private String externalUserid;
    private String tagId;
    private String tagName;
    private String tagGroupId;
    private String tagGroupName;
    private String createTime;
}
