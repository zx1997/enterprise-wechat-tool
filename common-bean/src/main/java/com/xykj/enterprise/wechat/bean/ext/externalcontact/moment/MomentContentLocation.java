package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MomentContentLocation implements Serializable {
    private String latitude;
    private String longitude;
    private String name;
}
