package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class UnassignedListInfo implements Serializable {
    private String handover_userid;
    private String external_userid;
    private Integer dimission_time;
}
