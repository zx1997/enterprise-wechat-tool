package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class GetUnassignedListVo extends BaseResp implements Serializable {
    private ArrayList<UnassignedListInfo> info;
    private Boolean is_last;
    private String next_cursor;
}
