package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

/**
 * @author wondream
 */
@Data
public class LoginInfoVo {
    private Integer userType; //登录类型 1 员工 2 客户
    private Long id; //用户标识
    private String userId; //用户标识
    private Long corpId; //企业标识
    private String name; //用户标识
    private String mobile; //手机号码
    private String department; //部门
    private Long order; //排序
    private String position; //职位
    private String gender; //性别。0表示未定义，1表示男性，2表示女性
    private String email; //邮箱
    private String isLeaderInDept;
    private String avatar;
    private String thumbAvatar;
    private String telephone;
    private String alias;
    private String status;
    private String qrCode;
    private String externalPosition;
    private String address;
    private String openUserid;
    private String mainDepartment;
    private String token;
    private String externalUserId; //用户标识
}
