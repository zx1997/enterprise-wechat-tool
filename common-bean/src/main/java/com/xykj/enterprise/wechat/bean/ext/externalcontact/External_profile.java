package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-08 15:20
 */
@Getter
@Setter
public class External_profile {
    private List<External_attr> external_attr;
}
