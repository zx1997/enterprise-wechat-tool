package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class GetMomentCommentsVo extends BaseResp implements Serializable {
    private ArrayList<MomentCommentInfo> comment_list;
    private ArrayList<MomentLikeInfo> like_list;
}
