package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author george
 * @create 2021-04-21 11:14
 */
@Data
public class CorpTagGroupVo {

    private String groupId;
    private String groupName;
    private List<CorpTagVo> tagList;

}
