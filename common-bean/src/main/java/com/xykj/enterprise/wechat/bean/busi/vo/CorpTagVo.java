package com.xykj.enterprise.wechat.bean.busi.vo;

import lombok.Data;

@Data
public class CorpTagVo {
    private Long id;
    private String corpId;
    private String groupId;
    private String groupName;
    private String tagId;
    private String tagName;
    private Integer order;
    private String createTime;
    private String isDeleted;
}
