package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class ExternalContactFollowUser implements Serializable {
    private String userid;
    private String remark;
    private String description;
    private Integer createtime;
    private ArrayList<ExternalContactFollowUserTag> tags;
    private String remark_corp_name;
    private ArrayList<String> remark_mobiles;
    private String oper_userid;
    private Integer add_way;
}
