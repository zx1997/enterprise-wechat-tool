package com.xykj.enterprise.wechat.bean.wap.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Author zhouxu
 * @create 2021-04-12 16:30
 */
@Data
public class CustomerFollowLogVo {

    private Long logId;
    private String corpid;
    private String customerId;
    private String userId;
    private String followContent;
    private String followStatus;
    private String scheduleId;
    private Date createTime;
    private Date updateTime;

}
