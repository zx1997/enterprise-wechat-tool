package com.xykj.enterprise.wechat.bean.wap.vo;

import lombok.Data;

@Data
public class UserTicketVo {
    private Integer errcode;
    private String errmsg;
    private String UserId;
    private String DeviceId;
    private String user_ticket;
    private Integer expires_in;
}
