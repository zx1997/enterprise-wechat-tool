package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MomentContentText implements Serializable {
    private String content;
}
