package com.xykj.enterprise.wechat.bean.ext.contacts.department;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class DepartmentListVo extends BaseResp implements Serializable {

    private List<DepartmentVo> department;

}
