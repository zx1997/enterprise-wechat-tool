package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-12 09:29
 */
@Getter
@Setter
public class Tag_group
{
    private String group_id;

    private String group_name;

    private int create_time;

    private int order;

    private boolean deleted;

    private List<Tag> tag;


}

