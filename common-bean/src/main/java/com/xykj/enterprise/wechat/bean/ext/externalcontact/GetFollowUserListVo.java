package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class GetFollowUserListVo extends BaseResp {

    List<String> follow_user;

}
