package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
class ContactWayConclusionsText implements Serializable {
    private String content;
}

@Getter
@Setter
class ContactWayConclusionsImage implements Serializable {
    private String pic_url;
}

@Getter
@Setter
class ContactWayConclusionsLink implements Serializable {
    private String title;
    private String picurl;
    private String desc;
    private String url;
}

@Getter
@Setter
class ContactWayConclusionsMiniprogram implements Serializable {
    private String title;
    private String pic_media_id;
    private String appid;
    private String page;
}

@Getter
@Setter
class ContactWayConclusions implements Serializable {
    private ContactWayConclusionsText text;
    private ContactWayConclusionsImage image;
    private ContactWayConclusionsLink link;
    private ContactWayConclusionsMiniprogram miniprogram;
}

@Getter
@Setter
public class ContactWayVo implements Serializable {
    private String config_id;
    private Integer type;
    private Integer scene;
    private Integer style;
    private String remark;
    private Boolean skip_verify;
    private String state;
    private String qr_code;
    private ArrayList<String> user;
    private ArrayList<Integer> party;
    private Boolean is_temp;
    private String expires_in;
    private String chat_expires_in;
    private String unionid;
    private ContactWayConclusions conclusions;
}
