package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-02 11:27
 */
@Data
public class Auth_info implements Serializable {
    private List<Agent> agent;

}