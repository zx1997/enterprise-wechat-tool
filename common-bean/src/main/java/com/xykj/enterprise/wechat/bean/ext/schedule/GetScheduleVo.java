package com.xykj.enterprise.wechat.bean.ext.schedule;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class GetScheduleVo extends BaseResp implements Serializable {
    private ArrayList<ScheduleInfo> schedule_list;
}
