package com.xykj.enterprise.wechat.bean.wap.vo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(description = "通过agentConfig注入应用的权限")
public class AgentConfigVo {

    private String corpid;
    private Long agentid;
    private Long timestamp;
    private String nonceStr;
    private String signature;

}
