package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import com.xykj.enterprise.wechat.bean.ext.BasicAttrMiniprogramVo;
import com.xykj.enterprise.wechat.bean.ext.BasicAttrTextVo;
import com.xykj.enterprise.wechat.bean.ext.BasicAttrWebVo;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
class ExternalContactAttrVo implements Serializable{
    private Integer type;
    private String name;
}

@Getter
@Setter
class ExternalContactTextAttrVo extends ExternalContactAttrVo implements Serializable{
    private BasicAttrTextVo text;
}

@Getter
@Setter
class ExternalContactWebAttrVo extends ExternalContactAttrVo implements Serializable{
    private BasicAttrWebVo web;
}

@Getter
@Setter
class ExternalContactMiniprogramAttrVo extends ExternalContactAttrVo implements Serializable{
    private BasicAttrMiniprogramVo miniprogram;
}

@Getter
@Setter
public class ExternalContactProfileVo implements Serializable {
    private ArrayList<ExternalContactAttrVo> external_attr;
}
