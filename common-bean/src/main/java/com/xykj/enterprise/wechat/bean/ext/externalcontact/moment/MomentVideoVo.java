package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Data;

/**
 * @Author george
 * @create 2021-05-10 13:42
 */
@Data
public class MomentVideoVo {
    private String media_id;
    private String thumb_media_id;
}
