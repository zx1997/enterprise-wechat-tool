package com.xykj.enterprise.wechat.bean.ext.schedule;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class ScheduleRemindersInfo implements Serializable {
    private Integer is_remind;
    private Integer is_repeat;
    private Integer remind_before_event_secs;
    private ArrayList<Integer> remind_time_diffs;
    private Integer repeat_type;
    private Integer repeat_until;
    private Integer is_custom_repeat;
    private Integer repeat_interval;
    private ArrayList<Integer> repeat_day_of_week;
    private ArrayList<Integer> repeat_day_of_month;
    private Integer timezone;
    private ArrayList<ScheduleRemindersExcludeTimeInfo> exclude_time_list;
}
