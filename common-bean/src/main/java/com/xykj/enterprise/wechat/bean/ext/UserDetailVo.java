package com.xykj.enterprise.wechat.bean.ext;

import lombok.Data;

@Data
public class UserDetailVo {
    private Integer errcode;
    private String errmsg;
    private String userid;
    private String name;
    private String mobile;
    private String gender;
    private String email;
    private String avatar;
    private String qr_code;
}

