package com.xykj.enterprise.wechat.bean.ext.schedule;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class AddScheduleVo extends BaseResp implements Serializable {
    private String schedule_id;
}
