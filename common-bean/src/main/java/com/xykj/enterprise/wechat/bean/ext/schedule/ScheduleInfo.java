package com.xykj.enterprise.wechat.bean.ext.schedule;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

@Getter
@Setter
public class ScheduleInfo implements Serializable {
    private String schedule_id;
    private Integer sequence;
    private String organizer;
    private ArrayList<ScheduleAttendeesInfo> attendees;
    private String summary;
    private String description;
    private ScheduleRemindersInfo reminders;
    private String location;
    private String cal_id;
    private Integer start_time;
    private Integer end_time;
    private Integer status;
}
