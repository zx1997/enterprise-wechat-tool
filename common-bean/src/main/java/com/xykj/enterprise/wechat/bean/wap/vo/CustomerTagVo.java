package com.xykj.enterprise.wechat.bean.wap.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(description = "客户标签（企业和个人）")
public class CustomerTagVo implements Serializable {

    @ApiModelProperty("主键ID")
    private Long id;
    @ApiModelProperty("添加外部联系人的userid")
    private String userId;
    @ApiModelProperty("外部联系人userid")
    private String externalId;
    @ApiModelProperty("标签类型 1 企业 2 个人")
    private String tagType;
    @ApiModelProperty("标签id")
    private String tagId;
    @ApiModelProperty("标签")
    private String tagName;
    @ApiModelProperty("创建时间")
    private String createTime;
    @ApiModelProperty("是否已经被删除")
    private String isDeleted;
}
