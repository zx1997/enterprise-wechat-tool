package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-04-06 22:40
 */
@Setter
@Getter
public class UserTextAttrProperty implements Serializable {
    private String value;

}
