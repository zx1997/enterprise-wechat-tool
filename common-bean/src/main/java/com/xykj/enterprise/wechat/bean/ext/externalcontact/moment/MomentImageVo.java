package com.xykj.enterprise.wechat.bean.ext.externalcontact.moment;

import lombok.Data;

/**
 * @Author george
 * @create 2021-05-10 13:38
 */
@Data
public class MomentImageVo {

    private String media_id;

}
