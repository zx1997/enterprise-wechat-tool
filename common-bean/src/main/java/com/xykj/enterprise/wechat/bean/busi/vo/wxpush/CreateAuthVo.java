package com.xykj.enterprise.wechat.bean.busi.vo.wxpush;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-04-01 10:23
 */
@Data
public class CreateAuthVo implements Serializable {

    private String SuiteId;
    private String InfoType;
    private Long TimeStamp;
    private String AuthCode;

}
