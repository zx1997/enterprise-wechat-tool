package com.xykj.enterprise.wechat.bean.wap;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * @author
 */
@Getter
@Setter
public class TokenVo {

    @ApiModelProperty
    private String token;
    @ApiModelProperty
    private String userid;
}
