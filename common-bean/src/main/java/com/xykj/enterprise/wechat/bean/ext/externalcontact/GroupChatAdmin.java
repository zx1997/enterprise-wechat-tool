package com.xykj.enterprise.wechat.bean.ext.externalcontact;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class GroupChatAdmin implements Serializable {
    private String userid;
}
