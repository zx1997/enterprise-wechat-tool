package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import com.xykj.enterprise.wechat.bean.ext.BaseResp;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
public class InviteVo extends BaseResp {

    private ArrayList<String> invalidUser;

    private ArrayList<String> invalidParty;

    private ArrayList<String> invalidTag;

}