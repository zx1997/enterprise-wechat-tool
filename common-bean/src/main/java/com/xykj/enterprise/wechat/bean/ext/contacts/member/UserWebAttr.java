package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-04-06 22:41
 */
@Setter
@Getter
public class UserWebAttr extends UserBaseAttr implements Serializable {
    private UserWebAttrProperty web;
}
