package com.xykj.enterprise.wechat.bean.ext;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class BasicAttrMiniprogramVo implements Serializable {
    private String appid;
    private String pagepath;
    private String title;
}
