package com.xykj.enterprise.wechat.bean.ext.identity;

import lombok.Data;

/**
 * @Author zhouxu
 * @create 2021-04-02 13:45
 */
@Data
public class Register_code_info {

    private String register_code;
    private String template_id;
    private String state;


}
