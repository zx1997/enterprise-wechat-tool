package com.xykj.enterprise.wechat.bean.ext;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @Author zhouxu
 * @create 2021-03-25 21:44
 */
@Getter
@Setter
public class BaseResp implements Serializable {
    private Integer errcode;
    private String errmsg;
}
