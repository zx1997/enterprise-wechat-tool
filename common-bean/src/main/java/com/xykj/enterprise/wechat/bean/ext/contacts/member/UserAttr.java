package com.xykj.enterprise.wechat.bean.ext.contacts.member;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @Author zhouxu
 * @create 2021-04-06 22:40
 */
@Getter
@Setter
public class UserAttr implements Serializable {
    private ArrayList<UserBaseAttr> attrs;
}
