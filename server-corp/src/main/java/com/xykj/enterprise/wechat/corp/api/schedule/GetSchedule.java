package com.xykj.enterprise.wechat.corp.api.schedule;

import com.xykj.enterprise.wechat.bean.ext.schedule.GetScheduleVo;
import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.service.corp.CorpService;
import com.xykj.enterprise.wechat.core.service.identity.IdentityService;
import com.xykj.enterprise.wechat.util.wecom.schedule.ScheduleUtil;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhouxu
 * @create 2021-04-13 10:50
 */
@Slf4j
@Function(description = "获取日程详情", parameters = {
        @Parameter(name = "userId", type = Type.String, description = "用户ID", required = true),
        @Parameter(name = "corpid", type = Type.String, description = "企业ID", required = true),
        @Parameter(name = "suite_id", type = Type.String, description = "suite_id", required = true),
        @Parameter(name = "secret", type = Type.String, description = "secret", required = true),

        @Parameter(name = "schedule_id_list", type = Type.String, description = "日程ID列表。一次最多拉取1000条--逗号分隔", required = true),
})
@Component
public class GetSchedule implements Action {

    @Autowired
    private IdentityService identityService;


    @Override
    public Response execute(Request request) throws Exception {
        String accessToken = identityService.getAccessToken(
                request.getString("corpid"),
                request.getString("suite_id"),
                request.getString("secret")
        );

        String scheduleIdList = request.getString("schedule_id_list");

        Map<String, Object> param = new HashMap<>();
        param.put("access_token", accessToken);
        param.put("schedule_id_list", scheduleIdList);
        GetScheduleVo vo = ScheduleUtil.getSchedule(accessToken, param);

        return Response.success().put("data", vo);
    }
}
