package com.xykj.enterprise.wechat.corp.api.contacts;

import com.xykj.enterprise.wechat.core.model.dodb.Department;
import com.xykj.enterprise.wechat.core.service.corp.DepartmentService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-06 15:43
 */
@Function(description = "获取部门列表", parameters = {
        @Parameter(name = "userId", type = Type.String, description = "用户ID", required = true),
        @Parameter(name = "corpid", type = Type.String, description = "corpid", required = true),

        @Parameter(name = "parentid", type = Type.Integer, description = "上级部门id", required = false),
})
@Slf4j
@Component
public class DepartmentList implements Action {

    @Autowired
    private DepartmentService departmentService;

    @Override
    public Response execute(Request request) throws Exception {
        String corpid = request.getString("corpid");
        Integer parentid = request.getInteger("parentid");
        if (parentid == null) {
            parentid = 0;
        }
        List<Department> list = departmentService.list(corpid, parentid);
        return Response.success().put("data", list);


    }
}
