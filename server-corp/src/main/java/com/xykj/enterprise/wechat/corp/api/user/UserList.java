package com.xykj.enterprise.wechat.corp.api.user;

import com.xykj.enterprise.wechat.core.model.dodb.AuthCorp;
import com.xykj.enterprise.wechat.core.model.dodb.User;
import com.xykj.enterprise.wechat.core.service.corp.UserService;
import com.xykj.enterprise.wechat.core.service.corp.CorpService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author zhouxu
 * @create 2021-04-06 17:39
 */
@Function(description = "获取部门成员详情", parameters = {
        @Parameter(name = "userId", type = Type.String, description = "用户ID", required = true),
        @Parameter(name = "corpid", type = Type.String, description = "企业ID", required = true),
        @Parameter(name = "suite_id", type = Type.String, description = "suite_id", required = true),
        @Parameter(name = "secret", type = Type.String, description = "secret", required = true),
        @Parameter(name = "userId", type = Type.String, description = "用户ID", required = true),
        @Parameter(name = "department_id", type = Type.Integer, description = "部门id", required = true),
        @Parameter(name = "fetch_child", type = Type.Integer, description = "1/0：是否递归获取子部门下面的成员", required = false),
})
@Slf4j
@Component
public class UserList implements Action {

    @Autowired
    private CorpService corpService;
    @Autowired
    private UserService userService;


    @Override
    public Response execute(Request request) throws Exception {

        User user = userService.getById(request.getString("userId"));

        return null;
    }
}
