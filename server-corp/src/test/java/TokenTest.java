import com.xykj.enterprise.wechat.util.other.HashUtil;
import com.ydn.simplecache.SimpleCache;
import org.springframework.beans.factory.annotation.Autowired;

//@Slf4j
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({"classpath:spring/app-config.xml"})
public class TokenTest {

    @Autowired
    SimpleCache simpleCache;

    //    @Test
    public void loginTest() {
        String token = HashUtil.md5("4") + System.currentTimeMillis();

        String userId = "LiuJiaYang";
        userId = "ZHANGSAN";
        //为登录判断使用
        simpleCache.put("TOKEN_" + token, userId, true);
        simpleCache.put("TOKEN_USERTYPE_" + userId, 1, true);
        simpleCache.put("TOKEN_USER_CORP_" + userId, "ww9e0eb307032dd027", true);
        //缓存token
        simpleCache.put("TOKEN_" + userId, token, true);
        System.out.println("======== TOKEN_" + userId + " ========");
        System.out.println(token);
        System.out.println("======== TOKEN_" + userId + " ========");
        String corpId = simpleCache.get("TOKEN_USER_CORP_" + userId);
        System.out.println(corpId);
    }

    //    @Test
    public void updateTokenTest() {
        String userId = "LiuJiaYang";
        userId = "ZHANGSAN";
        String token = "a87ff679a2f3e71d9181a67b7542122c1616484207058";
        simpleCache.put("TOKEN_" + token, userId, true);
    }


    //    @Test
    public void getTokenTest() {
        String userId = "LiuJiaYang";
        userId = "ZHANGSAN";
        String token = simpleCache.get("TOKEN_" + userId);
        System.out.println(token);
    }
}
