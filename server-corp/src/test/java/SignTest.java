import com.xykj.enterprise.wechat.util.other.HashUtil;
import com.ydn.simplecache.SimpleCache;
import org.springframework.beans.factory.annotation.Autowired;

//@Slf4j
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration({"classpath:spring/app-config.xml"})
public class SignTest {

    @Autowired
    SimpleCache simpleCache;

    //    @Test
    public void signTest() {
        String jsapi_ticket = "5bfb28fb9496f73b6a6b1bec9c23ac2a";
        String noncestr = "3162d41515fe2054dd9ccb17c9ba4a6f";
        Long timestamp = 1616488079312L;
        String url = "http://enterprise.wechat.xinykj.com/h5/pages/index/index";
        String string1 = "jsapi_ticket=" + jsapi_ticket;
        string1 = string1 + "&noncestr=" + noncestr;
        string1 = string1 + "&timestamp=" + timestamp;
        string1 = string1 + "&url=" + url;
        String result = HashUtil.getSha1(string1);
        System.out.println(result);
    }
}
