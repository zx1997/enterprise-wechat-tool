package com.xykj.enterprise.wechat.customer.api;

import com.xykj.enterprise.wechat.bean.wap.vo.CustomerFollowLogVo;
import com.xykj.enterprise.wechat.core.convert.Converter;
import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowLog;
import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import com.ydn.dbframe.plugin.activerecord.Page;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author zhouxu
 * @create 2021-04-12 16:48
 */
@Function(description = "跟进动态列表查询", parameters = {
        @Parameter(name = "userid", type = Type.String, description = "用户标识", required = true),
        @Parameter(name = "corpid", type = Type.String, description = "企业标识", required = true),
        @Parameter(name = "externalUserid", type = Type.String, description = "外部联系人id", required = true),
        @Parameter(name = "page", type = Type.String, description = "当前页", required = true),
        @Parameter(name = "limit", type = Type.String, description = "页大小", required = true)

})
@Component
@Slf4j
public class ListFollowLogs implements Action {

    @Autowired
    private CustomerService customerService;

    @Override
    public Response execute(Request request) throws Exception {
        // 跟进动态列表查询
        String corpid = request.getString("corpid");
        String externalUserid = request.getString("externalUserid");
        String userId = request.getString("userid");
        Integer page = request.getInteger("page");
        Integer limit = request.getInteger("limit");

        try {
            Page<CustomerFollowLog> logPage = customerService.getFollowLog(page, limit, corpid, userId, externalUserid);
            List<CustomerFollowLogVo> voList = ((Converter<List<CustomerFollowLog>, List<CustomerFollowLogVo>>) followLogs -> {
                List<CustomerFollowLogVo> vos = new ArrayList<>();
                followLogs.forEach((v) -> {
                    CustomerFollowLogVo logVo = new CustomerFollowLogVo();
                    logVo.setLogId(v.getLogId());
                    logVo.setCorpid(v.getCorpid());
                    logVo.setCustomerId(v.getCustomerId());
                    logVo.setUserId(v.getUserId());
                    logVo.setFollowContent(v.getFollowContent());
                    logVo.setFollowStatus(v.getFollowStatus());
                    logVo.setScheduleId(v.getScheduleId());
                    logVo.setCreateTime(v.getCreateTime());
                    logVo.setUpdateTime(v.getUpdateTime());
                    vos.add(logVo);
                });
                return vos;
            }).convert(logPage.getList());

            return Response.success()
                    .put("list", voList)
                    .put("total", logPage.getTotalPage())
                    .put("count", logPage.getTotalRow())
                    ;
        } catch (Exception e) {
            log.error("", e);
            return Response.fail(e);
        }

    }
}
