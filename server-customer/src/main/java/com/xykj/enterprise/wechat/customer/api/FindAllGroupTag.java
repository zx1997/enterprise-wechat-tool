package com.xykj.enterprise.wechat.customer.api;

import com.xykj.enterprise.wechat.bean.busi.vo.CorpTagGroupVo;
import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.ListProperty;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Property;
import com.ydn.appserver.annotations.Result;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 企业标签列表
 *
 * @author wondream
 */
@Function(description = "企业标签列表查询", parameters = {
        @Parameter(name = "corpid", type = Type.String, description = "企业ID", required = true),
}, result = @Result(
        properties = {
                @Property(name = "total", type = Type.Integer, description = "总页数"),
                @Property(name = "count", type = Type.Integer, description = "总行数"),

        },
        listProperties = {
                @ListProperty(name = "list", properties = {
                        @Property(name = "id", type = Type.Integer, description = "编号"),
                        @Property(name = "corpid", type = Type.Integer, description = "企业ID"),
                        @Property(name = "groupId", type = Type.String, description = "标签组ID"),
                        @Property(name = "groupName", type = Type.String, description = "标签组名"),
                        @Property(name = "tagId", type = Type.String, description = "标签id"),
                        @Property(name = "tagName", type = Type.String, description = "标签"),
                        @Property(name = "order", type = Type.String, description = "排序值"),
                        @Property(name = "createTime", type = Type.String, description = "创建时间"),
                        @Property(name = "isDeleted", type = Type.String, description = "是否已经被删除"),
                })
        }
))
@Slf4j
@Component
public class FindAllGroupTag implements Action {

    @Autowired
    private CustomerService customerService;

    @Override
    public Response execute(Request request) throws Exception {
        String corpId = request.getString("corpid");
        try {
            List<CorpTagGroupVo> groupVos = customerService.findAllCorpTagGroup(corpId);
            return Response.success().put("list", groupVos).put("total", groupVos.size());
        } catch (Exception e) {
            log.info("", e);
            return Response.fail("操作异常");
        }
    }

}

