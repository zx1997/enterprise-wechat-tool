package com.xykj.enterprise.wechat.customer.api;

import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author zhouxu
 * @create 2021-04-12 16:48
 */
@Function(description = "保存跟进动态", parameters = {
        @Parameter(name = "userid", type = Type.String, description = "用户标识", required = true),
        @Parameter(name = "corpid", type = Type.String, description = "企业标识", required = true),

        @Parameter(name = "logId", type = Type.String, description = "日志ID", required = false),
        @Parameter(name = "customerId", type = Type.String, description = "外部联系人id", required = true),
        @Parameter(name = "followContent", type = Type.String, description = "跟进内容", required = true),
        @Parameter(name = "followStatus", type = Type.String, description = "跟进状态", required = true),
        @Parameter(name = "scheduleId", type = Type.String, description = "日程ID", required = false),

})
@Component
@Slf4j
public class SaveFollowLog implements Action {

    @Autowired
    private CustomerService customerService;

    @Override
    public Response execute(Request request) throws Exception {
        // 跟进动态列表查询
        String corpid = request.getString("corpid");
        String userId = request.getString("userid");
        String customerId = request.getString("customerId");
        String followContent = request.getString("followContent");
        String followStatus = request.getString("followStatus");
        String scheduleId = request.getString("scheduleId");
        Long logId = request.getLong("logId");

        try {
            customerService.saveFollowLog(
                    logId, corpid, userId, customerId, followContent, followStatus, scheduleId
            );

            return Response.success();
        } catch (Exception e) {
            log.error("", e);
            return Response.fail(e);
        }

    }
}
