package com.xykj.enterprise.wechat.customer.api;

import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Author zhouxu
 * @create 2021-04-12 16:48
 */
@Function(description = "删除跟进动态", parameters = {
        @Parameter(name = "userid", type = Type.String, description = "用户标识", required = true),
        @Parameter(name = "corpid", type = Type.String, description = "企业标识", required = true),

        @Parameter(name = "logId", type = Type.String, description = "日志ID", required = true),

})
@Component
@Slf4j
public class DeleteFollowLog implements Action {

    @Autowired
    private CustomerService customerService;

    @Override
    public Response execute(Request request) throws Exception {
        String corpid = request.getString("corpid");
        String userId = request.getString("userid");
        Long logId = request.getLong("logId");

        try {
            customerService.deleteFollowId(logId);

            return Response.success();
        } catch (Exception e) {
            log.error("", e);
            return Response.fail(e);
        }

    }
}
