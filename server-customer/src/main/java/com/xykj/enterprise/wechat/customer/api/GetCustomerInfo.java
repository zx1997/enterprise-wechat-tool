package com.xykj.enterprise.wechat.customer.api;

import com.xykj.enterprise.wechat.bean.wap.vo.GetCustomerInfoVo;
import com.xykj.enterprise.wechat.core.model.dodb.Customer;
import com.xykj.enterprise.wechat.core.model.dodb.CustomerFollowUser;
import com.xykj.enterprise.wechat.core.service.customer.CustomerService;
import com.ydn.appserver.Action;
import com.ydn.appserver.Request;
import com.ydn.appserver.Response;
import com.ydn.appserver.annotations.Function;
import com.ydn.appserver.annotations.Parameter;
import com.ydn.appserver.annotations.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Function(description = "获取客户信息", parameters = {
        @Parameter(name = "userid", type = Type.String, description = "用户标识", required = true),
        @Parameter(name = "corpid", type = Type.String, description = "企业标识", required = true),
        @Parameter(name = "externalUserid", type = Type.String, description = "外部联系人id", required = false)
})
@Component
@Slf4j
public class GetCustomerInfo implements Action {

    @Autowired
    private CustomerService customerService;

    @Override
    public Response execute(Request request) throws Exception {

        String corpid = request.getString("corpid");
        String externalUserid = request.getString("externalUserid");
        Customer customer = customerService.findByExternalUserid(externalUserid, corpid);


        try {
            // 基本信息：头像、名称、地区、电话、状态、备注
            GetCustomerInfoVo vo = new GetCustomerInfoVo();
            vo.setId(customer.getId());
            vo.setExternalUserid(customer.getExternalUserid());
            vo.setName(customer.getName());
            vo.setAvatar(customer.getAvatar());
            vo.setPosition(customer.getPosition());
            vo.setMobile(customer.getMobile());
            // TODO 状态
            vo.setStatus("沟通中（测试）");
            // 备注
            String userid = request.getString("userid");
            CustomerFollowUser followUser = customerService.getUserFollowInfo(
                    externalUserid,
                    corpid,
                    userid
            );
            vo.setRemark(followUser == null ? null : followUser.getRemark());
            // 企业标签信息
            vo.setTags(followUser == null ? null : followUser.getTags());

            // 统计信息：好友天数、已加员工个数、已加群聊数
            vo.setFriendDayCount(100);
            vo.setStaffAddCount(111);
            vo.setGroupAddCount(999);

            return Response.success().put("data", vo);

        } catch (Exception e) {
            log.error("", e);
            return Response.fail(e);
        }
    }
}
